﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iQuad.Madrasati.Globals
{
    public static class Configuration
    {
        public static string APPLICATION_NAME = "Madrasatie";
        public static string APPLICATION_DESCRIPTION = "Madrasatie";
        public static string CLIENT_NAME = "iQuad";
        public static string COMPANY_NAME = "Whitebeard International";
        public static string COMPANY_SITE = "http://WhitebeardInternational.com";

        public static string STORAGE_LOCATION_DOWNLOAD = "http://storage.dev.datainterface.madrasatie.wbintl.co/";

        public static string API_BASE_URL = "/api/madrasatie/upload_assignment";           

        public static string STORAGE_LOCATION_UPLOAD = "C:\\Madrasatie\\Storage\\assignments\\download_attachment\\";

        public static bool MPNS_PRODUCTION = true; //PRODUCTION
        //public static bool MPNS_PRODUCTION = false; //DEV

        public static string APPLICATION_IDENTIFIER = "madrasatie.app.wb";

        public static string ANDROID_PUSH_SERVICE_API = "http://localhost:29779/";
    }
}