//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace iQuad.Madrasati.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class grn
    {
        public int id { get; set; }
        public string grn_no { get; set; }
        public string invoice_no { get; set; }
        public Nullable<System.DateTime> grn_date { get; set; }
        public Nullable<System.DateTime> invoice_date { get; set; }
        public Nullable<decimal> other_charges { get; set; }
        public Nullable<bool> is_deleted { get; set; }
        public Nullable<int> purchase_order_id { get; set; }
        public Nullable<int> finance_transaction_id { get; set; }
        public Nullable<System.DateTime> created_at { get; set; }
        public Nullable<System.DateTime> updated_at { get; set; }
        public Nullable<int> school_id { get; set; }
        public Nullable<int> tax_mode { get; set; }
        public Nullable<bool> is_previous { get; set; }
        public Nullable<int> delivery_note_id { get; set; }
    }
}
