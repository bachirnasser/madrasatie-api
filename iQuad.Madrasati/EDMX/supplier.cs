//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace iQuad.Madrasati.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class supplier
    {
        public int id { get; set; }
        public string name { get; set; }
        public string contact_no { get; set; }
        public string address { get; set; }
        public string tin_no { get; set; }
        public string region { get; set; }
        public string help_desk { get; set; }
        public Nullable<bool> is_deleted { get; set; }
        public Nullable<int> supplier_type_id { get; set; }
        public Nullable<System.DateTime> created_at { get; set; }
        public Nullable<System.DateTime> updated_at { get; set; }
        public Nullable<int> school_id { get; set; }
    }
}
