//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace iQuad.Madrasati.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class discipline_student_actions
    {
        public int id { get; set; }
        public Nullable<int> discipline_action_id { get; set; }
        public Nullable<int> discipline_participation_id { get; set; }
        public Nullable<System.DateTime> created_at { get; set; }
        public Nullable<System.DateTime> updated_at { get; set; }
    }
}
