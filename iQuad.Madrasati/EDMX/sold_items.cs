//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace iQuad.Madrasati.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class sold_items
    {
        public int id { get; set; }
        public Nullable<int> store_item_id { get; set; }
        public Nullable<int> invoice_id { get; set; }
        public Nullable<int> quantity { get; set; }
        public string code { get; set; }
        public string invoice_type { get; set; }
        public Nullable<decimal> rate { get; set; }
        public Nullable<System.DateTime> created_at { get; set; }
        public Nullable<System.DateTime> updated_at { get; set; }
        public Nullable<int> school_id { get; set; }
    }
}
