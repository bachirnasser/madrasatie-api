//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace iQuad.Madrasati.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class exam
    {
        public int id { get; set; }
        public Nullable<int> exam_group_id { get; set; }
        public Nullable<int> subject_id { get; set; }
        public Nullable<System.DateTime> start_time { get; set; }
        public Nullable<System.DateTime> end_time { get; set; }
        public Nullable<decimal> maximum_marks { get; set; }
        public Nullable<decimal> minimum_marks { get; set; }
        public Nullable<int> grading_level_id { get; set; }
        public Nullable<int> weightage { get; set; }
        public Nullable<int> event_id { get; set; }
        public Nullable<System.DateTime> created_at { get; set; }
        public Nullable<System.DateTime> updated_at { get; set; }
        public Nullable<int> school_id { get; set; }
        public Nullable<decimal> weight { get; set; }
    }
}
