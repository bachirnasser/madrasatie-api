//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace iQuad.Madrasati.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class discount_particular_logs
    {
        public int id { get; set; }
        public Nullable<bool> is_amount { get; set; }
        public string receiver_type { get; set; }
        public Nullable<int> finance_fee_id { get; set; }
        public Nullable<int> user_id { get; set; }
        public string name { get; set; }
        public Nullable<decimal> amount { get; set; }
        public Nullable<System.DateTime> created_at { get; set; }
        public Nullable<System.DateTime> updated_at { get; set; }
        public Nullable<int> school_id { get; set; }
    }
}
