//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace iQuad.Madrasati.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class @event
    {
        public int id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public Nullable<System.DateTime> start_date { get; set; }
        public Nullable<System.DateTime> end_date { get; set; }
        public Nullable<bool> is_common { get; set; }
        public Nullable<bool> is_holiday { get; set; }
        public Nullable<bool> is_exam { get; set; }
        public Nullable<bool> is_due { get; set; }
        public Nullable<System.DateTime> created_at { get; set; }
        public Nullable<System.DateTime> updated_at { get; set; }
        public Nullable<int> origin_id { get; set; }
        public string origin_type { get; set; }
        public Nullable<int> school_id { get; set; }
    }
}
