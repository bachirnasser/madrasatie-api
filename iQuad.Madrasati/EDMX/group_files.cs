//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace iQuad.Madrasati.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class group_files
    {
        public int id { get; set; }
        public Nullable<int> group_id { get; set; }
        public Nullable<int> user_id { get; set; }
        public string file_description { get; set; }
        public Nullable<int> group_post_id { get; set; }
        public Nullable<System.DateTime> created_at { get; set; }
        public Nullable<System.DateTime> updated_at { get; set; }
        public string doc_file_name { get; set; }
        public string doc_content_type { get; set; }
        public Nullable<int> doc_file_size { get; set; }
        public Nullable<System.DateTime> doc_updated_at { get; set; }
        public Nullable<int> school_id { get; set; }
    }
}
