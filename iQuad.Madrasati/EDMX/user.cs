//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace iQuad.Madrasati.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class user
    {
        public int id { get; set; }
        public string username { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public Nullable<bool> admin { get; set; }
        public Nullable<bool> student { get; set; }
        public Nullable<bool> employee { get; set; }
        public string hashed_password { get; set; }
        public string salt { get; set; }
        public string reset_password_code { get; set; }
        public Nullable<System.DateTime> reset_password_code_until { get; set; }
        public Nullable<System.DateTime> created_at { get; set; }
        public Nullable<System.DateTime> updated_at { get; set; }
        public Nullable<bool> parent { get; set; }
        public Nullable<bool> is_first_login { get; set; }
        public Nullable<bool> is_deleted { get; set; }
        public Nullable<int> school_id { get; set; }
        public string google_refresh_token { get; set; }
        public string google_access_token { get; set; }
        public string google_expired_at { get; set; }
    }
}
