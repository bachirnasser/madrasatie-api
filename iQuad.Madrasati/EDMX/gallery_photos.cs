//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace iQuad.Madrasati.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class gallery_photos
    {
        public int id { get; set; }
        public Nullable<int> gallery_category_id { get; set; }
        public string description { get; set; }
        public Nullable<System.DateTime> created_at { get; set; }
        public Nullable<System.DateTime> updated_at { get; set; }
        public string photo_file_name { get; set; }
        public string photo_content_type { get; set; }
        public Nullable<int> photo_file_size { get; set; }
        public Nullable<System.DateTime> photo_updated_at { get; set; }
        public string name { get; set; }
        public Nullable<int> school_id { get; set; }
        public Nullable<bool> is_common { get; set; }
    }
}
