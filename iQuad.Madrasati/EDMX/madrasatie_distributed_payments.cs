//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace iQuad.Madrasati.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class madrasatie_distributed_payments
    {
        public int id { get; set; }
        public Nullable<int> madrasatie_payments_distribution_id { get; set; }
        public string amount { get; set; }
        public Nullable<System.DateTime> payment_date { get; set; }
        public string receipt_no { get; set; }
        public string payment_notes { get; set; }
        public Nullable<int> created_by { get; set; }
        public Nullable<int> school_id { get; set; }
        public Nullable<System.DateTime> created_at { get; set; }
        public Nullable<System.DateTime> updated_at { get; set; }
    }
}
