//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace iQuad.Madrasati.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class timetable_entries
    {
        public int id { get; set; }
        public Nullable<int> batch_id { get; set; }
        public Nullable<int> weekday_id { get; set; }
        public Nullable<int> class_timing_id { get; set; }
        public Nullable<int> subject_id { get; set; }
        public Nullable<int> employee_id { get; set; }
        public Nullable<int> timetable_id { get; set; }
        public Nullable<System.DateTime> updated_at { get; set; }
        public Nullable<System.DateTime> created_at { get; set; }
        public string entry_type { get; set; }
        public Nullable<int> entry_id { get; set; }
        public Nullable<bool> mode { get; set; }
        public Nullable<int> school_id { get; set; }
    }
}
