//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace iQuad.Madrasati.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class employee_salary_structures
    {
        public int id { get; set; }
        public Nullable<int> employee_id { get; set; }
        public Nullable<int> payroll_category_id { get; set; }
        public string amount { get; set; }
        public Nullable<System.DateTime> updated_at { get; set; }
        public Nullable<System.DateTime> created_at { get; set; }
        public string gross_salary { get; set; }
        public string net_pay { get; set; }
        public Nullable<int> payroll_group_id { get; set; }
        public Nullable<int> revision_number { get; set; }
        public Nullable<int> latest_revision_id { get; set; }
        public Nullable<int> school_id { get; set; }
    }
}
