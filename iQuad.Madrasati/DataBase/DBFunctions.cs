﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using WB.XAAS.Engine.Crawler;
using System.Diagnostics;
using System.Data;
//using WB.XAAS.Engine.Crawler.ErrorsAndWarnings;

namespace iQuad.Madrasati.DataBase
{
    class DBFunctions
    {
        internal static bool addCrawlingSite(int number, string site, int depth, int RequestID)
        {
            DBConnection con = null;
            try
            {
                con = new DBConnection("insert into sites (number, site, depth, start_date, status,RequestID) values (@number, @site, @depth, @start_Date, @status,@RequestID)", DBConnection.DBCommandType.INSERT);
                con.DbCommand.Parameters.AddWithValue("@number", number);
                con.DbCommand.Parameters.AddWithValue("@site", site);
                con.DbCommand.Parameters.AddWithValue("@depth", depth);
                con.DbCommand.Parameters.AddWithValue("@RequestID", RequestID);
                con.DbCommand.Parameters.AddWithValue("@start_Date", DateTime.Now);
                con.DbCommand.Parameters.AddWithValue("@status", SiteStatusEnum.Crawling);
                //Debug.WriteLine("Debug: addSiteToCrawlQueue");

                //Debug.WriteLine("Debug: Executing Query");
                con.DbCommand.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                EventLog.WriteEntry("Marvin_DBFunctions_Exceptions AddCrawlingSite ", e.Message + "\nstack trace:\n" + e.StackTrace);
                //Debug.WriteLine("Debug: Exception -- e.message: '" + e.Message + "'");
                return false;
            }
            finally
            {
                con.close();
            }
            return true;
        }

        internal static bool updateSiteCrawlingStatus(int siteNumber, SiteStatusEnum status)
        {
            DBConnection con = new DBConnection("update sites set status = @status, end_date = @endDate where number = @number", DBConnection.DBCommandType.UPDATE);
            con.DbCommand.Parameters.AddWithValue("@status", status);
            con.DbCommand.Parameters.AddWithValue("@number", siteNumber);
            con.DbCommand.Parameters.AddWithValue("@endDate", DateTime.Now);
            try
            {
                con.DbCommand.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                EventLog.WriteEntry("Marvin_DBFunctions_Exceptions updateSiteCrawlingStatus ", e.Message + "\nstack trace:\n" + e.StackTrace);
                return false;
            }
            finally
            {
                con.close();
            }
            return true;
        }

        private static int selectSiteNumberFromUri(string siteUri)
        {
            DBConnection con = new DBConnection("select number from sites where site = @siteUri order by number desc", DBConnection.DBCommandType.PARAMETRIZEDSELECT);
            con.DbCommand.Parameters.Add("@siteUri", MySql.Data.MySqlClient.MySqlDbType.String).Value = siteUri;
            con.refreshParametrizedReader();
            int number = -1;
            try
            {
                if (con.DbReader.Read())
                    Int32.TryParse(con.DbReader["number"].ToString(), out number);
            }
            catch (Exception e)
            {
                EventLog.WriteEntry("Marvin_DBFunctions_Exceptions selectSiteNumberFromUri ", e.Message + "\nstack trace:\n" + e.StackTrace);
            }
            finally
            {
                con.close();
            }
            return number;
        }

        internal static DataTable selectSiteCrawlingInfo(Uri siteUri)
        {
            int siteNumber = selectSiteNumberFromUri(siteUri.ToString());
            DBConnection con = new DBConnection("select site, depth, status, start_date, end_date from sites where number = @siteNumber", DBConnection.DBCommandType.PARAMETRIZEDSELECT);
            if (siteNumber != -1)
            {
                con.DbCommand.Parameters.AddWithValue("@siteNumber", siteNumber);
                con.refreshParametrizedReader();
                DataTable toReturn = new DataTable();
                string site; int depth, status; DateTime start, end;
                if (con.DbReader.Read())
                {
                    //site = con.DbReader.GetString("site");
                    site = con.DbReader.GetString(0);
                    depth = con.DbReader.GetInt32(1);
                    status = con.DbReader.GetInt32(2);
                    try
                    {
                        start = con.DbReader.GetDateTime(3);
                    }
                    catch (Exception e)
                    {
                        EventLog.WriteEntry("Marvin_DBFunctions_Exceptions selectSiteCrawlingInfo ", e.Message + "\nstack trace:\n" + e.StackTrace);
                        start = DateTime.MinValue;
                    }
                    try
                    {
                        end = con.DbReader.GetDateTime(4);
                    }
                    catch
                    {
                        end = DateTime.MaxValue;
                    }
                    toReturn.Columns.Add("site", typeof(string));
                    toReturn.Columns.Add("depth", typeof(int));
                    toReturn.Columns.Add("status", typeof(int));
                    toReturn.Columns.Add("start_date", typeof(DateTime));
                    toReturn.Columns.Add("end_date", typeof(DateTime));
                    toReturn.Rows.Add(new object[] { site, depth, status, start, end });
                }
                con.close();
                return toReturn;
            }
            else
            {
                con.close();
                return null;
            }
        }

        internal static bool addSiteCurrentStatus(int siteNumber, int depth, int currentDepth, int exceptionsCount, int errorsCount, int warningsCount, int RequestID)
        {
            int number = getMaxNumber("current_queue") + 1;
            DBConnection con = new DBConnection("insert into current_queue(number, site_number, crawl_depth, current_crawl_depth, exceptions, errors, warnings,RequestID) values(@number, @siteNumber, @depth, @currentDepth, @exceptionsCount, @errorsCount, @warningsCount,@RequestID)", DBConnection.DBCommandType.INSERT);
            con.DbCommand.Parameters.AddWithValue("@number", number);
            con.DbCommand.Parameters.AddWithValue("@siteNumber", siteNumber);
            con.DbCommand.Parameters.AddWithValue("@depth", depth);
            con.DbCommand.Parameters.AddWithValue("@currentDepth", currentDepth);
            con.DbCommand.Parameters.AddWithValue("@RequestID", RequestID);
            con.DbCommand.Parameters.AddWithValue("@exceptionsCount", exceptionsCount);
            con.DbCommand.Parameters.AddWithValue("@errorsCount", errorsCount);
            con.DbCommand.Parameters.AddWithValue("@warningsCount", warningsCount);
            //Debug.WriteLine("Debug: addSiteCurrentStatus");
            try
            {
                //Debug.WriteLine("Debug: Executing Query");
                con.DbCommand.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                //Debug.WriteLine("Debug: Exception -- e.message: '" + e.Message + "'");
                EventLog.WriteEntry("Marvin_DBFunctions_Exceptions addSiteCurrentStatus ", e.Message + "\nstack trace:\n" + e.StackTrace);
                return false;
            }
            finally
            {
                con.close();
            }
            return true;
        }

        internal static DataTable selectSiteCurrentStatus(Uri siteUri)
        {
            int siteNumber = selectSiteNumberFromUri(siteUri.ToString());
            int currentQueueNumber = selectCurrentNumberForUri(siteNumber);
            DBConnection con = new DBConnection("select crawl_depth, current_crawl_depth, exceptions, errors, warnings from current_queue where number = @number", DBConnection.DBCommandType.PARAMETRIZEDSELECT);
            try
            {
                if (currentQueueNumber != -1)
                {
                    con.DbCommand.Parameters.AddWithValue("@number", currentQueueNumber);
                    con.refreshParametrizedReader();
                    DataTable toReturn = null;
                    int depth, currentDepth, exceptions, errors, warnings;
                    if (con.DbReader.Read())
                    {
                        depth = con.DbReader.GetInt32(0);
                        currentDepth = con.DbReader.GetInt32(1);
                        exceptions = con.DbReader.GetInt32(2);
                        errors = con.DbReader.GetInt32(3);
                        warnings = con.DbReader.GetInt32(4);
                        toReturn = new DataTable();
                        toReturn.Columns.Add("site", typeof(string));
                        toReturn.Columns.Add("crawl_depth", typeof(int));
                        toReturn.Columns.Add("current_crawl_depth", typeof(int));
                        toReturn.Columns.Add("exceptions", typeof(int));
                        toReturn.Columns.Add("errors", typeof(int));
                        toReturn.Columns.Add("warnings", typeof(int));
                        toReturn.Rows.Add(new object[] { siteUri.ToString(), depth, currentDepth, exceptions, errors, warnings });
                    }
                    con.close();
                    return toReturn;
                }
                else
                {
                    con.close();
                    return null;
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Marvin_DBFunctions_Exceptions selectSiteCurrentStatus ", ex.Message + "\nstack trace:\n" + ex.StackTrace);
            }
            finally
            {
                con.close();
            }
            return null;
        }

        private static int selectCurrentNumberForUri(int siteNumber)
        {
            DBConnection con = new DBConnection("select max(number) as 'maxnumber' from current_queue where site_number = @siteNumber", DBConnection.DBCommandType.PARAMETRIZEDSELECT);
            con.DbCommand.Parameters.AddWithValue("@siteNumber", siteNumber);
            con.refreshParametrizedReader();
            int maxNumber = -1;
            if (con.DbReader.Read())
                Int32.TryParse(con.DbReader["maxnumber"].ToString(), out maxNumber);
            con.close();
            return maxNumber;
        }

        internal static bool addHyperlinkVisit(string hyperlink, int parentSite, string content, string usefulContent, int responseStatus)
        {
            int number = getMaxNumber("tested_hyperlinks") + 1;
            DBConnection con = new DBConnection("insert into tested_hyperlinks (number, hyperlink, parent_site_number, site_content, site_useful_content, response_status) values (@number, @hyperlink, @parentSite, @content, @usefulContent, @responseStatus)", DBConnection.DBCommandType.INSERT);
            con.DbCommand.Parameters.AddWithValue("@number", number);
            con.DbCommand.Parameters.AddWithValue("@parentSite", parentSite);
            con.DbCommand.Parameters.AddWithValue("@hyperlink", hyperlink);
            con.DbCommand.Parameters.AddWithValue("@content", content);
            con.DbCommand.Parameters.AddWithValue("@usefulContent", usefulContent);
            con.DbCommand.Parameters.AddWithValue("@responseStatus", responseStatus);
            //Debug.WriteLine("Debug: addHyperlinkVisit");
            try
            {
                // Debug.WriteLine("Debug: Executing Query");
                con.DbCommand.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                EventLog.WriteEntry("Marvin_DBFunctions_Exceptions addHyperlinkVisit ", e.Message + "\nstack trace:\n" + e.StackTrace);
                //Debug.WriteLine("Debug: Exception -- e.message: '" + e.Message + "'");
                return false;
            }
            finally
            {
                con.close();
            }
            return true;
        }

        internal static bool addHyperlinkVisit(string hyperlink, int parentSite, string content, string usefulContent, int responseStatus, int RequestID,string Summary)
        {
            int number = getMaxNumber("tested_hyperlinks") + 1;
            DBConnection con = new DBConnection("insert into tested_hyperlinks (number, hyperlink, parent_site_number, site_content, site_useful_content, response_status,RequestID,SummarizedText) values (@number, @hyperlink, @parentSite, @content, @usefulContent, @responseStatus,@RequestID,@Summary)", DBConnection.DBCommandType.INSERT);
            con.DbCommand.Parameters.AddWithValue("@number", number);
            con.DbCommand.Parameters.AddWithValue("@parentSite", parentSite);
            con.DbCommand.Parameters.AddWithValue("@hyperlink", hyperlink);
            con.DbCommand.Parameters.AddWithValue("@content", content);
            con.DbCommand.Parameters.AddWithValue("@usefulContent", usefulContent);
            con.DbCommand.Parameters.AddWithValue("@responseStatus", responseStatus);
            con.DbCommand.Parameters.AddWithValue("@RequestID", RequestID);
            con.DbCommand.Parameters.AddWithValue("@Summary", Summary);
            //Debug.WriteLine("Debug: addHyperlinkVisit");
            try
            {
                // Debug.WriteLine("Debug: Executing Query");
                con.DbCommand.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                EventLog.WriteEntry("Marvin_DBFunctions_Exceptions addHyperlinkVisit ", e.Message + "\nstack trace:\n" + e.StackTrace);
                //Debug.WriteLine("Debug: Exception -- e.message: '" + e.Message + "'");
                return false;
            }
            finally
            {
                con.close();
            }
            return true;
        }

        private static int getMaxNumber(string tableName)
        {
            DBConnection dbcon = new DBConnection("select max(number) as 'maxnumber' from " + tableName, DBConnection.DBCommandType.SELECT);
            int maxNumber = -1;
            if (dbcon.DbReader.Read())
                Int32.TryParse(dbcon.DbReader["maxnumber"].ToString(), out maxNumber);
            dbcon.close();
            return maxNumber;
        }

        internal static bool addWarningInSite(string siteUri, int seed, WarningsEnum warning, int RequestID)
        {
            int number = getMaxNumber("warnings") + 1;
            int warningValue = (int)warning;
            DBConnection con = new DBConnection("insert into warnings (number, seed_number, site_uri, warning,RequestID) values (@number, @seed, @siteUri, @warningValue,@RequestID)", DBConnection.DBCommandType.INSERT);
            con.DbCommand.Parameters.AddWithValue("@number", number);
            con.DbCommand.Parameters.AddWithValue("@seed", seed);
            con.DbCommand.Parameters.AddWithValue("@RequestID", RequestID);
            con.DbCommand.Parameters.AddWithValue("@siteUri", siteUri);
            con.DbCommand.Parameters.AddWithValue("@warningValue", warningValue);
            //Debug.WriteLine("Debug: addWarningInSite");
            try
            {
                //Debug.WriteLine("Debug: Executing Query");
                con.DbCommand.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                EventLog.WriteEntry("Marvin_DBFunctions_Exceptions addWarningInSite ", e.Message + "\nstack trace:\n" + e.StackTrace);
                //EventLog.WriteEntry("DBFunctions_Exceptions", e.Message);
                //Debug.WriteLine("Debug: Exception -- e.message: '" + e.Message + "'");
                return false;
            }
            finally
            {
                con.close();
            }
            return true;
        }

        internal static DataTable selectWarningsOfType(WarningsEnum warning)
        {
            DBConnection con = new DBConnection("select site_uri from warnings where warning = @warning", DBConnection.DBCommandType.PARAMETRIZEDSELECT);
            try
            {
                int warningNumber = (int)warning;
                
                con.DbCommand.Parameters.AddWithValue("@warning", warningNumber);
                con.refreshParametrizedReader();
                DataTable toReturn = new DataTable();
                string uri;
                if (con.DbReader.Read())
                {
                    toReturn.Columns.Add("site_uri", typeof(string));
                    do
                    {
                        uri = con.DbReader.GetString(0);
                        toReturn.Rows.Add(new object[] { uri });
                    }
                    while (con.DbEnumerator.MoveNext());
                }
                con.close();
                return toReturn;
            }
            catch(Exception e)
            {
                EventLog.WriteEntry("Marvin_DBFunctions_Exceptions selectWarningsOfType ", e.Message + "\nstack trace:\n" + e.StackTrace);
                con.close();
                return null;
            }
            finally
            {
              
            }
        
        
        }

        internal static DataTable selectSitesToCrawl()
        {
            DataTable toReturn = new DataTable();
            DBConnection con = new DBConnection("select number, site_uri, depth, stick_to_seed, RequestID  from sites_to_crawl where status = @status", DBConnection.DBCommandType.PARAMETRIZEDSELECT);
            try
            {
                con.DbCommand.Parameters.AddWithValue("@status", SiteStatusEnum.NotCrawledYet);
                con.refreshParametrizedReader();
                string uri;
                int depth, number, stickToSeed, requestID;
                
                if (con.DbReader.Read())
                {
                    toReturn.Columns.Add("number", typeof(int));
                    toReturn.Columns.Add("site_uri", typeof(string));
                    toReturn.Columns.Add("depth", typeof(int));
                    toReturn.Columns.Add("stick_to_seed", typeof(int));
                    toReturn.Columns.Add("RequestID", typeof(int));
                    do
                    {
                        number = con.DbReader.GetInt32(0);
                        uri = con.DbReader.GetString(1);
                        depth = con.DbReader.GetInt32(2);
                        stickToSeed = con.DbReader.GetInt32(3);
                        requestID = con.DbReader.GetInt32(4);
                        toReturn.Rows.Add(new object[] { number, uri, depth, stickToSeed, requestID });
                    }
                    while (con.DbEnumerator.MoveNext());
                }
            }
            catch (Exception e)
            {
                EventLog.WriteEntry("Marvin_DBFunctions_Exceptions selectSitesToCrawl ", e.Message + "\nstack trace:\n" + e.StackTrace);
            }
            finally
            {
                con.close();
            }
            return toReturn;
        }

        internal static bool addSiteToCrawlQueue(string site, int depth, int RequestID)
        {
            int number = getMaxNumber("sites_to_crawl") + 1;
            DBConnection con = new DBConnection("insert into sites_to_crawl (number, site_uri, depth, status,RequestID) values (@number, @site, @depth, @status,@RequestID)", DBConnection.DBCommandType.INSERT);
            con.DbCommand.Parameters.AddWithValue("@number", number);
            con.DbCommand.Parameters.AddWithValue("@site", site);
            con.DbCommand.Parameters.AddWithValue("@depth", depth);
            con.DbCommand.Parameters.AddWithValue("@RequestID", RequestID);
            con.DbCommand.Parameters.AddWithValue("@status", SiteStatusEnum.NotCrawledYet);
            //Debug.WriteLine("Debug: addSiteToCrawlQueue");
            try
            {
                //Debug.WriteLine("Debug: Executing Query");
                con.DbCommand.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                EventLog.WriteEntry("Marvin_DBFunctions_Exceptions addSiteToCrawlQueue ", e.Message + "\nstack trace:\n" + e.StackTrace);
                //Debug.WriteLine("Debug: Exception -- e.message: '" + e.Message + "'");
                return false;
            }
            finally
            {
                con.close();
            }
            return true;
        }

        internal static bool updateSitesToCrawlStatus(int siteNumber)
        {
            DBConnection con = new DBConnection("update sites_to_crawl set status = @status where number = @siteNumber", DBConnection.DBCommandType.UPDATE);
            con.DbCommand.Parameters.AddWithValue("@siteNumber", siteNumber);
            con.DbCommand.Parameters.AddWithValue("@status", SiteStatusEnum.Crawling);
            //Debug.WriteLine("Debug: updateSitesToCrawlStatus");
            try
            {
                //Debug.WriteLine("Debug: Executing Query");
                con.DbCommand.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                EventLog.WriteEntry("Marvin_DBFunctions_Exceptions updateSitesToCrawlStatus ", e.Message + "\nstack trace:\n" + e.StackTrace);
                //Debug.WriteLine("Debug: Exception -- e.message: '" + e.Message + "'");
                return false;
            }
            finally
            {
                con.close();
            }
            return true;
        }
    }
}
