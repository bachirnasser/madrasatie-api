﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using MySql.Data.MySqlClient;

namespace iQuad.Madrasati.DataBase
{
    public class DBConnection
    {
        public enum DBCommandType { SELECT, PARAMETRIZEDSELECT, INSERT, UPDATE, DELETE };
        MySqlConnection dbConnection;
        MySqlTransaction dbTransaction;
        MySqlCommand dbCommand;
        MySqlDataReader dbReader;
        DBCommandType dbCommandType;

        public DBConnection(string commandText, DBCommandType commandType)
        {
            dbCommandType = commandType;
            try
            {
                //DEV
                //dbConnection = new MySqlConnection("data source=34.252.186.96;initial catalog=fedena;user id=APIUser;password=passPASS00; Max Pool Size=10000; Connection Timeout=100");

                //Production
                dbConnection = new MySqlConnection("data source=52.21.230.131;initial catalog=fedena;user id=walid.baydoun;password=passPASS00; Max Pool Size=10000; Connection Timeout=100");

                //EventLog.WriteEntry("Marvin_DBConnection_String", dbConnection.ConnectionString);
                //dbConnection = new MyMySqlConnection("Server=127.0.0.1;Database=xaas_web_crawler;Uid=root;charset=utf8;");
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Marvin_DBConnection_Exception", ex.Message + "\nStack Trace:\n" + ex.StackTrace);
            }

            try
            {
                dbConnection.Open();
            }
            catch(Exception e)
            {
                throw new Exception("Error opening db connection -- message " + e.Message);
            }

            dbTransaction = dbConnection.BeginTransaction();
            dbCommand = new MySqlCommand(commandText, dbConnection, dbTransaction);
            if (commandType == DBCommandType.SELECT)
                dbReader = dbCommand.ExecuteReader();
        }
        public void close()
        {
            try
            {
                if (dbCommandType == DBCommandType.SELECT || dbCommandType == DBCommandType.PARAMETRIZEDSELECT)
                    dbReader.Close();
                dbTransaction.Commit();
                dbCommand.Cancel();
                dbConnection.Close();
            }
            catch
            {
                return;
            }
        }
        public MySqlDataReader DbReader
        {
            get
            {
                if (dbCommandType == DBCommandType.SELECT || dbCommandType == DBCommandType.PARAMETRIZEDSELECT)
                {
                    if (dbReader.HasRows)
                    {
                        return dbReader;
                    }
                    else
                        return null;
                }
                else
                    throw new Exception("No db reader exists for non SELECT command types");
            }
        }
        public MySqlCommand DbCommand
        {
            get { return dbCommand; }
        }
        public System.Collections.IEnumerator DbEnumerator
        {
            get
            {
                if (dbCommandType == DBCommandType.SELECT || dbCommandType == DBCommandType.PARAMETRIZEDSELECT)
                {
                    try
                    {
                        if (dbReader.HasRows)
                        {
                            return dbReader.GetEnumerator();
                        }
                        else
                        {
                            return null;
                        }
                    }
                    catch
                    {
                        throw new Exception("An error occured while trying to get an enumerator");
                    }
                }
                else
                    throw new Exception("Can't get enumerator for non SELECT command types");
            }
        }
        public void refreshParametrizedReader()
        {
            if (dbCommandType == DBCommandType.PARAMETRIZEDSELECT)
            {
                dbReader = dbCommand.ExecuteReader();
            }
            else
                throw new Exception("Refresh is allowed only for parametrized select command");
        }
    }
}