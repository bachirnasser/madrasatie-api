﻿using iQuad.Madrasati.EDMX;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;
using iQuad.Madrasati.DataBase;
using System.Globalization;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.IO;
using System.Web;
using System.Collections;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Specialized;
using System.Xml.Linq;
using System.Xml;
using System.Drawing;
using System.Text.RegularExpressions;

namespace iQuad.Madrasati.Controllers
{
    [RoutePrefix("api/User")]
    public class UserController : ApiController
    {

        [Route("SchoolByActivationCode")]
        [HttpGet]
        public async Task<List<Dictionary<string, string>>> SchoolByActivationCode(string ActivationCode)
        {
            List<madrasatie_school_configs> madrasatieSchoolConfigsList = new List<madrasatie_school_configs>();
            List<object> madrasatieSchoolConfigsListResult = new List<object>();
            List<Dictionary<string, string>> ListUserDictionary = new List<Dictionary<string, string>>();

            //int activationCodeKey = Convert.ToInt32(ActivationCode);

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                var now = DateTime.Now;

                madrasatieSchoolConfigsList = (from madrasatieSchoolConfigs in fedenaEntities.madrasatie_school_configs where madrasatieSchoolConfigs.school_code == ActivationCode select madrasatieSchoolConfigs).ToList<madrasatie_school_configs>();

                var madrasatieSchoolConfigsListNow = DateTime.Now - now;

                var now2 = DateTime.Now;

                var school =
                    (from MSC in madrasatieSchoolConfigsList.DefaultIfEmpty()

                     join sc in fedenaEntities.schools on MSC.school_id equals sc.id into school_MSC
                     from sc_MSC in school_MSC.DefaultIfEmpty()

                     select new
                     {
                         Id = MSC.id,
                         ForeignLanName = MSC.foreign_lan_name,
                         SchoolNumber = MSC.school_number,
                         MobileNumber = MSC.mobile_number,
                         SchoolListNumber = MSC.school_list_number,
                         Province = MSC.province,
                         EducationalAreaDistrict = MSC.educational_area_district,
                         District = MSC.district,
                         City = MSC.city,
                         Type = MSC.type,
                         LicenseNumber = MSC.license_number,
                         LicenseDate = MSC.license_date,
                         SchoolId = MSC.school_id,
                         CreatedAt = MSC.created_at,
                         UpdatedAt = MSC.updated_at,
                         SchoolCode = MSC.school_code,
                         SchoolName = sc_MSC.name
                     }).ToList();

                var schoolNow = DateTime.Now - now2;

                var now3 = DateTime.Now;

                string schoolLogo = string.Empty;

                try
                {
                    var schoolLogoList = await GetSchoolLogo(Convert.ToInt32(school.First().SchoolId));
                    schoolLogo = schoolLogoList.ToList()[0]["Logo"];
                }
                catch
                {
                   
                }

                var schoolLogoListNow = DateTime.Now - now3;

                //System.IO.File.WriteAllText(@"C:\MadrasatieTiming\SchoolActivationtiming.txt", "schoolLogoListNow = " + schoolLogoListNow.ToString() + "       schoolNow = " + schoolNow.ToString() + "       madrasatieSchoolConfigsListNow = " + madrasatieSchoolConfigsListNow.ToString());


                Dictionary<string, string> schoolDictionary = new Dictionary<string, string>();

                schoolDictionary.Add("Id", school.First().Id.ToString());
                schoolDictionary.Add("ForeignLanName", school.First().ForeignLanName == null ? string.Empty : school.First().ForeignLanName.ToString());
                schoolDictionary.Add("SchoolNumber", school.First().SchoolNumber == null ? string.Empty : school.First().SchoolNumber.ToString());
                schoolDictionary.Add("MobileNumber", school.First().MobileNumber == null ? string.Empty : school.First().MobileNumber.ToString());
                schoolDictionary.Add("SchoolListNumber", school.First().SchoolListNumber == null ? string.Empty : school.First().SchoolListNumber.ToString());
                schoolDictionary.Add("Province", school.First().Province == null ? string.Empty : school.First().Province.ToString());
                schoolDictionary.Add("EducationalAreaDistrict", school.First().EducationalAreaDistrict == null ? string.Empty : school.First().EducationalAreaDistrict.ToString());
                schoolDictionary.Add("District", school.First().District == null ? string.Empty : school.First().District.ToString());
                schoolDictionary.Add("City", school.First().City == null ? string.Empty : school.First().City.ToString());
                schoolDictionary.Add("Type", school.First().Type == null ? string.Empty : school.First().Type.ToString());
                schoolDictionary.Add("LicenseNumber", school.First().LicenseNumber == null ? string.Empty : school.First().LicenseNumber.ToString());
                schoolDictionary.Add("LicenseDate", school.First().LicenseDate == null ? string.Empty : school.First().LicenseDate.ToString());
                schoolDictionary.Add("SchoolId", school.First().SchoolId == null ? string.Empty : school.First().SchoolId.ToString());
                schoolDictionary.Add("CreatedAt", school.First().CreatedAt == null ? string.Empty : school.First().CreatedAt.ToString());
                schoolDictionary.Add("UpdatedAt", school.First().UpdatedAt == null ? string.Empty : school.First().UpdatedAt.ToString());
                schoolDictionary.Add("SchoolCode", school.First().SchoolCode == null ? string.Empty : school.First().SchoolCode.ToString());
                schoolDictionary.Add("SchoolName", school.First().SchoolName == null ? string.Empty : school.First().SchoolName.ToString());
                schoolDictionary.Add("SchoolLogo", schoolLogo == null ? string.Empty : schoolLogo.ToString());


                ListUserDictionary.Add(schoolDictionary);

                //madrasatieSchoolConfigsListResult = school.ToList<object>();
            }

            return ListUserDictionary;
        }


        [Route("Salt")]
        public List<object> Salt(string UserName, int SchoolID)
        {
            List<user> usersList = new List<user>();
            List<object> usersListResult = new List<object>();

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                usersList = (from users in fedenaEntities.users where users.username == UserName && users.school_id == SchoolID select users).ToList<user>();

                var user = from us in usersList

                           join sc in fedenaEntities.madrasatie_school_configs on us.school_id equals sc.school_id into us_sc
                           from U in usersList.DefaultIfEmpty()

                           select new
                           {
                               Salt = U.salt
                           };

                usersListResult = user.ToList<object>();
            }

            return usersListResult;
        }


        [Route("SignIn")]
        public List<Dictionary<string, string>> SignIn(string UserName, string Password, int SchoolID,bool Encrypted = true)
        {
            List<user> usersList = new List<user>();
            List<object> usersListResult = new List<object>();

            List<Dictionary<string, string>> ListUserDictionary = new List<Dictionary<string, string>>();

            Password = Encrypted == false ? Hash(Password) : Password;

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                var now1 = DateTime.Now;

                usersList = (from users in fedenaEntities.users where users.username == UserName && users.hashed_password == Password && users.school_id == SchoolID select users).ToList<user>();

                var usersListTime = DateTime.Now - now1;

                var now2 = DateTime.Now;

                var user = from us in usersList

                           join sc in fedenaEntities.madrasatie_school_configs on us.school_id equals sc.school_id into us_sc
                           from U in usersList.DefaultIfEmpty()

                           select new
                           {
                               Id = U.id,
                               Username = U.username,
                               FirstName = U.first_name,
                               LastName = U.last_name,
                               Email = U.email,
                               Admin = U.admin,
                               Student = U.student,
                               Employee = U.employee,
                               Salt = U.salt,
                               ResetPasswordCode = U.reset_password_code,
                               ResetPasswordCodeUntil = U.reset_password_code_until,
                               CreatedAt = U.created_at,
                               UpdatedAt = U.updated_at,
                               Parent = U.parent,
                               IsFirstLogin = U.is_first_login,
                               IsDeleted = U.is_deleted,
                               SchoolId = U.school_id,
                               GoogleRefreshToken = U.google_refresh_token,
                               GoogleAccessToken = U.google_access_token,
                               GoogleExpiredAt = U.google_expired_at
                           };

                usersListResult = user.ToList<object>();

                var userTime = DateTime.Now - now2;

                var now3 = DateTime.Now;

                int userIDKey = Convert.ToInt32(user.ToList().First().Id);
                bool IsStudent = Convert.ToBoolean(user.ToList().First().Student);
                bool IsEmployee = Convert.ToBoolean(user.ToList().First().Employee);
                bool IsParent = Convert.ToBoolean(user.ToList().First().Parent);
                bool IsAdmin = Convert.ToBoolean(user.ToList().First().Admin);
                TimeSpan joinTime = new TimeSpan();
                TimeSpan forLoopTime = new TimeSpan();
                if (IsStudent)
                {
                    var resultIsStudent = (    
                        from s in fedenaEntities.students

                        join bat_stud in fedenaEntities.batch_students on s.id equals bat_stud.student_id into student_batch_students
                        from s_b_s in student_batch_students.DefaultIfEmpty()

                        join bat in fedenaEntities.batches on s_b_s.batch_id equals bat.id into batch_batch_students
                        from b_b_s in batch_batch_students.DefaultIfEmpty()
                        
                        join cor in fedenaEntities.courses on b_b_s.course_id equals cor.id into batch_courses
                        from b_c in batch_courses.DefaultIfEmpty()

                        where s.user_id == userIDKey

                        select new
                        {
                            StudentAdmissionNo = s.admission_no,
                            StudentFirstName = s.first_name,
                            StudentMiddleName = s.middle_name,
                            StudentLastName = s.last_name,
                            BatchName = b_b_s.name,
                            CourseName = b_c.course_name,
                            StudentID = s.id,
                            StudentGender = s.gender
                        }).ToList();

                    foreach(var student in resultIsStudent)
                    {
                        Dictionary<string, object> userDictionary = new Dictionary<string, object>();

                        userDictionary.Add("Id", user.ToList().First().Id);
                        userDictionary.Add("Username", user.ToList().First().Username);
                        userDictionary.Add("FirstName", user.ToList().First().FirstName);
                        userDictionary.Add("LastName", user.ToList().First().LastName);
                        userDictionary.Add("Email", user.ToList().First().Email);
                        userDictionary.Add("Admin", user.ToList().First().Admin);
                        userDictionary.Add("Student", user.ToList().First().Student);
                        userDictionary.Add("Employee", user.ToList().First().Employee);
                        userDictionary.Add("Salt", user.ToList().First().Salt);
                        userDictionary.Add("ResetPasswordCode", user.ToList().First().ResetPasswordCode);
                        userDictionary.Add("ResetPasswordCodeUntil", user.ToList().First().ResetPasswordCodeUntil);
                        userDictionary.Add("CreatedAt", user.ToList().First().CreatedAt);
                        userDictionary.Add("UpdatedAt", user.ToList().First().UpdatedAt);
                        userDictionary.Add("Parent", user.ToList().First().Parent);
                        userDictionary.Add("IsFirstLogin", user.ToList().First().IsFirstLogin);
                        userDictionary.Add("IsDeleted", user.ToList().First().IsDeleted);
                        userDictionary.Add("SchoolId", user.ToList().First().SchoolId);
                        userDictionary.Add("GoogleRefreshToken", user.ToList().First().GoogleRefreshToken);
                        userDictionary.Add("GoogleAccessToken", user.ToList().First().GoogleAccessToken);
                        userDictionary.Add("GoogleExpiredAt", user.ToList().First().GoogleExpiredAt);

                        userDictionary.Add("UserType", "Student");
                        userDictionary.Add("UserTypeID", student.StudentID);

                        userDictionary.Add("Gender", student.StudentGender);

                        Dictionary<string, string> userDictionaryString = userDictionary.ToDictionary(k => k.Key, k => k.Value == null ? string.Empty : k.Value.ToString());

                        ListUserDictionary.Add(userDictionaryString);
                    }

                }

                if (IsEmployee || IsAdmin)
                {
                    var nowJoin = DateTime.Now;

                    var resultIsEmployee = (from emp in fedenaEntities.employees

                                           join emp_cat in fedenaEntities.employee_categories on emp.employee_category_id equals emp_cat.id into empcatemp
                                           from emp_cat_emp in empcatemp.DefaultIfEmpty()

                                           join emp_pos in fedenaEntities.employee_positions on emp.employee_position_id equals emp_pos.id into empposemp
                                           from emp_pos_emp in empposemp.DefaultIfEmpty()

                                           join emp_dep in fedenaEntities.employee_departments on emp.employee_department_id equals emp_dep.id into empdepemp
                                           from emp_dep_emp in empdepemp.DefaultIfEmpty()

                                           join u in fedenaEntities.users on emp.user_id equals u.id into uemp
                                           from u_emp in uemp.DefaultIfEmpty()

                                           join userMang in fedenaEntities.users on emp.reporting_manager_id equals userMang.id into userMang
                                           from userMang_emp in userMang.DefaultIfEmpty()

                                           where u_emp.id == userIDKey

                                           select new
                                           {
                                               EmployeeNumber = emp.employee_number,
                                               FirstName = emp.first_name,
                                               MiddleName = emp.middle_name,
                                               LastName = emp.last_name,
                                               Category = emp_cat_emp.name,
                                               Position = emp_pos_emp.name,
                                               Department = emp_dep_emp.name,
                                               ReportingManagaer = userMang_emp.first_name,
                                               EmployeeGender = emp.gender,
                                               EmployeeID = emp.id
                                           }).ToList();

                    joinTime = DateTime.Now - nowJoin;

                    var nowForloop = DateTime.Now;

                    foreach (var employee in resultIsEmployee)
                    {
                        Dictionary<string, object> userDictionary = new Dictionary<string, object>();

                        userDictionary.Add("Id", user.ToList().First().Id.ToString());
                        userDictionary.Add("Username", user.ToList().First().Username);
                        userDictionary.Add("FirstName", user.ToList().First().FirstName);
                        userDictionary.Add("LastName", user.ToList().First().LastName);
                        userDictionary.Add("Email", user.ToList().First().Email);
                        userDictionary.Add("Admin", user.ToList().First().Admin);
                        userDictionary.Add("Student", user.ToList().First().Student);
                        userDictionary.Add("Employee", user.ToList().First().Employee);
                        userDictionary.Add("Salt", user.ToList().First().Salt);
                        userDictionary.Add("ResetPasswordCode", user.ToList().First().ResetPasswordCode);
                        userDictionary.Add("ResetPasswordCodeUntil", user.ToList().First().ResetPasswordCodeUntil);
                        userDictionary.Add("CreatedAt", user.ToList().First().CreatedAt);
                        userDictionary.Add("UpdatedAt", user.ToList().First().UpdatedAt);
                        userDictionary.Add("Parent", user.ToList().First().Parent);
                        userDictionary.Add("IsFirstLogin", user.ToList().First().IsFirstLogin);
                        userDictionary.Add("IsDeleted", user.ToList().First().IsDeleted);
                        userDictionary.Add("SchoolId", user.ToList().First().SchoolId);
                        userDictionary.Add("GoogleRefreshToken", user.ToList().First().GoogleRefreshToken);
                        userDictionary.Add("GoogleAccessToken", user.ToList().First().GoogleAccessToken);
                        userDictionary.Add("GoogleExpiredAt", user.ToList().First().GoogleExpiredAt);

                        userDictionary.Add("UserType", "Employee");
                        userDictionary.Add("UserTypeID", employee.EmployeeID);

                        userDictionary.Add("Gender", employee.EmployeeGender);

                        Dictionary<string, string> userDictionaryString = userDictionary.ToDictionary(k => k.Key, k => k.Value == null ? string.Empty : k.Value.ToString());

                        ListUserDictionary.Add(userDictionaryString);
                    }

                    forLoopTime = DateTime.Now - nowForloop;
                }

                if (IsParent)
                {
                    var resultIsParent = (from g in fedenaEntities.guardians

                                          join u in fedenaEntities.users on g.user_id equals u.id into users_guardians
                                          from u_g in users_guardians.DefaultIfEmpty()

                                          join stud in fedenaEntities.students on g.ward_id equals stud.sibling_id into stud_guardians
                                          from s_g in stud_guardians.DefaultIfEmpty()

                                          where u_g.id == userIDKey

                                          select new
                                          {
                                              Username = u_g.username,
                                              GuarduianFirstName = g.first_name,
                                              GuardianLastName = g.last_name,
                                              GuardianRelation = g.relation,
                                              CountChildren = stud_guardians.Count(),
                                              ParentID = g.id
                                          }).ToList();

                    foreach (var parent in resultIsParent)
                    {
                        Dictionary<string, object> userDictionary = new Dictionary<string, object>();

                        userDictionary.Add("Id", user.ToList().First().Id);
                        userDictionary.Add("Username", user.ToList().First().Username);
                        userDictionary.Add("FirstName", user.ToList().First().FirstName);
                        userDictionary.Add("LastName", user.ToList().First().LastName);
                        userDictionary.Add("Email", user.ToList().First().Email);
                        userDictionary.Add("Admin", user.ToList().First().Admin);
                        userDictionary.Add("Student", user.ToList().First().Student);
                        userDictionary.Add("Employee", user.ToList().First().Employee);
                        userDictionary.Add("Salt", user.ToList().First().Salt);
                        userDictionary.Add("ResetPasswordCode", user.ToList().First().ResetPasswordCode);
                        userDictionary.Add("ResetPasswordCodeUntil", user.ToList().First().ResetPasswordCodeUntil);
                        userDictionary.Add("CreatedAt", user.ToList().First().CreatedAt);
                        userDictionary.Add("UpdatedAt", user.ToList().First().UpdatedAt);
                        userDictionary.Add("Parent", user.ToList().First().Parent);
                        userDictionary.Add("IsFirstLogin", user.ToList().First().IsFirstLogin);
                        userDictionary.Add("IsDeleted", user.ToList().First().IsDeleted);
                        userDictionary.Add("SchoolId", user.ToList().First().SchoolId);
                        userDictionary.Add("GoogleRefreshToken", user.ToList().First().GoogleRefreshToken);
                        userDictionary.Add("GoogleAccessToken", user.ToList().First().GoogleAccessToken);
                        userDictionary.Add("GoogleExpiredAt", user.ToList().First().GoogleExpiredAt);

                        userDictionary.Add("UserType", "Parent");
                        userDictionary.Add("UserTypeID", parent.ParentID);

                        userDictionary.Add("Gender", string.Empty);

                        Dictionary<string, string> userDictionaryString = userDictionary.ToDictionary(k => k.Key, k => k.Value == null ? string.Empty : k.Value.ToString());

                        ListUserDictionary.Add(userDictionaryString);
                    }
                }

                var resultTime = DateTime.Now - now3;

                //var path = @"C:\MadrasatieTiming\SignIntiming.txt";
                
                //string appendText = "resultTime = " + resultTime.ToString() + Environment.NewLine;
                //appendText += "userTime = " + userTime.ToString() + Environment.NewLine;
                //appendText += "usersListTime = " + usersListTime.ToString() + Environment.NewLine;
                //appendText += "joinTime = " + joinTime.ToString() + Environment.NewLine;
                //appendText += "forLoopTime = " + forLoopTime.ToString() + Environment.NewLine;

                //File.AppendAllText(path, appendText);
            }

            return ListUserDictionary;
        }


        [Route("StudentProfile")]
        public List<object> StudentProfile(int UserID)
        {
            List<user> usersList = new List<user>();

            List<object> resultJSON = new List<object>();

            int userIDKey = Convert.ToInt32(UserID);

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {

//                from batches b inner
//                  join users u on b.school_id = u.school_id
//                  inner join students s on s.user_id = u.id
//                  inner join courses c on c.id = b.course_id
//                  WHERE u.id = 313662 and b.is_deleted = 0 and b.is_active = 1


                var result = from b in fedenaEntities.batches

                             join stud in fedenaEntities.students on b.id equals stud.batch_id

                             join cor in fedenaEntities.courses on b.course_id equals cor.id 

                             where stud.user_id == userIDKey
                             && b.is_deleted == false
                             && b.is_active == true

                             select new
                             {
                                 StudentAdmissionNo = stud.admission_no,
                                 StudentFirstName = stud.first_name,
                                 StudentMiddleName = stud.middle_name,
                                 StudentLastName = stud.last_name,
                                 BatchName = b.name,
                                 CourseName = cor.course_name,
                                 StudentID = stud.id,
                                 StudentGender = stud.gender
                             };

                resultJSON = result.ToList().Distinct().ToList<object>();
            }

            return resultJSON;
        }
        


        [Route("EmployeeProfile")]
        public List<object> EmployeeProfile(int UserID)
        {
            List<user> usersList = new List<user>();

            List<object> resultJSON = new List<object>();

            int userIDKey = Convert.ToInt32(UserID);

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                usersList = (from users in fedenaEntities.users where users.id == userIDKey select users).ToList<user>();

                var result = from emp in fedenaEntities.employees

                             join emp_cat in fedenaEntities.employee_categories on emp.employee_category_id equals emp_cat.id into empcatemp
                             from emp_cat_emp in empcatemp.DefaultIfEmpty()

                             join emp_pos in fedenaEntities.employee_positions on emp.employee_position_id equals emp_pos.id into empposemp
                             from emp_pos_emp in empposemp.DefaultIfEmpty()

                             join emp_dep in fedenaEntities.employee_departments on emp.employee_department_id equals emp_dep.id into empdepemp
                             from emp_dep_emp in empdepemp.DefaultIfEmpty()

                             join u in fedenaEntities.users on emp.user_id equals u.id into uemp
                             from u_emp in uemp.DefaultIfEmpty()

                             join userMang in fedenaEntities.users on emp.reporting_manager_id equals userMang.id into userMang
                             from userMang_emp in userMang.DefaultIfEmpty()

                             where u_emp.id == userIDKey

                             select new
                             {
                                 EmployeeNumber = emp.employee_number,
                                 FirstName = emp.first_name,
                                 MiddleName = emp.middle_name,
                                 LastName = emp.last_name,
                                 Category = emp_cat_emp.name,
                                 Position = emp_pos_emp.name,
                                 Department = emp_dep_emp.name,
                                 ReportingManagaer = userMang_emp.first_name,
                                 EmployeeGender = emp.gender
                             };

                resultJSON = result.ToList<object>();
            }

            return resultJSON;
        }


        [Route("ParentProfile")]
        public List<object> ParentProfile(int UserID)
        {
            List<user> usersList = new List<user>();

            List<object> resultJSON = new List<object>();

            int userIDKey = Convert.ToInt32(UserID);

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                usersList = (from users in fedenaEntities.users where users.id == userIDKey select users).ToList<user>();

                var result = from g in fedenaEntities.guardians

                             join u in fedenaEntities.users on g.user_id equals u.id into users_guardians
                             from u_g in users_guardians.DefaultIfEmpty()

                             join stud in fedenaEntities.students on g.ward_id equals stud.sibling_id into stud_guardians
                             from s_g in stud_guardians.DefaultIfEmpty()

                             where u_g.id == userIDKey

                             select new
                             {
                                 Username = u_g.username,
                                 GuarduianFirstName = g.first_name,
                                 GuardianLastName = g.last_name,
                                 GuardianRelation = g.relation,
                                 CountChildren = stud_guardians.Count()
                             };

                resultJSON = result.ToList<object>();
            }

            return resultJSON;
        }


        [Route("SchoolConfiguration")]
        public List<object> SchoolConfiguration(int UserID)
        {
            List<object> resultJSON = new List<object>();

            int userIDKey = Convert.ToInt32(UserID);

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                var result = from c in fedenaEntities.configurations
                             join u in fedenaEntities.users on c.school_id equals u.school_id into users_schools
                             from u_s in users_schools.DefaultIfEmpty()
                             where c.config_key == "StudentAttendanceType" && u_s.id == userIDKey
                             select new
                             {
                                 ConfigurationValue = c.config_value
                             };

                resultJSON = result.ToList<object>();
            }

            return resultJSON;
        }


        [Route("PrivilegeName")]
        public List<Dictionary<string, string>> PrivilegeName(int UserID)
        {
            List<Dictionary<string,string>> resultJSON = new List<Dictionary<string, string>>();

            try
            {
                DBConnection con = new DBConnection("SELECT P.name AS PRIVILEGENAME FROM fedena.privileges P LEFT JOIN fedena.privileges_users PU ON P.ID = PU.privilege_id WHERE user_id = @userID AND P.name = 'StudentAttendanceRegister'", DBConnection.DBCommandType.PARAMETRIZEDSELECT);
                con.DbCommand.Parameters.AddWithValue("@userID", UserID);
                con.refreshParametrizedReader();
                if(con.DbReader != null)
                {
                    while (con.DbReader.Read())
                    {
                        Dictionary<string, string> PrivilegeNameDict = new Dictionary<string, string>();
                        PrivilegeNameDict.Add("PrivilegeName", con.DbReader["PRIVILEGENAME"].ToString());
                        resultJSON.Add(PrivilegeNameDict);
                    }
                }else
                {
                    Dictionary<string, string> PrivilegeNameDict = new Dictionary<string, string>();
                    PrivilegeNameDict.Add("PrivilegeName", "");
                    resultJSON.Add(PrivilegeNameDict);
                }

                con.close();
            }
            catch
            {
                Dictionary<string, string> ErrorDict = new Dictionary<string, string>();
                ErrorDict.Add("Error", "An error has occured");
                resultJSON.Add(ErrorDict);
            }


            return resultJSON;
        }


        [Route("Batches")]
        public List<object> Batches(int UserID)
        {
            List<object> resultJSON = new List<object>();

            int userIDKey = Convert.ToInt32(UserID);

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                var result = from es in fedenaEntities.employees_subjects

                             join s in fedenaEntities.subjects on es.subject_id equals s.id

                             join b in fedenaEntities.batches on s.batch_id equals b.id

                             join c in fedenaEntities.courses on b.course_id equals c.id into batches_courses
                             from c_b in batches_courses.DefaultIfEmpty()

                             join e in fedenaEntities.employees on es.employee_id equals e.id

                             where e.user_id == userIDKey
                             && b.is_active == true
                             && b.is_deleted == false

                             select new
                             {
                                 BatchID = b.id,
                                 BatchName = b.name + "-" + c_b.course_name
                             };

                resultJSON = result.Distinct().ToList<object>();
            }

            return resultJSON;
        }


        [Route("StudentAttendance")]
        public List<Dictionary<string, string>> StudentAttendance(int BatchID, string AttendanceMonthDate)
        {
            List<Dictionary<string, string>> resultJSON = new List<Dictionary<string, string>>();

            var attendanceMonthDate = Convert.ToDateTime(AttendanceMonthDate);

            try
            {
                DBConnection con = new DBConnection("SELECT S.id AS StudentID, S.first_name AS StudentFirstName, S.middle_name AS StudentMiddleName, S.last_name AS StudentLastName, S.school_id AS StudentSchoolID, M.AttendanceId AS AttendanceID, M.forenoon AS AttendanceForeNoon, M.afternoon AS AttendanceAfterNoon, M.Reason AS AttendanceReason, M.verified AS AttendanceVerified, M.batch_id AS StudentBatchID, M.latency_mins AS AttendanceLatencyMinutes FROM fedena.students S LEFT JOIN fedena.batches B ON S.batch_id = B.id LEFT JOIN(SELECT S.*, A.forenoon, A.afternoon, A.Reason, A.verified, A.id AttendanceId, A.latency_mins FROM fedena.attendances A LEFT JOIN fedena.students S ON A.student_id = S.id WHERE S.ID IN (SELECT S.id FROM fedena.students S LEFT JOIN fedena.batches B ON S.batch_id = B.id WHERE B.id = @batchID) AND month_date = @attendanceMonthDate) M ON M.id = S.id WHERE B.id = @batchID", DBConnection.DBCommandType.PARAMETRIZEDSELECT);
                con.DbCommand.Parameters.AddWithValue("@batchID", BatchID);
                con.DbCommand.Parameters.AddWithValue("@attendanceMonthDate", attendanceMonthDate);
                con.refreshParametrizedReader();

                if (con.DbReader != null)
                {
                    while (con.DbReader.Read())
                    {
                        Dictionary<string, string> StudentAttendanceDict = new Dictionary<string, string>();
                        StudentAttendanceDict.Add("StudentID", con.DbReader["StudentID"].ToString());
                        StudentAttendanceDict.Add("StudentFirstName", con.DbReader["StudentFirstName"].ToString());
                        StudentAttendanceDict.Add("StudentMiddleName", con.DbReader["StudentMiddleName"].ToString());
                        StudentAttendanceDict.Add("StudentLastName", con.DbReader["StudentLastName"].ToString());
                        StudentAttendanceDict.Add("StudentSchoolID", con.DbReader["StudentSchoolID"].ToString());
                        StudentAttendanceDict.Add("AttendanceID", con.DbReader["AttendanceID"].ToString());
                        StudentAttendanceDict.Add("AttendanceForeNoon", con.DbReader["AttendanceForeNoon"].ToString());
                        StudentAttendanceDict.Add("AttendanceAfterNoon", con.DbReader["AttendanceAfterNoon"].ToString());
                        StudentAttendanceDict.Add("AttendanceReason", con.DbReader["AttendanceReason"].ToString());
                        StudentAttendanceDict.Add("AttendanceVerified", con.DbReader["AttendanceVerified"].ToString());
                        StudentAttendanceDict.Add("StudentBatchID", con.DbReader["StudentBatchID"].ToString());
                        StudentAttendanceDict.Add("AttendanceLatencyMinutes", con.DbReader["AttendanceLatencyMinutes"].ToString());
                        resultJSON.Add(StudentAttendanceDict);
                    }
                }
                else
                {
                    Dictionary<string, string> StudentAttendanceDict = new Dictionary<string, string>();
                    StudentAttendanceDict.Add("StudentID", "");
                    StudentAttendanceDict.Add("StudentFirstName", "");
                    StudentAttendanceDict.Add("StudentMiddleName", "");
                    StudentAttendanceDict.Add("StudentLastName", "");
                    StudentAttendanceDict.Add("StudentSchoolID", "");
                    StudentAttendanceDict.Add("AttendanceID", "");
                    StudentAttendanceDict.Add("AttendanceForeNoon", "");
                    StudentAttendanceDict.Add("AttendanceAfterNoon", "");
                    StudentAttendanceDict.Add("AttendanceReason", "");
                    StudentAttendanceDict.Add("AttendanceVerified", "");
                    StudentAttendanceDict.Add("StudentBatchID", "");
                    StudentAttendanceDict.Add("AttendanceLatencyMinutes", "");
                    resultJSON.Add(StudentAttendanceDict);
                }

                con.close();
            }
            catch
            {
                Dictionary<string, string> ErrorDict = new Dictionary<string, string>();
                ErrorDict.Add("Error", "An error has occured");
                resultJSON.Add(ErrorDict);
            }

            resultJSON = resultJSON.Distinct().ToList();

            return resultJSON;
        }


        [Route("DeleteAttendance")]
        public List<Dictionary<string, string>> DeleteAttendance(int AttendanceID)
        {
            try
            {
                List<object> resultJSON = new List<object>();

                int attendanceIDKey = Convert.ToInt32(AttendanceID);

                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    var result = from a in fedenaEntities.attendances
                                 where a.id == attendanceIDKey
                                 select a;

                    foreach (var attendance in result)
                    {
                        fedenaEntities.attendances.Remove(attendance);
                    }

                    fedenaEntities.SaveChanges();

                    resultJSON = result.ToList<object>();
                }


                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                OutputMessage.Add("Message", "Success");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
            catch
            {
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                OutputMessage.Add("Message", "Error");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
        }


        [Route("UpdateAttendance")]
        public List<Dictionary<string, string>> UpdateAttendance(int AttendanceID, bool Forenoon, bool Afternoon, int LatencyMinutes)
        {
            string actionName = "Lateness";

            try
            {
                List<object> resultJSON = new List<object>();

                int attendanceIDKey = Convert.ToInt32(AttendanceID);

                int? studentID;

                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    List<attendance> result = (from a in fedenaEntities.attendances
                                               where a.id == attendanceIDKey
                                               select a).ToList();

                    foreach (var attendance in result)
                    {
                        studentID = attendance.student_id;

                        attendance.forenoon = Forenoon;
                        attendance.afternoon = Afternoon;
                        attendance.latency_mins = LatencyMinutes;
                    }

                    fedenaEntities.SaveChanges();
                    resultJSON = result.ToList<object>();

                    var resultIDs = (from a in fedenaEntities.attendances

                                     join s in fedenaEntities.students on a.student_id equals s.id

                                     join g in fedenaEntities.guardians on s.sibling_id equals g.ward_id into stud_guardians
                                     from s_g in stud_guardians.DefaultIfEmpty()

                                     where a.id == attendanceIDKey

                                     select new
                                     {
                                         Student_UserID = s.user_id,
                                         StudentID = s.id,
                                         SchoolID = s.school_id,
                                         GuardianUserID = s_g.user_id
                                     }).ToList();


                    foreach (var notification in resultIDs)
                    {
                        try
                        {
                            List<Dictionary<string, string>> DeviceInformation = GetUserToken(notification.Student_UserID, notification.SchoolID); // student

                            foreach (var device in DeviceInformation)
                            {
                                string studentToken = device["Token"];
                                string deviceType = device["DeviceType"];

                                string studentMessage = generatePushNotificationMessageLateness(null, notification.StudentID, null, notification.SchoolID, "student", actionName);
                                PushNotification(studentToken, studentMessage, deviceType, Globals.Configuration.MPNS_PRODUCTION);
                            }
                        }
                        catch { }

                        try
                        {
                            List<Dictionary<string, string>> DeviceInformation = GetUserToken(notification.GuardianUserID, notification.SchoolID); //parent

                            foreach (var device in DeviceInformation)
                            {
                                string parentToken = device["Token"];
                                string deviceType = device["DeviceType"];

                                string parentMessage = generatePushNotificationMessageLateness(notification.GuardianUserID, notification.StudentID, null, notification.SchoolID, "parent", actionName);
                                PushNotification(parentToken, parentMessage, deviceType, Globals.Configuration.MPNS_PRODUCTION);
                            }

                        }
                        catch { }
                    }

                    
                }




                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                OutputMessage.Add("Message", "Success");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
            catch
            {
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                OutputMessage.Add("Message", "Error");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
        }


        [Route("UpdateAttendanceByParent")]
        public List<Dictionary<string, string>> UpdateAttendanceByParent(int AttendanceID, string Reason)
        {
            try
            {
                List<object> resultJSON = new List<object>();

                int attendanceIDKey = Convert.ToInt32(AttendanceID);

                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    List<attendance> result = (from a in fedenaEntities.attendances
                                               where a.id == attendanceIDKey
                                               select a).ToList();

                    foreach (var attendance in result)
                    {
                        attendance.reason = Reason;
                        attendance.verified = true;
                    }

                    fedenaEntities.SaveChanges();

                    resultJSON = result.ToList<object>();
                }

                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                OutputMessage.Add("Message", "Success");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
            catch
            {
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                OutputMessage.Add("Message", "Error");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
        }


        [Route("SetAttendanceOnDailyBasis")]
        public List<Dictionary<string, string>> SetAttendanceOnDailyBasis(int[] StudentIDArray, int batchID, string AttendanceMonthDate)
        {
            string actionName = "abscence";

            try
            {
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();
                List<int> ListAttendanceID = new List<int>();

                var attendanceMonthDate = Convert.ToDateTime(AttendanceMonthDate);

                List<object> resultJSON = new List<object>();

                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    foreach (int studentID in StudentIDArray)
                    {
                        var result = (from s in fedenaEntities.students

                                      where s.id == studentID
                                      && s.batch_id == batchID

                                      select new
                                      {
                                          StudentID = s.id,
                                          Forenoon = true,
                                          Afternoon = true,
                                          Reason = "",
                                          SchoolID = s.school_id,
                                          Verified = false,
                                          LatencyMins = 0,
                                          BatchID = s.batch_id,
                                          MonthDate = attendanceMonthDate
                                      }).ToList();


                        foreach (var student in result)
                        {
                            var IfAvailableCheck = (from a in fedenaEntities.attendances

                                                    where a.student_id == student.StudentID
                                                    && a.forenoon == student.Forenoon
                                                    && a.afternoon == student.Afternoon
                                                    && a.reason == student.Reason
                                                    && a.school_id == student.SchoolID
                                                    && a.verified == student.Verified
                                                    && a.latency_mins == student.LatencyMins
                                                    && a.batch_id == student.BatchID

                                                    select new
                                                    {
                                                        AttendaceID = a.id
                                                    }).ToList();

                            if (IfAvailableCheck.Count > 0)
                            {
                                //break;
                            }

                            attendance att = new attendance();
                            att.student_id = student.StudentID;
                            att.forenoon = student.Forenoon;
                            att.afternoon = student.Afternoon;
                            att.reason = student.Reason;
                            att.school_id = student.SchoolID;
                            att.verified = student.Verified;
                            att.latency_mins = student.LatencyMins;
                            att.batch_id = student.BatchID;
                            att.month_date = student.MonthDate;

                            fedenaEntities.attendances.Add(att);

                            fedenaEntities.SaveChanges();

                            string OriginType = "daily";

                            var resultNotifications = (from s in fedenaEntities.students
                                                       join a in fedenaEntities.attendances on s.id equals a.student_id
                                                       join g in fedenaEntities.guardians on s.immediate_contact_id equals g.id into guardians_students
                                                       from g_s in guardians_students.DefaultIfEmpty()

                                                       where s.id == studentID && a.month_date == student.MonthDate
                                                       select new
                                                       {
                                                           GuardianUserID = g_s.user_id,
                                                           TextMessage = "Dear parent, your child " + s.first_name + " is absent on " + a.month_date + " Kindly verify the reason. Thanks",
                                                           OriginType = OriginType,
                                                           AttendanceID = a.id,
                                                           IsChecked = false,
                                                           CreatedAt = DateTime.Now,
                                                           SchoolID = s.school_id,
                                                           Student_UserID = s.user_id
                                                       }).ToList();

                            foreach (var notification in resultNotifications)
                            {
                                madrasatie_notifications MadrasatieNotifications = new madrasatie_notifications();
                                MadrasatieNotifications.user_id = notification.GuardianUserID;
                                MadrasatieNotifications.student_id = studentID;
                                MadrasatieNotifications.message_text = notification.TextMessage;
                                MadrasatieNotifications.origin_type = notification.OriginType;
                                MadrasatieNotifications.origin_id = notification.AttendanceID;
                                MadrasatieNotifications.is_checked = notification.IsChecked;
                                MadrasatieNotifications.created_at = notification.CreatedAt;

                                fedenaEntities.madrasatie_notifications.Add(MadrasatieNotifications);

                                fedenaEntities.SaveChanges();

                                try
                                {
                                    List<Dictionary<string, string>> DeviceInformation = GetUserToken(notification.Student_UserID, notification.SchoolID); // student

                                    foreach (var device in DeviceInformation)
                                    {
                                        string studentToken = device["Token"];
                                        string deviceType = device["DeviceType"];

                                        string studentMessage = generatePushNotificationMessage(null, studentID, null, notification.SchoolID, "student", actionName);
                                        PushNotification(studentToken, studentMessage, deviceType, Globals.Configuration.MPNS_PRODUCTION);
                                    }

                                }
                                catch { }

                                try
                                {
                                    List<Dictionary<string, string>> DeviceInformation = GetUserToken(notification.GuardianUserID, notification.SchoolID); //parent

                                    foreach(var device in DeviceInformation)
                                    {
                                        string parentToken = device["Token"];
                                        string deviceType = device["DeviceType"];

                                        string parentMessage = generatePushNotificationMessage(notification.GuardianUserID, studentID, null, notification.SchoolID, "parent", actionName);
                                        PushNotification(parentToken, parentMessage, deviceType, Globals.Configuration.MPNS_PRODUCTION);
                                    }
                                }
                                catch { }

                            }

                            int AttendanceID = (from a in fedenaEntities.attendances

                                                where a.student_id == student.StudentID
                                                && a.forenoon == student.Forenoon
                                                && a.afternoon == student.Afternoon
                                                && a.reason == student.Reason
                                                && a.school_id == student.SchoolID
                                                && a.verified == student.Verified
                                                && a.latency_mins == student.LatencyMins
                                                && a.batch_id == student.BatchID
                                                && a.month_date == student.MonthDate

                                                select new
                                                {
                                                    AttendanceID = a.id
                                                }).ToList().First().AttendanceID;

                            //ListAttendanceID.Add(AttendanceID);
                        }
                    }
                }

                OutputMessage.Add("Message", "Success");
                //OutputMessage.Add("AttendanceID", ListAttendanceID);
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
            catch
            {
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                OutputMessage.Add("Message", "Error");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
        }

        [Route("AllBatches")]
        public List<object> AllBatches(int SchoolID)
        {
            List<object> resultJSON = new List<object>();

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                var result = from b in fedenaEntities.batches

                             join c in fedenaEntities.courses on b.course_id equals c.id into batches_courses
                             from c_b in batches_courses.DefaultIfEmpty()

                             where b.school_id == SchoolID
                             && b.is_active == true
                             && b.is_deleted == false

                             select new
                             {
                                 BatchID = b.id,
                                 BatchName = b.name + "-" + c_b.course_name
                             };

                resultJSON = result.ToList<object>();
            }

            return resultJSON;
        }

        [Route("AllSubjectsForBatch")]
        public List<object> AllSubjectsForBatch(int BatchID, int? UserID = null)
        {
            List<object> resultJSON = new List<object>();

            int batchIDKey = Convert.ToInt32(BatchID);

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                var result = from s in fedenaEntities.subjects

                             join es in fedenaEntities.employees_subjects on s.id equals es.subject_id into employeesSubjects_subjects
                             from es_s in employeesSubjects_subjects.DefaultIfEmpty()

                             join e in fedenaEntities.employees on es_s.employee_id equals e.id into employeesSubjects_employees
                             from es_e in employeesSubjects_employees.DefaultIfEmpty()

                             where s.batch_id == batchIDKey
                             && es_e.user_id == (UserID == null ? es_e.user_id : UserID)

                             select new
                             {
                                 SubjectID = s.id,
                                 SubjectName = s.name
                             };

                resultJSON = result.Distinct().ToList<object>();
            }

            return resultJSON;
        }


        [Route("AllStudentsForBatchSubject")]
        public List<object> AllStudentsForBatchSubject(int BatchID, int EntryID, int WeekDayID, DateTime MonthDate, string PeriodName = null)
        {
            List<object> resultJSON = new List<object>();

            int batchIDKey = Convert.ToInt32(BatchID);

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                var subjectLeavesList = from sl in fedenaEntities.subject_leaves

                                        join ct in fedenaEntities.class_timings on sl.class_timing_id equals ct.id

                                        join te in fedenaEntities.timetable_entries on sl.class_timing_id equals te.class_timing_id

                                        join s in fedenaEntities.students on sl.student_id equals s.id

                                        where sl.month_date == MonthDate
                                        && te.weekday_id == WeekDayID
                                        && ct.name == (PeriodName == null ? ct.name : PeriodName)
                                        && ct.school_id == s.school_id
                                        && te.entry_id == EntryID

                                        select new
                                        {
                                            ID = sl.id,
                                            StudentID = sl.student_id,
                                            MonthDate = sl.month_date,
                                            SubjectID = sl.subject_id,
                                            EmployeeID = sl.employee_id,
                                            ClassTimingID = sl.class_timing_id,
                                            Reason = sl.reason,
                                            CreatedAt = sl.created_at,
                                            UpdatedAt = sl.updated_at,
                                            BatchID = sl.batch_id,
                                            SchoolID = sl.school_id,
                                            Verified = sl.verified,
                                            LatencyMins = sl.latency_mins
                                        };

                var result = from te in fedenaEntities.timetable_entries

                             join ct in fedenaEntities.class_timings on te.class_timing_id equals ct.id into timetableentries_classtimings
                             from t_c in timetableentries_classtimings.DefaultIfEmpty()

                             join s in fedenaEntities.students on te.batch_id equals s.batch_id into timetableentries_students
                             from t_s in timetableentries_students.DefaultIfEmpty()

                             join sl in subjectLeavesList on t_s.id equals sl.StudentID into timetableentries_subjectLeavesList
                             from t_sub in timetableentries_subjectLeavesList.DefaultIfEmpty()

                             where te.entry_id == EntryID
                             && te.batch_id == batchIDKey
                             && te.entry_type == "Subject"
                             && te.weekday_id == WeekDayID
                             && t_c.name == (PeriodName == null ? t_c.name : PeriodName)
                             select new
                             {
                                 StudentID = t_s.id,
                                 FullName = t_s.first_name + " " + t_s.last_name,
                                 WeekDay = (
                                 te.weekday_id == 1 ? "Monday" :
                                 te.weekday_id == 2 ? "Tuesday" :
                                 te.weekday_id == 3 ? "Wednesday" :
                                 te.weekday_id == 4 ? "Thursday" :
                                 te.weekday_id == 5 ? "Friday" :
                                 te.weekday_id == 6 ? "Saturday" :
                                 te.weekday_id == 7 ? "Sunday" : ""
                                 ),
                                 //Period = t_c.name,
                                 AttendanceID = t_sub.ID == null ? "" : t_sub.ID.ToString(),
                                 Reason = t_sub.Reason,
                                 Verified = t_sub.Verified,
                                 LatencyMinutes = t_sub.LatencyMins == null ? 0 : t_sub.LatencyMins,
                                 LatencyMonthDate = t_sub.MonthDate
                             };

                resultJSON = result.Distinct().ToList<object>();
            }

            return resultJSON;
        }



        [Route("NewSubjectLeaves")]
        public List<Dictionary<string,string>> NewSubjectLeaves(int[] StudentIDArray, int BatchID, int EntryID, DateTime MonthDate, string PeriodName, int WeekDayID)
        {
            string actionName = "abscence";
            try
            {
                List<object> resultJSON = new List<object>();
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    foreach (int studentID in StudentIDArray) {
                        var result = (from s in fedenaEntities.students

                                      join te in fedenaEntities.timetable_entries on s.batch_id equals te.batch_id into timetableentries_students
                                      from t_s in timetableentries_students.DefaultIfEmpty()

                                      join ct in fedenaEntities.class_timings on t_s.class_timing_id equals ct.id into timetableentries_class_timings
                                      from ct_te in timetableentries_class_timings.DefaultIfEmpty()

                                      where s.id == studentID
                                      && s.batch_id == BatchID
                                      && t_s.entry_id == EntryID
                                      && ct_te.name == PeriodName
                                      && ct_te.school_id == s.school_id
                                      && t_s.weekday_id == WeekDayID

                                      select new
                                      {
                                          ID = s.id,
                                          MonthDate = MonthDate,
                                          EntryID = t_s.entry_id,
                                          ClassTimingID = ct_te.id,
                                          PeriodName = ct_te.name,
                                          Reason = "",
                                          BatchID = s.batch_id,
                                          SchoolID = s.school_id,
                                          Verified = false,
                                          LatencyMinutes = 0
                                      }).ToList();

                        foreach (var student in result)
                        {
                            var IfAvailableCheck = (from sl in fedenaEntities.subject_leaves

                                                    join te in fedenaEntities.timetable_entries on sl.batch_id equals te.batch_id into timetableentries_students
                                                    from t_s in timetableentries_students.DefaultIfEmpty()

                                                    join ct in fedenaEntities.class_timings on t_s.class_timing_id equals ct.id into timetableentries_class_timings
                                                    from ct_te in timetableentries_class_timings.DefaultIfEmpty()

                                                    where sl.student_id == student.ID
                                                    && sl.subject_id == student.EntryID
                                                    && sl.class_timing_id == student.ClassTimingID
                                                    && sl.reason == student.Reason
                                                    && sl.batch_id == student.BatchID
                                                    && sl.school_id == student.SchoolID
                                                    && sl.verified == student.Verified
                                                    && sl.latency_mins == student.LatencyMinutes
                                                    && sl.latency_mins == student.LatencyMinutes
                                                    && t_s.entry_id == EntryID
                                                    && ct_te.name == PeriodName
                                                    && t_s.weekday_id == WeekDayID

                                                    select new
                                                    {
                                                        SubjectLeavesID = sl.id
                                                    }).ToList();

                            if (IfAvailableCheck.Count > 0)
                            {
                                //break;
                            }

                            subject_leaves subjectLeaves = new subject_leaves();

                            subjectLeaves.student_id = student.ID;
                            subjectLeaves.month_date = student.MonthDate;
                            subjectLeaves.subject_id = student.EntryID;
                            subjectLeaves.class_timing_id = student.ClassTimingID;
                            subjectLeaves.reason = student.Reason;
                            subjectLeaves.batch_id = student.BatchID;
                            subjectLeaves.school_id = student.SchoolID;
                            subjectLeaves.verified = student.Verified;
                            subjectLeaves.latency_mins = student.LatencyMinutes;

                            fedenaEntities.subject_leaves.Add(subjectLeaves);

                            fedenaEntities.SaveChanges();
                            
                            string OriginType = "subjectwise";

                            var resultNotifications = (from s in fedenaEntities.students

                                                       join sl in fedenaEntities.subject_leaves on s.id equals sl.student_id into subjectLeaves_students
                                                       from stu_sl in subjectLeaves_students.DefaultIfEmpty()

                                                       join sub in fedenaEntities.subjects on stu_sl.subject_id equals sub.id into subjects_subjectLeaves
                                                       from s_sl in subjects_subjectLeaves.DefaultIfEmpty()

                                                       join g in fedenaEntities.guardians on s.immediate_contact_id equals g.id into guardians_students
                                                       from g_s in guardians_students.DefaultIfEmpty()

                                                       where s.id == studentID
                                                       && stu_sl.month_date == student.MonthDate

                                                       select new
                                                       {
                                                           GuardianUserID = g_s.user_id,
                                                           TextMessage = "Dear parent, your child " + s.first_name + " is absent on " + stu_sl.month_date + " for subject " + s_sl.name + " during period " + student.PeriodName + ", Kindly verify the reason. Thanks",
                                                           OriginType = OriginType,
                                                           SubjectLeavesID = stu_sl.id,
                                                           IsChecked = false,
                                                           CreatedAt = DateTime.Now,
                                                           SchoolID = s.school_id,
                                                           Student_UserID = s.user_id
                                                       }).ToList();

                            foreach (var notification in resultNotifications)
                            {
                                madrasatie_notifications MadrasatieNotifications = new madrasatie_notifications();
                                MadrasatieNotifications.user_id = notification.GuardianUserID;
                                MadrasatieNotifications.student_id = studentID;
                                MadrasatieNotifications.message_text = notification.TextMessage;
                                MadrasatieNotifications.origin_type = notification.OriginType;
                                MadrasatieNotifications.origin_id = notification.SubjectLeavesID;
                                MadrasatieNotifications.is_checked = notification.IsChecked;
                                MadrasatieNotifications.created_at = notification.CreatedAt;

                                fedenaEntities.madrasatie_notifications.Add(MadrasatieNotifications);

                                fedenaEntities.SaveChanges();

                                try
                                {
                                    List<Dictionary<string, string>> DeviceInformation = GetUserToken(notification.Student_UserID, notification.SchoolID); // student
                                    
                                    foreach(var device in DeviceInformation)
                                    {
                                        string studentToken = device["Token"];
                                        string deviceType = device["DeviceType"];

                                        string studentMessage = generatePushNotificationMessage(null, studentID, null, notification.SchoolID, "student", actionName);
                                        PushNotification(studentToken, studentMessage, deviceType, Globals.Configuration.MPNS_PRODUCTION);
                                    }
                                }
                                catch { }

                                try
                                {
                                    List<Dictionary<string, string>> DeviceInformation = GetUserToken(notification.GuardianUserID, notification.SchoolID); //parent

                                    foreach(var device in DeviceInformation)
                                    {
                                        string parentToken = device["Token"];
                                        string deviceType = device["DeviceType"];

                                        string parentMessage = generatePushNotificationMessage(notification.GuardianUserID, studentID, null, notification.SchoolID, "parent", actionName);
                                        PushNotification(parentToken, parentMessage, deviceType, Globals.Configuration.MPNS_PRODUCTION);
                                    }

                                }
                                catch { }
                            }
                        }

                        if(result.Count == 0)
                        {
                            OutputMessage.Add("Message", "No Data Inserted");
                            OutputMessageList.Add(OutputMessage);

                            return OutputMessageList;
                        }

                    }
                }

                OutputMessage.Add("Message", "Success");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
            catch
            {
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                OutputMessage.Add("Message", "Error");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
        }



        [Route("UpdateSubjectLeaves")]
        public List<Dictionary<string, string>> UpdateSubjectLeaves(int SubjectLeavesID, decimal LatencyMinutes)
        {
            string actionName = "Lateness";

            try
            {
                List<object> resultJSON = new List<object>();

                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    List<subject_leaves> result = (from sl in fedenaEntities.subject_leaves where sl.id == SubjectLeavesID select sl).ToList();

                    foreach (var subjectLeaves in result)
                    {
                        subjectLeaves.latency_mins = LatencyMinutes;
                    }

                    fedenaEntities.SaveChanges();

                    resultJSON = result.ToList<object>();
                    
                    var resultIDs = (from sl in fedenaEntities.subject_leaves

                                     join s in fedenaEntities.students on sl.student_id equals s.id

                                     join g in fedenaEntities.guardians on s.sibling_id equals g.ward_id into stud_guardians
                                     from s_g in stud_guardians.DefaultIfEmpty()

                                     where sl.id == SubjectLeavesID

                                     select new
                                     {
                                         Student_UserID = s.user_id,
                                         StudentID = s.id,
                                         SchoolID = s.school_id,
                                         GuardianUserID = s_g.user_id
                                     }).ToList();


                    foreach (var notification in resultIDs)
                    {
                        try
                        {
                            List<Dictionary<string, string>> DeviceInformation = GetUserToken(notification.Student_UserID, notification.SchoolID); // student

                            foreach (var device in DeviceInformation)
                            {
                                string studentToken = device["Token"];
                                string deviceType = device["DeviceType"];

                                string studentMessage = generatePushNotificationMessageLateness(null, notification.StudentID, null, notification.SchoolID, "student", actionName);
                                PushNotification(studentToken, studentMessage, deviceType, Globals.Configuration.MPNS_PRODUCTION);
                            }
                        }
                        catch { }

                        try
                        {
                            List<Dictionary<string, string>> DeviceInformation = GetUserToken(notification.GuardianUserID, notification.SchoolID); //parent

                            foreach (var device in DeviceInformation)
                            {
                                string parentToken = device["Token"];
                                string deviceType = device["DeviceType"];

                                string parentMessage = generatePushNotificationMessageLateness(notification.GuardianUserID, notification.StudentID, null, notification.SchoolID, "parent", actionName);
                                PushNotification(parentToken, parentMessage, deviceType, Globals.Configuration.MPNS_PRODUCTION);
                            }

                        }
                        catch { }
                    }
                }

                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                OutputMessage.Add("Message", "Success");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
            catch
            {
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                OutputMessage.Add("Message", "Error");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
        }



        [Route("UpdateSubjectLeavesForParents")]
        public List<Dictionary<string, string>> UpdateSubjectLeavesForParents(int SubjectLeavesID, string Reason)
        {
            try
            {
                List<object> resultJSON = new List<object>();

                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    List<subject_leaves> result = (from sl in fedenaEntities.subject_leaves where sl.id == SubjectLeavesID select sl).ToList();

                    foreach (var subjectLeaves in result)
                    {
                        subjectLeaves.reason = Reason;
                        subjectLeaves.verified = true;
                    }

                    fedenaEntities.SaveChanges();

                    resultJSON = result.ToList<object>();
                }

                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                OutputMessage.Add("Message", "Success");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
            catch
            {
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                OutputMessage.Add("Message", "Error");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
        }


        [Route("StudentByUser")]
        public List<object> StudentByUser(int UserID)
        {
            List<object> resultJSON = new List<object>();

            int userIDKey = Convert.ToInt32(UserID);

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                var result = from s in fedenaEntities.students

                             join u in fedenaEntities.users on s.user_id equals u.id into users_students
                             from s_u in users_students.DefaultIfEmpty()

                             where s_u.id == userIDKey

                             select new
                             {
                                 StudentID = s_u.id,
                                 StudentFirstName = s_u.first_name,
                                 StudentLastName = s_u.last_name
                             };

                resultJSON = result.ToList<object>();
            }

            return resultJSON;
        }
        


        [Route("EditPassword")]
        public List<Dictionary<string, string>> EditPassword(int UserID, string HashedPassword)
        {
            try
            {
                List<object> resultJSON = new List<object>();

                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    List<user> result = (from u in fedenaEntities.users
                                               where u.id == UserID
                                               select u).ToList();

                    foreach (var user in result)
                    {
                        user.hashed_password = HashedPassword.Trim();
                    }

                    fedenaEntities.SaveChanges();

                    resultJSON = result.ToList<object>();
                }

                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                OutputMessage.Add("Message", "Success");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
            catch
            {
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                OutputMessage.Add("Message", "Error");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
        }



        [Route("SchoolConfigurationAndUserPrivilege")]
        public List<Dictionary<string, string>> SchoolConfigurationAndUserPrivilege(int UserID)
        {
            List<Dictionary<string, string>> PrivilegeNameDictList = new List<Dictionary<string, string>>();

            List<object> ConfigurationValueList = new List<object>();
            //Dictionary<string,string> ConfigurationValueDictionary = new Dictionary<string, string>();
            string Configuration = string.Empty;

            int userIDKey = Convert.ToInt32(UserID);

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                var ConfigurationValue = from c in fedenaEntities.configurations
                             join u in fedenaEntities.users on c.school_id equals u.school_id into users_schools
                             from u_s in users_schools.DefaultIfEmpty()
                             where c.config_key == "StudentAttendanceType" && u_s.id == userIDKey
                             //where u_s.id == userIDKey
                             select new
                             {
                                 ConfigurationValue = c.config_value
                             };

                ConfigurationValueList = ConfigurationValue.ToList<object>();

                Configuration = ConfigurationValue.First().ConfigurationValue;
                //ConfigurationValueDictionary.Add("ConfigurationValue", ConfigurationValue.First().ConfigurationValue);
            }

            try
            {
                DBConnection con = new DBConnection("SELECT P.ID AS PRIVILEGEID,P.name AS PRIVILEGENAME FROM fedena.privileges P LEFT JOIN fedena.privileges_users PU ON P.ID = PU.privilege_id WHERE user_id = @userID", DBConnection.DBCommandType.PARAMETRIZEDSELECT);
                //DBConnection con = new DBConnection("SELECT P.ID AS PRIVILEGEID,P.name AS PRIVILEGENAME FROM fedena.privileges P LEFT JOIN fedena.privileges_users PU ON P.ID = PU.privilege_id WHERE user_id = @userID AND P.name = 'StudentAttendanceRegister'", DBConnection.DBCommandType.PARAMETRIZEDSELECT);
                con.DbCommand.Parameters.AddWithValue("@userID", UserID);
                con.refreshParametrizedReader();
                if (con.DbReader != null)
                {
                    while (con.DbReader.Read())
                    {
                        Dictionary<string, string> PrivilegeNameDict = new Dictionary<string, string>();
                        PrivilegeNameDict.Add("ConfigurationValue", Configuration);
                        PrivilegeNameDict.Add("PrivilegeID", con.DbReader["PRIVILEGEID"].ToString());
                        PrivilegeNameDict.Add("PrivilegeName", con.DbReader["PRIVILEGENAME"].ToString());

                        PrivilegeNameDictList.Add(PrivilegeNameDict);
                    }
                }
                else
                {
                    Dictionary<string, string> PrivilegeNameDict = new Dictionary<string, string>();
                    PrivilegeNameDict.Add("ConfigurationValue", Configuration);
                    PrivilegeNameDict.Add("PrivilegeID", "");
                    PrivilegeNameDict.Add("PrivilegeName", "");
                    
                    PrivilegeNameDictList.Add(PrivilegeNameDict);
                }

                con.close();
            }
            catch
            {
                Dictionary<string, string> ErrorDict = new Dictionary<string, string>();
                ErrorDict.Add("Error", "An error has occured");
                PrivilegeNameDictList.Add(ErrorDict);
            }


            return PrivilegeNameDictList;
        }


        //User/parentProfile?UserID=1498787
        [Route("ChildrenForParent")]
        public List<object> ChildrenForParent(int UserID)
        {
            List<object> resultJSON = new List<object>();

            int userIDKey = Convert.ToInt32(UserID);

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                var result = from s in fedenaEntities.students

                             join g in fedenaEntities.guardians on s.sibling_id equals g.ward_id into guardians_students
                             from g_s in guardians_students.DefaultIfEmpty()

                             join u in fedenaEntities.users on g_s.user_id equals u.id into users_guardians
                             from u_g in users_guardians.DefaultIfEmpty()

                             where u_g.id == userIDKey

                             select new
                             {
                                 StudentID = s.id,
                                 StudentAdmissionNumber = s.admission_no,
                                 StudentFirstName = s.first_name,
                                 UserID = s.user_id
                             };

                resultJSON = result.ToList<object>();
            }

            return resultJSON;
        }

        
        [Route("GetPeriods")]
        public List<object> GetPeriods(int BatchID, int EntryID, int WeekDayID)
        {
            List<object> resultJSON = new List<object>();

            int batchIDKey = Convert.ToInt32(BatchID);

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                var result = from te in fedenaEntities.timetable_entries

                             join ct in fedenaEntities.class_timings on te.class_timing_id equals ct.id into timetableentries_classtimings
                             from t_c in timetableentries_classtimings.DefaultIfEmpty()

                             where te.entry_id == EntryID
                             && te.batch_id == batchIDKey
                             && te.weekday_id == WeekDayID
                             select new
                             {
                                 WeekDay = (
                                 te.weekday_id == 1 ? "Monday" :
                                 te.weekday_id == 2 ? "Tuesday" :
                                 te.weekday_id == 3 ? "Wednesday" :
                                 te.weekday_id == 4 ? "Thursday" :
                                 te.weekday_id == 5 ? "Friday" :
                                 te.weekday_id == 6 ? "Saturday" :
                                 te.weekday_id == 7 ? "Sunday" : ""
                                 ),
                                 Period = t_c.name,
                                 StartTime = t_c.start_time,
                                 EndTime = t_c.end_time
                             };

                resultJSON = result.ToList<object>();
            }

            return resultJSON;
        }


        [Route("GetAbsenceDetailsAttendance")]
        public List<object> GetAbsenceDetailsAttendance(int AttendanceID)
        {
            List<object> resultJSON = new List<object>();

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                var result = from att in fedenaEntities.attendances
                             where att.id == AttendanceID
                             select new
                             {
                                 StudentID = att.student_id,
                                 PeriodTableEntryID = att.period_table_entry_id,
                                 Forenoon = att.forenoon,
                                 Afternoon = att.afternoon,
                                 Reason = att.reason,
                                 MonthDate = att.month_date,
                                 BatchID = att.batch_id,
                                 UpdatedAt = att.updated_at,
                                 CreatedAt = att.created_at,
                                 SchoolID = att.school_id,
                                 Verified = att.verified,
                                 LatencyMinutes = att.latency_mins
                             };

                resultJSON = result.ToList<object>();
            }

            return resultJSON;
        }


        [Route("GetAbsenceDetailsSubject")]
        public List<object> GetAbsenceDetailsSubject(int SubjectLeavesID)
        {
            List<object> resultJSON = new List<object>();

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                var result = from sl in fedenaEntities.subject_leaves
                             where sl.id == SubjectLeavesID
                             select new
                             {
                                 StudentID = sl.student_id,
                                 MonthDate = sl.month_date,
                                 SubjectID = sl.subject_id,
                                 EmployeeID = sl.employee_id,
                                 ClassTimingID = sl.class_timing_id,
                                 Reason = sl.reason,
                                 CreatedAt = sl.created_at,
                                 UpdatedAt = sl.updated_at,
                                 BatchID = sl.batch_id,
                                 SchoolID = sl.school_id,
                                 Verified = sl.verified,
                                 LatencyMinutes = sl.latency_mins
                             };

                resultJSON = result.ToList<object>();
            }

            return resultJSON;
        }


        [Route("DeleteFromSubjectLeaves")]
        public List<Dictionary<string,string>> DeleteFromSubjectLeaves(int SubjectLeavesID)
        {
            try
            {
                List<object> resultJSON = new List<object>();

                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    var result = from sl in fedenaEntities.subject_leaves
                                 where sl.id == SubjectLeavesID
                                 select sl;

                    foreach (var subjectLeaves in result)
                    {
                        fedenaEntities.subject_leaves.Remove(subjectLeaves);
                    }

                    fedenaEntities.SaveChanges();

                    resultJSON = result.ToList<object>();
                }


                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                OutputMessage.Add("Message", "Success");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
            catch
            {
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                OutputMessage.Add("Message", "Error");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
        }

        [Route("GetAttendanceHistoryForSubject")]
        public List<object> GetAttendanceHistoryForSubject(int UserID)
        {
            List<object> resultJSON = new List<object>();

            int userIDKey = Convert.ToInt32(UserID);

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                var result = from u in fedenaEntities.users

                             join s in fedenaEntities.students on u.id equals s.user_id into users_students
                             from u_s in users_students.DefaultIfEmpty()

                             join sl in fedenaEntities.subject_leaves on u_s.id equals sl.student_id into users_subjectleaves
                             from u_sl in users_subjectleaves.DefaultIfEmpty()

                             join ct in fedenaEntities.class_timings on u_sl.class_timing_id equals ct.id into classTimings_subjectLeaves
                             from ct_sl in classTimings_subjectLeaves.DefaultIfEmpty()

                             join sub in fedenaEntities.subjects on u_sl.subject_id equals sub.id into subjects_subjectleaves
                             from sub_sl in subjects_subjectleaves.DefaultIfEmpty()

                             where u.id == userIDKey

                             select new
                             {
                                 StudentLeaveID = u_sl.id,
                                 StudentID = u_sl.student_id,
                                 MonthDate = u_sl.month_date,
                                 SubjectID = u_sl.subject_id,
                                 EmployeeID = u_sl.employee_id,
                                 ClassTimingID = u_sl.class_timing_id,
                                 Reason = u_sl.reason,
                                 CreatedAt = u_sl.created_at,
                                 UpdatedAt = u_sl.updated_at,
                                 BatchID = u_sl.batch_id,
                                 SchoolID = u_sl.school_id,
                                 Verified = u_sl.verified,
                                 LatencyMinutes = u_sl.latency_mins,
                                 Period = ct_sl.name,
                                 SubjectName = sub_sl.name
                             };

                resultJSON = result.ToList<object>();
            }

            return resultJSON;
        }


        [Route("GetAttendanceHistoryForDaily")]
        public List<object> GetAttendanceHistoryForDaily(int UserID)
        {
            List<object> resultJSON = new List<object>();

            int userIDKey = Convert.ToInt32(UserID);

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                var result = from u in fedenaEntities.users

                             join s in fedenaEntities.students on u.id equals s.user_id into users_students
                             from u_s in users_students.DefaultIfEmpty()

                             join att in fedenaEntities.attendances on u_s.id equals att.student_id into users_subjectleaves
                             from u_att in users_subjectleaves.DefaultIfEmpty()

                             where u.id == userIDKey

                             select new
                             {
                                 AttendanceID = u_att.id,
                                 StudentID = u_att.student_id,
                                 PeriodTableEntryID = u_att.period_table_entry_id,
                                 Forenoon = u_att.forenoon,
                                 Afternoon = u_att.afternoon,
                                 Reason = u_att.reason,
                                 MonthDate = u_att.month_date,
                                 BatchID = u_att.batch_id,
                                 UpdatedAt = u_att.updated_at,
                                 CreatedAt = u_att.created_at,
                                 SchoolID = u_att.school_id,
                                 Verified = u_att.verified,
                                 LatencyMinutes = u_att.latency_mins,
                             };

                resultJSON = result.ToList<object>();
            }

            return resultJSON;
        }


        [Route("AbsenceInformationFromParentForDailyBasis")]
        public List<object> AbsenceInformationFromParentForDailyBasis(int StudentID, bool Forenoon, bool Afternoon, string Reason, DateTime MonthDate)
        {
            List<object> resultJSON = new List<object>();
            
            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                var result = from s in fedenaEntities.students
                             where s.id == StudentID

                             select new
                             {
                                 StudentID = s.id,
                                 SchoolID = s.school_id,
                                 Verified = true,
                                 BatchID = s.batch_id
                             };

                foreach (var student in result)
                {
                    attendance newAttendance = new attendance();
                    newAttendance.student_id = student.StudentID;
                    newAttendance.forenoon = Forenoon;
                    newAttendance.afternoon = Afternoon;
                    newAttendance.reason = Reason;
                    newAttendance.school_id = student.SchoolID;
                    newAttendance.verified = student.Verified;
                    newAttendance.batch_id = student.BatchID;
                    newAttendance.month_date = MonthDate;

                    fedenaEntities.attendances.Add(newAttendance);
                }

                fedenaEntities.SaveChanges();

                resultJSON = result.ToList<object>();
            }

            return resultJSON;
        }


        [Route("AbsenceInformationFromParentForSubjectWise")]
        public List<object> AbsenceInformationFromParentForSubjectWise(int StudentID, string Reason, DateTime MonthDate)
        {
            List<object> resultJSON = new List<object>();

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                var result = from s in fedenaEntities.students
                             where s.id == StudentID

                             select new
                             {
                                 StudentID = s.id,
                                 SchoolID = s.school_id,
                                 Verified = true,
                             };

                foreach (var student in result)
                {
                    subject_leaves newSubjectLeave = new subject_leaves();
                    newSubjectLeave.student_id = student.StudentID;
                    newSubjectLeave.month_date = MonthDate;
                    newSubjectLeave.reason = Reason;
                    newSubjectLeave.school_id = student.SchoolID;
                    newSubjectLeave.verified = student.Verified;

                    fedenaEntities.subject_leaves.Add(newSubjectLeave);
                }

                fedenaEntities.SaveChanges();

                resultJSON = result.ToList<object>();
            }

            return resultJSON;
        }




        [Route("WeekDaysForSubject")]
        public List<object> WeekDaysForSubject(int BatchID, int EntryID)
        {
            List<object> resultJSON = new List<object>();

            int batchIDKey = Convert.ToInt32(BatchID);

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                var result = from te in fedenaEntities.timetable_entries

                             where te.entry_id == EntryID
                             && te.batch_id == batchIDKey

                             select new
                             {
                                 WeekDay = (
                                 te.weekday_id == 1 ? "Monday" :
                                 te.weekday_id == 2 ? "Tuesday" :
                                 te.weekday_id == 3 ? "Wednesday" :
                                 te.weekday_id == 4 ? "Thursday" :
                                 te.weekday_id == 5 ? "Friday" :
                                 te.weekday_id == 6 ? "Saturday" :
                                 te.weekday_id == 7 ? "Sunday" : ""
                                 )
                             };

                //resultJSON = result.ToList<object>();
                resultJSON = result.Distinct().ToList<object>();
            }

            return resultJSON;
        }


        [Route("GetAssignmentByStudent")]
        public List<object> GetAssignmentByStudent(string StudentID, int SchoolID, DateTime? DueDate = null)
        {
            List<object> resultJSON = new List<object>();

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                bool? PublishAgenda = (from msc in fedenaEntities.madrasatie_system_configs where msc.school_id == SchoolID select msc.publish_agenda).ToList()[0];

                if (DueDate == null)
                {
                    var result = (from a in fedenaEntities.assignments

                                  join s in fedenaEntities.subjects on a.subject_id equals s.id into assignment_subject
                                  from a_s in assignment_subject.DefaultIfEmpty()

                                  join sc in fedenaEntities.school_domains on a_s.school_id equals sc.linkable_id into assignment_school
                                  from a_sc in assignment_school.DefaultIfEmpty()

                                  where a.student_list.Contains(StudentID)
                                  && a.is_published == (PublishAgenda == true ? true : a.is_published)

                                  select new
                                  {
                                      SubjectID = a_s.id,
                                      SubjectName = a_s.name,
                                      AssignmentID = a.id,
                                      AssignmentTitle = a.title,
                                      AssignmentContent = a.content,
                                      AssignmentDuedate = a.duedate,
                                      AttachmentFileName = a.attachment_file_name,
                                      AssignmentType = a.assignment_type,
                                      AttachmentPath = Globals.Configuration.STORAGE_LOCATION_DOWNLOAD + a_sc.domain + "/assignments/download_attachment/" + a.id + "/" + a.attachment_file_name
                                  }).ToList();

                    resultJSON = result.ToList<object>();
                }
                else
                {
                    var result = (from a in fedenaEntities.assignments

                                  join s in fedenaEntities.subjects on a.subject_id equals s.id into assignment_subject
                                  from a_s in assignment_subject.DefaultIfEmpty()

                                  join sc in fedenaEntities.school_domains on a_s.school_id equals sc.linkable_id into assignment_school
                                  from a_sc in assignment_school.DefaultIfEmpty()

                                  where a.student_list.Contains(StudentID)
                                  && a.duedate == DueDate
                                  && a.is_published == (PublishAgenda == true ? true : a.is_published)

                                  select new
                                  {
                                      SubjectID = a_s.id,
                                      SubjectName = a_s.name,
                                      AssignmentID = a.id,
                                      AssignmentTitle = a.title,
                                      AssignmentContent = a.content,
                                      AssignmentDuedate = a.duedate,
                                      AttachmentFileName = a.attachment_file_name,
                                      AssignmentType = a.assignment_type,
                                      AttachmentPath = Globals.Configuration.STORAGE_LOCATION_DOWNLOAD + "/assignments/download_attachment/" + a.id + "/" + a.attachment_file_name
                                  }).ToList();

                    resultJSON = result.ToList<object>();
                }

            }

            return resultJSON;
        }


        //[Route("GetAssignmentByStudent")]
        //public List<object> GetAssignmentByStudent(string StudentID, DateTime? DueDate = null)
        //{
        //    List<object> resultJSON = new List<object>();

        //    using (fedenaEntities fedenaEntities = new fedenaEntities())
        //    {
        //        if (DueDate == null)
        //        {
        //            var result = (from a in fedenaEntities.assignments

        //                          join s in fedenaEntities.subjects on a.subject_id equals s.id into assignment_subject
        //                          from a_s in assignment_subject.DefaultIfEmpty()

        //                          join sc in fedenaEntities.school_domains on a_s.school_id equals sc.linkable_id into assignment_school
        //                          from a_sc in assignment_school.DefaultIfEmpty()

        //                          where a.student_list.Contains(StudentID)

        //                          select new
        //                          {
        //                              SubjectID = a_s.id,
        //                              SubjectName = a_s.name,
        //                              AssignmentID = a.id,
        //                              AssignmentTitle = a.title,
        //                              AssignmentContent = a.content,
        //                              AssignmentDuedate = a.duedate,
        //                              AttachmentFileName = a.attachment_file_name,
        //                              AssignmentType = a.assignment_type,
        //                              AttachmentPath = "http://" + a_sc.domain + "/assignments/download_attachment/" + a.id.ToString()
        //                          }).ToList();

        //            resultJSON = result.ToList<object>();
        //        }
        //        else
        //        {
        //            var result = (from a in fedenaEntities.assignments

        //                          join s in fedenaEntities.subjects on a.subject_id equals s.id into assignment_subject
        //                          from a_s in assignment_subject.DefaultIfEmpty()

        //                          join sc in fedenaEntities.school_domains on a_s.school_id equals sc.linkable_id into assignment_school
        //                          from a_sc in assignment_school.DefaultIfEmpty()

        //                          where a.student_list.Contains(StudentID)
        //                          && a.duedate == DueDate

        //                          select new
        //                          {
        //                              SubjectID = a_s.id,
        //                              SubjectName = a_s.name,
        //                              AssignmentID = a.id,
        //                              AssignmentTitle = a.title,
        //                              AssignmentContent = a.content,
        //                              AssignmentDuedate = a.duedate,
        //                              AttachmentFileName = a.attachment_file_name,
        //                              AssignmentType = a.assignment_type,
        //                              //AttachmentPath = Globals.Configuration.STORAGE_LOCATION_DOWNLOAD + a_sc.domain + "/assignments/download_attachment/" + a.id + "/" + a.attachment_file_name
        //                              AttachmentPath = Globals.Configuration.STORAGE_LOCATION_DOWNLOAD + "/assignments/download_attachment/" + a.id + "/" + a.attachment_file_name
        //                          }).ToList();

        //            resultJSON = result.ToList<object>();
        //        }

        //    }

        //    return resultJSON;
        //}


        [Route("AssignmentAnswer")]
        public List<object> AssignmentAnswer(int AssignmentID, int StudentID, int? FileSizeInBytes, Dictionary<string, object> FileBase64, string AttachmentFileName, string AttachmentContentType, string Username = "", string FreeTextPassword = "")
        {
            List<object> resultJSON = new List<object>();
            List<Dictionary<string, string>> ListResultDictionary = new List<Dictionary<string, string>>();

            string Base64File = string.Empty;

            try
            {
                Base64File = FileBase64.Where(x => x.Key == "File").Select(x => x.Value).ToList()[0].ToString();
            }
            catch { }

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                var result = (from a in fedenaEntities.assignments

                             where a.id == AssignmentID

                             select new
                             {
                                 AssignmentID = a.id,
                                 StudentID = StudentID,
                                 AssignmentTitle = a.title,
                                 AssignmentContent = a.content,
                                 AttachmentFileName = a.attachment_file_name,
                                 AttachmentContentType = a.attachment_content_type,
                                 AttachmentFileSize = FileSizeInBytes,
                                 CreatedAt = DateTime.Now,
                                 SchoolID = a.school_id
                             }).ToList();

                foreach (var assignment in result)
                {
                    //Dictionary<string, string> resultDictionary = new Dictionary<string, string>();
                    string Path = string.Empty;

                    assignment_answers AssignmentAnswers = new assignment_answers();
                    AssignmentAnswers.assignment_id = assignment.AssignmentID;
                    AssignmentAnswers.student_id = assignment.StudentID;
                    AssignmentAnswers.title = assignment.AssignmentTitle;
                    AssignmentAnswers.content = assignment.AssignmentContent;
                    AssignmentAnswers.attachment_file_name = assignment.AttachmentFileName;
                    AssignmentAnswers.attachment_content_type = assignment.AttachmentContentType;
                    AssignmentAnswers.attachment_file_size = assignment.AttachmentFileSize;
                    AssignmentAnswers.created_at = assignment.CreatedAt;
                    AssignmentAnswers.school_id = assignment.SchoolID;

                    fedenaEntities.assignment_answers.Add(AssignmentAnswers);

                    fedenaEntities.SaveChanges();
                }

                var resultAnswers = (from a in fedenaEntities.assignments

                                     join ans in fedenaEntities.assignment_answers on a.id equals ans.assignment_id into assignments_assignmentAnswers
                                     from a_ans in assignments_assignmentAnswers.DefaultIfEmpty()

                                     join sd in fedenaEntities.school_domains on a.school_id equals sd.linkable_id into schools_assignments
                                     from sc_a in schools_assignments.DefaultIfEmpty()

                                     where a.id == AssignmentID

                                     select new
                                     {
                                         AssignmentID = a.id,
                                         StudentID = StudentID,
                                         AssignmentTitle = a.title,
                                         AssignmentContent = a.content,
                                         AttachmentFileName = a.attachment_file_name,
                                         AttachmentContentType = a.attachment_content_type,
                                         AttachmentFileSize = FileSizeInBytes,
                                         CreatedAt = DateTime.Now,
                                         SchoolID = a.school_id,
                                         AnswerID = a_ans.id,
                                         SchoolDomain = sc_a.domain
                              }).ToList();

                foreach (var assignment in resultAnswers)
                {
                    //Dictionary<string, string> resultDictionary = new Dictionary<string, string>();
                    string Path = string.Empty;

                    //http://bis1.madrasatie.net/assignment_answers/download_attachment?assignment_answer=17098
                    Path = "http://" + assignment.SchoolDomain + "/assignment_answers/download_attachment?assignment_answer=" + assignment.AssignmentID.ToString();

                    //resultDictionary.Add("AssignmentAnswerPath", Path);

                    //ListResultDictionary.Add(resultDictionary);
                }

                var SchoolUrl = resultAnswers.First().SchoolDomain.ToString();
                SchoolUrl = SchoolUrl.Substring(SchoolUrl.Length - 1) == "/" ? SchoolUrl : SchoolUrl + "/";

                int? schoolID = resultAnswers.First().SchoolID;

                var oAuthClientsList = (from authClient in fedenaEntities.oauth_clients
                                        where authClient.school_id == schoolID
                                        && authClient.name == "MobileApp"
                                        select new
                                        {
                                            authClient.client_id,
                                            authClient.client_secret
                                        }).ToList();

                var ClientID = oAuthClientsList.First().client_id;
                var ClientSecret = oAuthClientsList.First().client_secret;

                var token = GetSchoolToken(Username, FreeTextPassword, ClientID, ClientSecret, SchoolUrl);

                if (AttachmentFileName.Count() > 0)
                {
                    foreach (var assignment in resultAnswers)
                    {
                        string baseUrl = string.Empty;

                        var assignmentID = assignment.AssignmentID;
                        var SchoolDomain = assignment.SchoolDomain;

                        SchoolDomain = SchoolDomain.Substring(SchoolDomain.Length - 1) == "/" ? SchoolDomain : SchoolDomain + "/";
                        baseUrl = SchoolDomain.Contains("http://") == true ? SchoolDomain : "http://" + SchoolDomain;
                        baseUrl = baseUrl + "api/madrasatie/upload_assignment_base64?";

                        baseUrl += "assignment_id=" + assignmentID;
                        baseUrl += "&";
                        baseUrl += "file_name=" + AttachmentFileName;

                        using (WebClient client = new WebClient())
                        {
                            client.Headers.Add("Authorization", token);
                            byte[] response =
                            client.UploadValues(baseUrl, new NameValueCollection()
                            {
                                { "attachment_file_name", Base64File }
                            });

                            string resultUpload = System.Text.Encoding.UTF8.GetString(response);
                        }
                    }
                }

                Dictionary<string, string> resultDictionary = new Dictionary<string, string>();
                resultDictionary.Add("Message", "Success");

                ListResultDictionary.Add(resultDictionary);

                resultJSON = ListResultDictionary.ToList<object>();
            }

            return resultJSON;
        }


        [Route("AssignmentByEmployee")]
        [HttpPost]
        public async Task<List<Dictionary<string, string>>> AssignmentByEmployee(int UserID, Dictionary<string, object> StudentListAndFile, int SubjectID, int BatchID,
            string Title, string Content, DateTime? DueDate, string Username, string FreeTextPassword, string AttachmentFileName = "", string AttachmentContentType = "", int? AttachmentFileSize = 0, string AssignmentType = "")
        {
            List<object> resultJSON = new List<object>();
            List<Dictionary<string, string>> ListResultOutputDictionary = new List<Dictionary<string, string>>();
            List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
            List<int> StudentList = new List<int>();
            string Base64File = string.Empty;
            int resultEventID;
            int AssignmentID;
            string EventDescription;

            var StudentListArray = StudentListAndFile.Where(x => x.Key == "StudentList").Select(x => x.Value).ToList()[0].ToString().Replace("[", string.Empty).Replace("]", string.Empty).Split(',').ToList();

            foreach (var studentID in StudentListArray)
            {
                StudentList.Add(Convert.ToInt32(GetNumbers(studentID)));
            }

            try
            {
                Base64File = StudentListAndFile.Where(x => x.Key == "File").Select(x => x.Value).ToList()[0].ToString();
            }
            catch { }

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                var EmployeeID = (from e in fedenaEntities.employees where e.user_id == UserID select e.id).ToList()[0];
                var studentListArray = string.Join(",", StudentList);

                var DateTimeNow = DateTime.Now;

                var resultList = (from e in fedenaEntities.employees

                                  join es in fedenaEntities.employees_subjects on e.id equals es.employee_id into employees_subjects
                                  from e_s in employees_subjects.DefaultIfEmpty()

                                  join u in fedenaEntities.users on e.user_id equals u.id into employees_users
                                  from e_u in employees_users.DefaultIfEmpty()

                                  where e_s.subject_id == SubjectID

                                  select new
                                  {
                                      EmployeeID = EmployeeID,
                                      SubjectID = e_s.subject_id,
                                      StudentList = studentListArray,
                                      Title = Title,
                                      Content = Content,
                                      DueDate = DueDate,
                                      AttachmentFileName = AttachmentFileName,
                                      AttachmentContentType = AttachmentContentType,
                                      AttachmentFileSize = AttachmentFileSize,
                                      SchoolID = e_s.school_id,
                                      CreatedAt = DateTimeNow
                                  }).Distinct().ToList();

                foreach (var assignment in resultList)
                {
                    assignment Assignment = new assignment();
                    Assignment.employee_id = assignment.EmployeeID;
                    Assignment.subject_id = assignment.SubjectID;
                    Assignment.student_list = assignment.StudentList;
                    Assignment.title = assignment.Title;
                    Assignment.content = assignment.Content;
                    Assignment.duedate = assignment.DueDate;
                    Assignment.attachment_file_name = assignment.AttachmentFileName;
                    Assignment.attachment_content_type = assignment.AttachmentContentType;
                    Assignment.attachment_file_size = assignment.AttachmentFileSize;
                    Assignment.school_id = assignment.SchoolID;
                    Assignment.created_at = assignment.CreatedAt;
                    Assignment.assignment_type = AssignmentType;

                    fedenaEntities.assignments.Add(Assignment);
                }

                fedenaEntities.SaveChanges();

                var CreatedAt = (from a in fedenaEntities.assignments select new { a.created_at });
                var maxCreatedAt = CreatedAt.Max(x => x.created_at);

                var resultOutput = (from a in fedenaEntities.assignments

                                    join sc in fedenaEntities.school_domains on a.school_id equals sc.linkable_id

                                    where a.created_at == maxCreatedAt
                                    && a.employee_id == EmployeeID

                                    select new
                                    {
                                        SchoolDomain = sc.domain,
                                        SchoolID = a.school_id,
                                        Content = a.content,
                                        AttachmentPathOne = "/srv/Fedena/Fedena_app/uploads/",
                                        AttachmentSchoolID = a.school_id.ToString(),
                                        AttachmentPathTwo = "/assignments/",
                                        AssignmentID = a.id.ToString()
                                    }).ToList();

                AssignmentID = Convert.ToInt32(resultOutput.First().AssignmentID);
                EventDescription = resultOutput.First().Content;

                foreach (var assignment in resultOutput)
                {
                    Dictionary<string, string> resultOutputDictionary = new Dictionary<string, string>();
                    string AttachmentPath = assignment.AttachmentPathOne + SixDigitsString(assignment.AttachmentSchoolID) + assignment.AttachmentPathTwo + SixDigitsString(assignment.AssignmentID);
                    resultOutputDictionary.Add("AttachmentPath", AttachmentPath);
                    resultOutputDictionary.Add("SchoolDomain", assignment.SchoolDomain);
                    resultOutputDictionary.Add("AssignmentID", assignment.AssignmentID);
                    resultOutputDictionary.Add("SchoolID", assignment.SchoolID.ToString());

                    ListResultOutputDictionary.Add(resultOutputDictionary);
                }

                var SchoolID = Convert.ToInt32(ListResultOutputDictionary[0]["SchoolID"]);

                var SchoolUrl = ListResultOutputDictionary[0]["SchoolDomain"].ToString();
                SchoolUrl = SchoolUrl.Substring(SchoolUrl.Length - 1) == "/" ? SchoolUrl : SchoolUrl + "/";

                var oAuthClientsList = (from authClient in fedenaEntities.oauth_clients
                                        where authClient.school_id == SchoolID
                                        && authClient.name == "MobileApp"
                                        select new
                                        {
                                            authClient.client_id,
                                            authClient.client_secret
                                        }).ToList();

                var ClientID = oAuthClientsList.First().client_id;
                var ClientSecret = oAuthClientsList.First().client_secret;

                var token = GetSchoolToken(Username, FreeTextPassword, ClientID, ClientSecret, SchoolUrl);

                if (AttachmentFileName.Count() > 0)
                {
                    foreach (var URL in ListResultOutputDictionary)
                    {
                        string baseUrl = string.Empty;

                        var assignmentID = URL["AssignmentID"];

                        URL["SchoolDomain"] = URL["SchoolDomain"].Substring(URL["SchoolDomain"].Length - 1) == "/" ? URL["SchoolDomain"] : URL["SchoolDomain"] + "/";
                        baseUrl = URL["SchoolDomain"].Contains("http://") == true ? URL["SchoolDomain"] : "http://" + URL["SchoolDomain"];
                        baseUrl = baseUrl + "api/madrasatie/upload_assignment_base64?";

                        baseUrl += "assignment_id=" + assignmentID;
                        baseUrl += "&";
                        baseUrl += "file_name=" + AttachmentFileName;

                        using (WebClient client = new WebClient())
                        {
                            client.Headers.Add("Authorization", token);
                            byte[] response =
                            client.UploadValues(baseUrl, new NameValueCollection()
                            {
                            { "attachment_file_name", Base64File }
                            });

                            string result = System.Text.Encoding.UTF8.GetString(response);
                        }
                    }
                }

                @event newEvent = new @event();

                newEvent.created_at = DateTime.Now;
                newEvent.description = EventDescription;
                newEvent.end_date = DueDate;
                newEvent.is_common = false;
                newEvent.is_due = false;
                newEvent.is_exam = false;
                newEvent.is_holiday = false;
                newEvent.origin_id = AssignmentID;
                newEvent.origin_type = "Assignment";
                newEvent.school_id = SchoolID;
                newEvent.start_date = DueDate;
                newEvent.title = Title;
                newEvent.updated_at = null;

                fedenaEntities.events.Add(newEvent);
                fedenaEntities.SaveChanges();


                var MaxCreatedAt = (from ev in fedenaEntities.events select new { ev.created_at }).Max(x => x.created_at);

                var resultEvents = (from be in fedenaEntities.events

                                    where be.created_at == MaxCreatedAt

                                    select new
                                    {
                                        EventID = be.id,
                                        BatchID = BatchID,
                                        CreatedAt = DateTime.Now,
                                        SchoolID = SchoolID,
                                    }).ToList();

                foreach (var ev in resultEvents)
                {
                    batch_events BatchEvents = new batch_events();
                    BatchEvents.id = ev.EventID;
                    BatchEvents.batch_id = ev.BatchID;
                    BatchEvents.created_at = ev.CreatedAt;
                    BatchEvents.school_id = ev.SchoolID;
                    BatchEvents.event_id = ev.EventID;

                    fedenaEntities.batch_events.Add(BatchEvents);

                    fedenaEntities.SaveChanges();
                }

                var MaxCreatedAtEvent = (from ev in fedenaEntities.events select new { ev.created_at }).Max(x => x.created_at);

                resultEventID = (from ev in fedenaEntities.events

                                 where ev.created_at == MaxCreatedAtEvent
                                 && ev.school_id == SchoolID
                                 select new
                                 {
                                     EventID = ev.id,
                                 }).ToList().First().EventID;


                try
                {
                    List<Dictionary<string, string>> ListOfUsers = await UserPerBatch(BatchID);

                    foreach (var user in ListOfUsers)
                    {
                        try
                        {
                            var NotifyUserID = Convert.ToInt32(user.First(x => x.Key == "UserID").Value);
                            var NotifySchoolID = Convert.ToInt32(user.First(x => x.Key == "SchoolID").Value);

                            List<Dictionary<string, string>> DeviceInformation = GetUserToken(NotifyUserID, NotifySchoolID);

                            foreach (var device in DeviceInformation)
                            {
                                string studentToken = device["Token"];
                                string deviceType = device["DeviceType"];

                                string assignmentMessage = generateAssignmentPushNotificationMessage();

                                PushNotification(studentToken, "", deviceType, Globals.Configuration.MPNS_PRODUCTION);
                            }
                        }
                        catch { }
                    }
                }
                catch { }
                

                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                if (resultOutput.Count > 0)
                {
                    OutputMessage.Add("Message", "Success");
                }
                else
                {
                    OutputMessage.Add("Message", "No records were updated");
                }

                OutputMessageList.Add(OutputMessage);
            }

            return OutputMessageList;
        }


        [Route("GetAssignmentsDetails")]
        public List<object> GetAssignmentsDetails(int AssignmentID, string Username = null, string FreeTextPassword = null)
        {
            List<object> resultJSON = new List<object>();
            List<Dictionary<string, string>> ListOutputMessage = new List<Dictionary<string, string>>();
            Dictionary<string, string> OutputMessage = new Dictionary<string, string>();
            string token = string.Empty;

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                var result = (from a in fedenaEntities.assignments

                             join sd in fedenaEntities.school_domains on a.school_id equals sd.linkable_id into schoolDomains_Assignment
                              from a_sd in schoolDomains_Assignment.DefaultIfEmpty()

                              join ans in fedenaEntities.assignment_answers on a.id equals ans.assignment_id into assignmentAnswers_Assignment
                              from a_ans in assignmentAnswers_Assignment.DefaultIfEmpty()

                              where a.id == AssignmentID

                             select new
                             {
                                 AssignmentID = a.id,
                                 SchoolID = a.school_id,
                                 EmployeeID = a.employee_id,
                                 SubjectID = a.subject_id,
                                 StudentList = a.student_list,
                                 AttachmentContentType = a.attachment_content_type,
                                 AttachmentFileSize = a.attachment_file_size,
                                 AttachmentUpdatedAt = a.attachment_updated_at,
                                 AttachmentCreatedAt = a.created_at,
                                 AssignmentAttachmentUpdatedAt = a.updated_at,
                                 AssignmentSchoolId = a.school_id,
                                 AssignmentTitle = a.title,
                                 AssignmentContent = a.content,
                                 AssignmentDuedate = a.duedate,
                                 AttachmentFileName = a.attachment_file_name,
                                 SchoolDomain = a_sd.domain,
                                 AssignmentType = a.assignment_type,
                                 AttachmentPath = "http://" + a_sd.domain + "/assignments/download_attachment/" + a.id.ToString()
                             }).ToList();

                ///////THE REQUEST OF TOKENS TO DOWNLOAD THE FILES ----- DO NOT DELETE
                //try
                //{
                //    int? SchoolID = result.First().SchoolID;
                //    string SchoolUrl = result.First().SchoolDomain;

                //    var oAuthClientsList = (from authClient in fedenaEntities.oauth_clients
                //                            where authClient.school_id == SchoolID
                //                            && authClient.name == "MobileApp"
                //                            select new
                //                            {
                //                                authClient.client_id,
                //                                authClient.client_secret
                //                            }).ToList();

                //    var ClientID = oAuthClientsList.First().client_id;
                //    var ClientSecret = oAuthClientsList.First().client_secret;

                //    token = GetSchoolToken(Username, FreeTextPassword, ClientID, ClientSecret, SchoolUrl);
                //}
                //catch
                //{

                //}

                //OutputMessage.Add("AssignmentID", result.First().AssignmentID.ToString());
                //OutputMessage.Add("SchoolID", result.First().SchoolID == null ? string.Empty : result.First().SchoolID.ToString());
                //OutputMessage.Add("EmployeeID", result.First().EmployeeID == null ? string.Empty : result.First().EmployeeID.ToString());
                //OutputMessage.Add("SubjectID", result.First().SubjectID == null ? string.Empty : result.First().SubjectID.ToString());
                //OutputMessage.Add("StudentList", result.First().StudentList == null ? string.Empty : result.First().StudentList.ToString());
                //OutputMessage.Add("AttachmentContentType", result.First().AttachmentContentType == null ? string.Empty : result.First().AttachmentContentType.ToString());
                //OutputMessage.Add("AttachmentFileSize", result.First().AttachmentFileSize == null ? string.Empty : result.First().AttachmentFileSize.ToString());
                //OutputMessage.Add("AttachmentUpdatedAt", result.First().AttachmentUpdatedAt == null ? string.Empty : result.First().AttachmentUpdatedAt.ToString());
                //OutputMessage.Add("AttachmentCreatedAt", result.First().AttachmentCreatedAt == null ? string.Empty : result.First().AttachmentCreatedAt.ToString());
                //OutputMessage.Add("AssignmentAttachmentUpdatedAt", result.First().AssignmentAttachmentUpdatedAt == null ? string.Empty : result.First().AssignmentAttachmentUpdatedAt.ToString());
                //OutputMessage.Add("AssignmentSchoolId", result.First().AssignmentSchoolId == null ? string.Empty : result.First().AssignmentSchoolId.ToString());
                //OutputMessage.Add("AssignmentTitle", result.First().AssignmentTitle == null ? string.Empty : result.First().AssignmentTitle.ToString());
                //OutputMessage.Add("AssignmentContent", result.First().AssignmentContent == null ? string.Empty : result.First().AssignmentContent.ToString());
                //OutputMessage.Add("AssignmentDuedate", result.First().AssignmentDuedate == null ? string.Empty : result.First().AssignmentDuedate.ToString());
                //OutputMessage.Add("AttachmentFileName", result.First().AttachmentFileName == null ? string.Empty : result.First().AttachmentFileName.ToString());
                //OutputMessage.Add("SchoolDomain", result.First().SchoolDomain == null ? string.Empty : result.First().SchoolDomain.ToString());
                //OutputMessage.Add("AssignmentType", result.First().AssignmentType == null ? string.Empty : result.First().AssignmentType.ToString());
                //OutputMessage.Add("AttachmentPath", result.First().AttachmentPath == null ? string.Empty : result.First().AttachmentPath.ToString());
                //OutputMessage.Add("Token", token == null ? string.Empty : token);

                //ListOutputMessage.Add(OutputMessage);
                //resultJSON = ListOutputMessage.ToList<object>();

                resultJSON = result.ToList<object>();
            }

            return resultJSON;
        }


        [Route("GetNotificationsPerUser")]
        public List<object> GetNotificationsPerUser(int UserID)
        {
            List<object> resultJSON = new List<object>();

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                var result = from mn in fedenaEntities.madrasatie_notifications

                             join s in fedenaEntities.students on mn.student_id equals s.id into students_madrasatieNotifications
                             from mn_s in students_madrasatieNotifications.DefaultIfEmpty()

                             where mn.user_id == UserID

                             select new
                             {
                                 NotificationID = mn.id,
                                 UserID = mn.user_id,
                                 MessageText = mn.message_text,
                                 OriginType = mn.origin_type,
                                 OriginID = mn.origin_id,
                                 IsChecked = mn.is_checked,
                                 CreatedAt = mn.created_at,
                                 UpdatedAt = mn.updated_at,
                                 UserIDofTheStudent = mn_s.user_id
                             };

                resultJSON = result.ToList<object>();
            }

            return resultJSON;
        }


        [Route("CountUnreadNotificationsPerUser")]
        public List<object> CountUnreadNotificationsPerUser(int UserID)
        {
            List<object> resultJSON = new List<object>();
            Dictionary<string,int> CountDictionary = new Dictionary<string, int>();

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                var result = (from mn in fedenaEntities.madrasatie_notifications

                             where mn.user_id == UserID
                             && mn.is_checked == false

                             select new
                             {}).ToList<object>().Count;

                CountDictionary.Add("UnreadNotifications", result);

                resultJSON.Add(CountDictionary);
            }

            return resultJSON;
        }
        



        [Route("GetAllEventsPerUser")]
        public List<object> GetAllEventsPerUser(int UserID, int SchoolID)
        {
            List<object> resultJSON = new List<object>();
            List<Dictionary<string, string>> ListResultDictionaryEvents = new List<Dictionary<string, string>>();
            List<Dictionary<string, string>> ListResultDictionaryBatchEvents = new List<Dictionary<string, string>>();
            List<Dictionary<string, string>> ListResultDictionaryUserEvents = new List<Dictionary<string, string>>();
            bool PublishAgenda = new bool();

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                PublishAgenda = Convert.ToBoolean((from msc in fedenaEntities.madrasatie_system_configs where msc.school_id == SchoolID select msc.publish_agenda).ToList()[0]);
            }

            if (PublishAgenda) //school is published
            {
                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    //var joinAssignments = (from e in fedenaEntities.events

                    //                       join ass in fedenaEntities.assignments on e.origin_id equals ass.id into assignments_events
                    //                       from ass_ev in assignments_events.DefaultIfEmpty()

                    //                       where ass_ev.is_published == true
                    //                       select ass_ev);

                    var resultEvents = (from e in fedenaEntities.events

                                        join ass in fedenaEntities.assignments on e.origin_id equals ass.id into assignments_events
                                        from ass_ev in assignments_events.DefaultIfEmpty()

                                        where e.is_common == true
                                        && e.school_id == SchoolID

                                        select new
                                        {
                                            EventsID = e.id,
                                            Title = e.title,
                                            Description = e.description,
                                            NotificationID = e.id,
                                            StartDate = e.start_date,
                                            EndDate = e.end_date,
                                            IsCommon = e.is_common,
                                            IsHoliday = e.is_holiday,
                                            IsExam = e.is_exam,
                                            IsDue = e.is_due,
                                            CreatedAt = e.created_at,
                                            UpdatedAt = e.updated_at,
                                            OriginID = e.origin_id,
                                            OriginType = e.origin_type,
                                            SchoolID = e.school_id,
                                            IsPublished = ass_ev.is_published
                                        }).ToList();

                    foreach (var ev in resultEvents)
                    {
                        Dictionary<string, string> resultDictionary = new Dictionary<string, string>();

                        resultDictionary.Add("EventsID", ev.EventsID == null ? "" : ev.EventsID.ToString());
                        resultDictionary.Add("Title", ev.Title == null ? "" : ev.Title.ToString());
                        resultDictionary.Add("Description", ev.Description == null ? "" : ev.Description.ToString());
                        resultDictionary.Add("NotificationID", ev.NotificationID == null ? "" : ev.NotificationID.ToString());

                        try
                        {
                            resultDictionary.Add("DateRange", String
                                .Join(
                                ",",
                                Enumerable.Range(0, 1 + Convert.ToDateTime(ev.EndDate).Subtract(Convert.ToDateTime(ev.StartDate)).Days)
                                .Select(offset => Convert.ToDateTime(ev.StartDate).AddDays(offset))
                                .ToArray()));

                            resultDictionary.Add("StartDate", Convert.ToDateTime(ev.StartDate).ToString("yyyy-MM-ddThh:mm:ss"));
                            resultDictionary.Add("EndDate", Convert.ToDateTime(ev.EndDate).ToString("yyyy-MM-ddThh:mm:ss"));
                        }
                        catch
                        {
                            try
                            {
                                resultDictionary.Add("DateRange", String
                                    .Join(
                                    ",",
                                    Enumerable.Range(0, 1 + Convert.ToDateTime(ev.StartDate).Subtract(Convert.ToDateTime(ev.EndDate)).Days)
                                    .Select(offset => Convert.ToDateTime(ev.EndDate).AddDays(offset))
                                    .ToArray()));

                                resultDictionary.Add("StartDate", Convert.ToDateTime(ev.EndDate).ToString("yyyy-MM-ddThh:mm:ss"));
                                resultDictionary.Add("EndDate", Convert.ToDateTime(ev.StartDate).ToString("yyyy-MM-ddThh:mm:ss"));
                            }
                            catch
                            {
                                resultDictionary.Add("DateRange", "");
                                resultDictionary.Add("StartDate", "N/A");
                                resultDictionary.Add("EndDate", "N/A");
                            }

                        }

                        resultDictionary.Add("IsCommon", ev.IsCommon == null ? "" : ev.IsCommon.ToString());
                        resultDictionary.Add("IsHoliday", ev.IsHoliday == null ? "" : ev.IsHoliday.ToString());
                        resultDictionary.Add("IsExam", ev.IsExam == null ? "" : ev.IsExam.ToString());
                        resultDictionary.Add("IsDue", ev.IsDue == null ? "" : ev.IsDue.ToString());
                        resultDictionary.Add("CreatedAt", ev.CreatedAt == null ? "" : Convert.ToDateTime(ev.CreatedAt).ToString("yyyy-MM-ddThh:mm:ss"));
                        resultDictionary.Add("UpdatedAt", ev.UpdatedAt == null ? "" : Convert.ToDateTime(ev.UpdatedAt).ToString("yyyy-MM-ddThh:mm:ss"));
                        resultDictionary.Add("OriginID", ev.OriginID == null ? "" : ev.OriginID.ToString());
                        resultDictionary.Add("OriginType", ev.OriginType == null ? "" : ev.OriginType.ToString());
                        resultDictionary.Add("SchoolID", ev.SchoolID == null ? "" : ev.SchoolID.ToString());
                        resultDictionary.Add("IsPublished", ev.IsPublished == null ? "" : ev.IsPublished.ToString());

                        ListResultDictionaryEvents.Add(resultDictionary);
                    }

                    var resultBatchEvents = (from be in fedenaEntities.batch_events

                                             //join b in fedenaEntities.batches on be.batch_id equals b.id

                                             join s in fedenaEntities.students on be.batch_id equals s.batch_id

                                             join e in fedenaEntities.events on be.event_id equals e.id

                                             join ass in fedenaEntities.assignments on e.origin_id equals ass.id into assignments_events
                                             from ass_ev in assignments_events.DefaultIfEmpty()

                                             where s.user_id == UserID
                                             && e.school_id == SchoolID
                                             //&& b.is_deleted == false
                                             //&& b.is_active == true

                                             select new
                                             {
                                                 EventsID = e.id,
                                                 Title = e.title,
                                                 Description = e.description,
                                                 NotificationID = e.id,
                                                 StartDate = e.start_date,
                                                 EndDate = e.end_date,
                                                 IsCommon = e.is_common,
                                                 IsHoliday = e.is_holiday,
                                                 IsExam = e.is_exam,
                                                 IsDue = e.is_due,
                                                 CreatedAt = e.created_at,
                                                 UpdatedAt = e.updated_at,
                                                 OriginID = e.origin_id,
                                                 OriginType = e.origin_type,
                                                 SchoolID = e.school_id,
                                                 IsPublished = ass_ev.is_published
                                             }).ToList();

                    foreach (var ev in resultBatchEvents)
                    {
                        Dictionary<string, string> resultDictionary = new Dictionary<string, string>();

                        resultDictionary.Add("EventsID", ev.EventsID == null ? "" : ev.EventsID.ToString());
                        resultDictionary.Add("Title", ev.Title == null ? "" : ev.Title.ToString());
                        resultDictionary.Add("Description", ev.Description == null ? "" : ev.Description.ToString());
                        resultDictionary.Add("NotificationID", ev.NotificationID == null ? "" : ev.NotificationID.ToString());

                        try
                        {
                            resultDictionary.Add("DateRange", String
                                .Join(
                                ",",
                                Enumerable.Range(0, 1 + Convert.ToDateTime(ev.EndDate).Subtract(Convert.ToDateTime(ev.StartDate)).Days)
                                .Select(offset => Convert.ToDateTime(ev.StartDate).AddDays(offset))
                                .ToArray()));

                            resultDictionary.Add("StartDate", Convert.ToDateTime(ev.StartDate).ToString("yyyy-MM-ddThh:mm:ss"));
                            resultDictionary.Add("EndDate", Convert.ToDateTime(ev.EndDate).ToString("yyyy-MM-ddThh:mm:ss"));
                        }
                        catch
                        {
                            try
                            {
                                resultDictionary.Add("DateRange", String
                                    .Join(
                                    ",",
                                    Enumerable.Range(0, 1 + Convert.ToDateTime(ev.StartDate).Subtract(Convert.ToDateTime(ev.EndDate)).Days)
                                    .Select(offset => Convert.ToDateTime(ev.EndDate).AddDays(offset))
                                    .ToArray()));

                                resultDictionary.Add("StartDate", Convert.ToDateTime(ev.EndDate).ToString("yyyy-MM-ddThh:mm:ss"));
                                resultDictionary.Add("EndDate", Convert.ToDateTime(ev.StartDate).ToString("yyyy-MM-ddThh:mm:ss"));
                            }
                            catch
                            {
                                resultDictionary.Add("DateRange", "");
                                resultDictionary.Add("StartDate", "N/A");
                                resultDictionary.Add("EndDate", "N/A");
                            }

                        }

                        resultDictionary.Add("IsCommon", ev.IsCommon == null ? "" : ev.IsCommon.ToString());
                        resultDictionary.Add("IsHoliday", ev.IsHoliday == null ? "" : ev.IsHoliday.ToString());
                        resultDictionary.Add("IsExam", ev.IsExam == null ? "" : ev.IsExam.ToString());
                        resultDictionary.Add("IsDue", ev.IsDue == null ? "" : ev.IsDue.ToString());
                        resultDictionary.Add("CreatedAt", ev.CreatedAt == null ? "" : Convert.ToDateTime(ev.CreatedAt).ToString("yyyy-MM-ddThh:mm:ss"));
                        resultDictionary.Add("UpdatedAt", ev.UpdatedAt == null ? "" : Convert.ToDateTime(ev.UpdatedAt).ToString("yyyy-MM-ddThh:mm:ss"));
                        resultDictionary.Add("OriginID", ev.OriginID == null ? "" : ev.OriginID.ToString());
                        resultDictionary.Add("OriginType", ev.OriginType == null ? "" : ev.OriginType.ToString());
                        resultDictionary.Add("SchoolID", ev.SchoolID == null ? "" : ev.SchoolID.ToString());
                        resultDictionary.Add("IsPublished", ev.IsPublished == null ? "" : ev.IsPublished.ToString());

                        ListResultDictionaryBatchEvents.Add(resultDictionary);
                    }


                    var resultUserEvents = (from ue in fedenaEntities.user_events

                                            join e in fedenaEntities.events on ue.event_id equals e.id

                                            join ass in fedenaEntities.assignments on e.origin_id equals ass.id into assignments_events
                                            from ass_ev in assignments_events.DefaultIfEmpty()

                                            where ue.user_id == UserID
                                            && e.school_id == SchoolID

                                            select new
                                            {
                                                EventsID = e.id,
                                                Title = e.title,
                                                Description = e.description,
                                                NotificationID = e.id,
                                                StartDate = e.start_date,
                                                EndDate = e.end_date,
                                                IsCommon = e.is_common,
                                                IsHoliday = e.is_holiday,
                                                IsExam = e.is_exam,
                                                IsDue = e.is_due,
                                                CreatedAt = e.created_at,
                                                UpdatedAt = e.updated_at,
                                                OriginID = e.origin_id,
                                                OriginType = e.origin_type,
                                                SchoolID = e.school_id,
                                                IsPublished = ass_ev.is_published
                                            }).ToList();

                    foreach (var ev in resultUserEvents)
                    {
                        Dictionary<string, string> resultDictionary = new Dictionary<string, string>();

                        resultDictionary.Add("EventsID", ev.EventsID == null ? "" : ev.EventsID.ToString());
                        resultDictionary.Add("Title", ev.Title == null ? "" : ev.Title.ToString());
                        resultDictionary.Add("Description", ev.Description == null ? "" : ev.Description.ToString());
                        resultDictionary.Add("NotificationID", ev.NotificationID == null ? "" : ev.NotificationID.ToString());

                        try
                        {
                            resultDictionary.Add("DateRange", String
                                .Join(
                                ",",
                                Enumerable.Range(0, 1 + Convert.ToDateTime(ev.EndDate).Subtract(Convert.ToDateTime(ev.StartDate)).Days)
                                .Select(offset => Convert.ToDateTime(ev.StartDate).AddDays(offset))
                                .ToArray()));

                            resultDictionary.Add("StartDate", Convert.ToDateTime(ev.StartDate).ToString("yyyy-MM-ddThh:mm:ss"));
                            resultDictionary.Add("EndDate", Convert.ToDateTime(ev.EndDate).ToString("yyyy-MM-ddThh:mm:ss"));
                        }
                        catch
                        {
                            try
                            {
                                resultDictionary.Add("DateRange", String
                                    .Join(
                                    ",",
                                    Enumerable.Range(0, 1 + Convert.ToDateTime(ev.StartDate).Subtract(Convert.ToDateTime(ev.EndDate)).Days)
                                    .Select(offset => Convert.ToDateTime(ev.EndDate).AddDays(offset))
                                    .ToArray()));

                                resultDictionary.Add("StartDate", Convert.ToDateTime(ev.EndDate).ToString("yyyy-MM-ddThh:mm:ss"));
                                resultDictionary.Add("EndDate", Convert.ToDateTime(ev.StartDate).ToString("yyyy-MM-ddThh:mm:ss"));
                            }
                            catch
                            {
                                resultDictionary.Add("DateRange", "");
                                resultDictionary.Add("StartDate", "N/A");
                                resultDictionary.Add("EndDate", "N/A");
                            }

                        }

                        resultDictionary.Add("IsCommon", ev.IsCommon == null ? "" : ev.IsCommon.ToString());
                        resultDictionary.Add("IsHoliday", ev.IsHoliday == null ? "" : ev.IsHoliday.ToString());
                        resultDictionary.Add("IsExam", ev.IsExam == null ? "" : ev.IsExam.ToString());
                        resultDictionary.Add("IsDue", ev.IsDue == null ? "" : ev.IsDue.ToString());
                        resultDictionary.Add("CreatedAt", ev.CreatedAt == null ? "" : Convert.ToDateTime(ev.CreatedAt).ToString("yyyy-MM-ddThh:mm:ss"));
                        resultDictionary.Add("UpdatedAt", ev.UpdatedAt == null ? "" : Convert.ToDateTime(ev.UpdatedAt).ToString("yyyy-MM-ddThh:mm:ss"));
                        resultDictionary.Add("OriginID", ev.OriginID == null ? "" : ev.OriginID.ToString());
                        resultDictionary.Add("OriginType", ev.OriginType == null ? "" : ev.OriginType.ToString());
                        resultDictionary.Add("SchoolID", ev.SchoolID == null ? "" : ev.SchoolID.ToString());
                        resultDictionary.Add("IsPublished", ev.IsPublished == null ? "" : ev.IsPublished.ToString());

                        ListResultDictionaryUserEvents.Add(resultDictionary);
                    }


                    resultJSON.AddRange(ListResultDictionaryEvents);
                    resultJSON.AddRange(ListResultDictionaryBatchEvents);
                    resultJSON.AddRange(ListResultDictionaryUserEvents);
                }
            }
            else //school is not published
            {
                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    var resultEvents = (from e in fedenaEntities.events

                                        where e.is_common == true
                                        && e.school_id == SchoolID

                                        select new
                                        {
                                            EventsID = e.id,
                                            Title = e.title,
                                            Description = e.description,
                                            NotificationID = e.id,
                                            StartDate = e.start_date,
                                            EndDate = e.end_date,
                                            IsCommon = e.is_common,
                                            IsHoliday = e.is_holiday,
                                            IsExam = e.is_exam,
                                            IsDue = e.is_due,
                                            CreatedAt = e.created_at,
                                            UpdatedAt = e.updated_at,
                                            OriginID = e.origin_id,
                                            OriginType = e.origin_type,
                                            SchoolID = e.school_id
                                        }).ToList();

                    foreach (var ev in resultEvents)
                    {
                        Dictionary<string, string> resultDictionary = new Dictionary<string, string>();

                        resultDictionary.Add("EventsID", ev.EventsID == null ? "" : ev.EventsID.ToString());
                        resultDictionary.Add("Title", ev.Title == null ? "" : ev.Title.ToString());
                        resultDictionary.Add("Description", ev.Description == null ? "" : ev.Description.ToString());
                        resultDictionary.Add("NotificationID", ev.NotificationID == null ? "" : ev.NotificationID.ToString());

                        try
                        {
                            resultDictionary.Add("DateRange", String
                                .Join(
                                ",",
                                Enumerable.Range(0, 1 + Convert.ToDateTime(ev.EndDate).Subtract(Convert.ToDateTime(ev.StartDate)).Days)
                                .Select(offset => Convert.ToDateTime(ev.StartDate).AddDays(offset))
                                .ToArray()));

                            resultDictionary.Add("StartDate", Convert.ToDateTime(ev.StartDate).ToString("yyyy-MM-ddThh:mm:ss"));
                            resultDictionary.Add("EndDate", Convert.ToDateTime(ev.EndDate).ToString("yyyy-MM-ddThh:mm:ss"));
                        }
                        catch
                        {
                            try
                            {
                                resultDictionary.Add("DateRange", String
                                    .Join(
                                    ",",
                                    Enumerable.Range(0, 1 + Convert.ToDateTime(ev.StartDate).Subtract(Convert.ToDateTime(ev.EndDate)).Days)
                                    .Select(offset => Convert.ToDateTime(ev.EndDate).AddDays(offset))
                                    .ToArray()));

                                resultDictionary.Add("StartDate", Convert.ToDateTime(ev.EndDate).ToString("yyyy-MM-ddThh:mm:ss"));
                                resultDictionary.Add("EndDate", Convert.ToDateTime(ev.StartDate).ToString("yyyy-MM-ddThh:mm:ss"));
                            }
                            catch
                            {
                                resultDictionary.Add("DateRange", "");
                                resultDictionary.Add("StartDate", "N/A");
                                resultDictionary.Add("EndDate", "N/A");
                            }

                        }

                        resultDictionary.Add("IsCommon", ev.IsCommon == null ? "" : ev.IsCommon.ToString());
                        resultDictionary.Add("IsHoliday", ev.IsHoliday == null ? "" : ev.IsHoliday.ToString());
                        resultDictionary.Add("IsExam", ev.IsExam == null ? "" : ev.IsExam.ToString());
                        resultDictionary.Add("IsDue", ev.IsDue == null ? "" : ev.IsDue.ToString());
                        resultDictionary.Add("CreatedAt", ev.CreatedAt == null ? "" : Convert.ToDateTime(ev.CreatedAt).ToString("yyyy-MM-ddThh:mm:ss"));
                        resultDictionary.Add("UpdatedAt", ev.UpdatedAt == null ? "" : Convert.ToDateTime(ev.UpdatedAt).ToString("yyyy-MM-ddThh:mm:ss"));
                        resultDictionary.Add("OriginID", ev.OriginID == null ? "" : ev.OriginID.ToString());
                        resultDictionary.Add("OriginType", ev.OriginType == null ? "" : ev.OriginType.ToString());
                        resultDictionary.Add("SchoolID", ev.SchoolID == null ? "" : ev.SchoolID.ToString());
                        resultDictionary.Add("IsPublished", "True");

                        ListResultDictionaryEvents.Add(resultDictionary);
                    }

                    var resultBatchEvents = (from be in fedenaEntities.batch_events

                                             //join b in fedenaEntities.batches on be.batch_id equals b.id

                                             join s in fedenaEntities.students on be.batch_id equals s.batch_id

                                             join e in fedenaEntities.events on be.event_id equals e.id

                                             where s.user_id == UserID
                                             && e.school_id == SchoolID
                                             //&& b.is_deleted == false
                                             //&& b.is_active == true

                                             select new
                                             {
                                                 EventsID = e.id,
                                                 Title = e.title,
                                                 Description = e.description,
                                                 NotificationID = e.id,
                                                 StartDate = e.start_date,
                                                 EndDate = e.end_date,
                                                 IsCommon = e.is_common,
                                                 IsHoliday = e.is_holiday,
                                                 IsExam = e.is_exam,
                                                 IsDue = e.is_due,
                                                 CreatedAt = e.created_at,
                                                 UpdatedAt = e.updated_at,
                                                 OriginID = e.origin_id,
                                                 OriginType = e.origin_type,
                                                 SchoolID = e.school_id
                                             }).ToList();

                    foreach (var ev in resultBatchEvents)
                    {
                        Dictionary<string, string> resultDictionary = new Dictionary<string, string>();

                        resultDictionary.Add("EventsID", ev.EventsID == null ? "" : ev.EventsID.ToString());
                        resultDictionary.Add("Title", ev.Title == null ? "" : ev.Title.ToString());
                        resultDictionary.Add("Description", ev.Description == null ? "" : ev.Description.ToString());
                        resultDictionary.Add("NotificationID", ev.NotificationID == null ? "" : ev.NotificationID.ToString());

                        try
                        {
                            resultDictionary.Add("DateRange", String
                                .Join(
                                ",",
                                Enumerable.Range(0, 1 + Convert.ToDateTime(ev.EndDate).Subtract(Convert.ToDateTime(ev.StartDate)).Days)
                                .Select(offset => Convert.ToDateTime(ev.StartDate).AddDays(offset))
                                .ToArray()));

                            resultDictionary.Add("StartDate", Convert.ToDateTime(ev.StartDate).ToString("yyyy-MM-ddThh:mm:ss"));
                            resultDictionary.Add("EndDate", Convert.ToDateTime(ev.EndDate).ToString("yyyy-MM-ddThh:mm:ss"));
                        }
                        catch
                        {
                            try
                            {
                                resultDictionary.Add("DateRange", String
                                    .Join(
                                    ",",
                                    Enumerable.Range(0, 1 + Convert.ToDateTime(ev.StartDate).Subtract(Convert.ToDateTime(ev.EndDate)).Days)
                                    .Select(offset => Convert.ToDateTime(ev.EndDate).AddDays(offset))
                                    .ToArray()));

                                resultDictionary.Add("StartDate", Convert.ToDateTime(ev.EndDate).ToString("yyyy-MM-ddThh:mm:ss"));
                                resultDictionary.Add("EndDate", Convert.ToDateTime(ev.StartDate).ToString("yyyy-MM-ddThh:mm:ss"));
                            }
                            catch
                            {
                                resultDictionary.Add("DateRange", "");
                                resultDictionary.Add("StartDate", "N/A");
                                resultDictionary.Add("EndDate", "N/A");
                            }

                        }

                        resultDictionary.Add("IsCommon", ev.IsCommon == null ? "" : ev.IsCommon.ToString());
                        resultDictionary.Add("IsHoliday", ev.IsHoliday == null ? "" : ev.IsHoliday.ToString());
                        resultDictionary.Add("IsExam", ev.IsExam == null ? "" : ev.IsExam.ToString());
                        resultDictionary.Add("IsDue", ev.IsDue == null ? "" : ev.IsDue.ToString());
                        resultDictionary.Add("CreatedAt", ev.CreatedAt == null ? "" : Convert.ToDateTime(ev.CreatedAt).ToString("yyyy-MM-ddThh:mm:ss"));
                        resultDictionary.Add("UpdatedAt", ev.UpdatedAt == null ? "" : Convert.ToDateTime(ev.UpdatedAt).ToString("yyyy-MM-ddThh:mm:ss"));
                        resultDictionary.Add("OriginID", ev.OriginID == null ? "" : ev.OriginID.ToString());
                        resultDictionary.Add("OriginType", ev.OriginType == null ? "" : ev.OriginType.ToString());
                        resultDictionary.Add("SchoolID", ev.SchoolID == null ? "" : ev.SchoolID.ToString());
                        resultDictionary.Add("IsPublished", "True");

                        ListResultDictionaryBatchEvents.Add(resultDictionary);
                    }


                    var resultUserEvents = (from ue in fedenaEntities.user_events

                                            join e in fedenaEntities.events on ue.event_id equals e.id

                                            where ue.user_id == UserID
                                            && e.school_id == SchoolID

                                            select new
                                            {
                                                EventsID = e.id,
                                                Title = e.title,
                                                Description = e.description,
                                                NotificationID = e.id,
                                                StartDate = e.start_date,
                                                EndDate = e.end_date,
                                                IsCommon = e.is_common,
                                                IsHoliday = e.is_holiday,
                                                IsExam = e.is_exam,
                                                IsDue = e.is_due,
                                                CreatedAt = e.created_at,
                                                UpdatedAt = e.updated_at,
                                                OriginID = e.origin_id,
                                                OriginType = e.origin_type,
                                                SchoolID = e.school_id
                                            }).ToList();

                    foreach (var ev in resultUserEvents)
                    {
                        Dictionary<string, string> resultDictionary = new Dictionary<string, string>();

                        resultDictionary.Add("EventsID", ev.EventsID == null ? "" : ev.EventsID.ToString());
                        resultDictionary.Add("Title", ev.Title == null ? "" : ev.Title.ToString());
                        resultDictionary.Add("Description", ev.Description == null ? "" : ev.Description.ToString());
                        resultDictionary.Add("NotificationID", ev.NotificationID == null ? "" : ev.NotificationID.ToString());

                        try
                        {
                            resultDictionary.Add("DateRange", String
                                .Join(
                                ",",
                                Enumerable.Range(0, 1 + Convert.ToDateTime(ev.EndDate).Subtract(Convert.ToDateTime(ev.StartDate)).Days)
                                .Select(offset => Convert.ToDateTime(ev.StartDate).AddDays(offset))
                                .ToArray()));

                            resultDictionary.Add("StartDate", Convert.ToDateTime(ev.StartDate).ToString("yyyy-MM-ddThh:mm:ss"));
                            resultDictionary.Add("EndDate", Convert.ToDateTime(ev.EndDate).ToString("yyyy-MM-ddThh:mm:ss"));
                        }
                        catch
                        {
                            try
                            {
                                resultDictionary.Add("DateRange", String
                                    .Join(
                                    ",",
                                    Enumerable.Range(0, 1 + Convert.ToDateTime(ev.StartDate).Subtract(Convert.ToDateTime(ev.EndDate)).Days)
                                    .Select(offset => Convert.ToDateTime(ev.EndDate).AddDays(offset))
                                    .ToArray()));

                                resultDictionary.Add("StartDate", Convert.ToDateTime(ev.EndDate).ToString("yyyy-MM-ddThh:mm:ss"));
                                resultDictionary.Add("EndDate", Convert.ToDateTime(ev.StartDate).ToString("yyyy-MM-ddThh:mm:ss"));
                            }
                            catch
                            {
                                resultDictionary.Add("DateRange", "");
                                resultDictionary.Add("StartDate", "N/A");
                                resultDictionary.Add("EndDate", "N/A");
                            }

                        }

                        resultDictionary.Add("IsCommon", ev.IsCommon == null ? "" : ev.IsCommon.ToString());
                        resultDictionary.Add("IsHoliday", ev.IsHoliday == null ? "" : ev.IsHoliday.ToString());
                        resultDictionary.Add("IsExam", ev.IsExam == null ? "" : ev.IsExam.ToString());
                        resultDictionary.Add("IsDue", ev.IsDue == null ? "" : ev.IsDue.ToString());
                        resultDictionary.Add("CreatedAt", ev.CreatedAt == null ? "" : Convert.ToDateTime(ev.CreatedAt).ToString("yyyy-MM-ddThh:mm:ss"));
                        resultDictionary.Add("UpdatedAt", ev.UpdatedAt == null ? "" : Convert.ToDateTime(ev.UpdatedAt).ToString("yyyy-MM-ddThh:mm:ss"));
                        resultDictionary.Add("OriginID", ev.OriginID == null ? "" : ev.OriginID.ToString());
                        resultDictionary.Add("OriginType", ev.OriginType == null ? "" : ev.OriginType.ToString());
                        resultDictionary.Add("SchoolID", ev.SchoolID == null ? "" : ev.SchoolID.ToString());
                        resultDictionary.Add("IsPublished", "True");

                        ListResultDictionaryUserEvents.Add(resultDictionary);
                    }


                    resultJSON.AddRange(ListResultDictionaryEvents);
                    resultJSON.AddRange(ListResultDictionaryBatchEvents);
                    resultJSON.AddRange(ListResultDictionaryUserEvents);
                }
            }

            return resultJSON;
        }



        [Route("GetAllEventsPerEmployee")]
        public List<object> GetAllEventsPerEmployee(int UserID, int SchoolID, DateTime? StartDate, DateTime? EndDate)
        {
            List<object> resultJSON = new List<object>();
            List<Dictionary<string, string>> ListResultDictionary = new List<Dictionary<string, string>>();
            bool PublishAgenda = new bool();

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                PublishAgenda = Convert.ToBoolean((from msc in fedenaEntities.madrasatie_system_configs where msc.school_id == SchoolID select msc.publish_agenda).ToList()[0]);
            }

            if (PublishAgenda) //school is published
            {

                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    //var joinAssignments = (from e in fedenaEntities.events

                    //                       join ass in fedenaEntities.assignments on e.origin_id equals ass.id into assignments_events
                    //                       from ass_ev in assignments_events.DefaultIfEmpty()

                    //                       where ass_ev.is_published == true
                    //                       select ass_ev).ToList();

                    var resultEvents = (from e in fedenaEntities.events

                                        join ass in fedenaEntities.assignments on e.origin_id equals ass.id into assignments_events
                                        from ass_ev in assignments_events.DefaultIfEmpty()

                                        where e.is_common == true && 
                                        e.school_id == SchoolID
                                        && e.start_date >= StartDate
                                        && e.end_date <= EndDate

                                        select new
                                        {
                                            EventsID = e.id,
                                            Title = e.title,
                                            Description = e.description,
                                            NotificationID = e.id,
                                            StartDate = e.start_date,
                                            EndDate = e.end_date,
                                            IsCommon = e.is_common,
                                            IsHoliday = e.is_holiday,
                                            IsExam = e.is_exam,
                                            IsDue = e.is_due,
                                            CreatedAt = e.created_at,
                                            UpdatedAt = e.updated_at,
                                            OriginID = e.origin_id,
                                            OriginType = e.origin_type,
                                            SchoolID = e.school_id,
                                            IsPublished = ass_ev.is_published
                                        }).Distinct().ToList();


                    foreach (var ev in resultEvents)
                    {
                        Dictionary<string, string> resultDictionary = new Dictionary<string, string>();

                        resultDictionary.Add("EventsID", ev.EventsID == null ? "" : ev.EventsID.ToString());
                        resultDictionary.Add("Title", ev.Title == null ? "" : ev.Title.ToString());
                        resultDictionary.Add("Description", ev.Description == null ? "" : ev.Description.ToString());
                        resultDictionary.Add("NotificationID", ev.NotificationID == null ? "" : ev.NotificationID.ToString());

                        try
                        {
                            resultDictionary.Add("DateRange", String
                                .Join(
                                ",",
                                Enumerable.Range(0, 1 + Convert.ToDateTime(ev.EndDate).Subtract(Convert.ToDateTime(ev.StartDate)).Days)
                                .Select(offset => Convert.ToDateTime(ev.StartDate).AddDays(offset))
                                .ToArray()));

                            resultDictionary.Add("StartDate", Convert.ToDateTime(ev.StartDate).ToString("yyyy-MM-ddThh:mm:ss"));
                            resultDictionary.Add("EndDate", Convert.ToDateTime(ev.EndDate).ToString("yyyy-MM-ddThh:mm:ss"));
                        }
                        catch
                        {
                            try
                            {
                                resultDictionary.Add("DateRange", String
                                    .Join(
                                    ",",
                                    Enumerable.Range(0, 1 + Convert.ToDateTime(ev.StartDate).Subtract(Convert.ToDateTime(ev.EndDate)).Days)
                                    .Select(offset => Convert.ToDateTime(ev.EndDate).AddDays(offset))
                                    .ToArray()));

                                resultDictionary.Add("StartDate", Convert.ToDateTime(ev.EndDate).ToString("yyyy-MM-ddThh:mm:ss"));
                                resultDictionary.Add("EndDate", Convert.ToDateTime(ev.StartDate).ToString("yyyy-MM-ddThh:mm:ss"));
                            }
                            catch
                            {
                                resultDictionary.Add("DateRange", "");
                                resultDictionary.Add("StartDate", "N/A");
                                resultDictionary.Add("EndDate", "N/A");
                            }

                        }

                        resultDictionary.Add("IsCommon", ev.IsCommon == null ? "" : ev.IsCommon.ToString());
                        resultDictionary.Add("IsHoliday", ev.IsHoliday == null ? "" : ev.IsHoliday.ToString());
                        resultDictionary.Add("IsExam", ev.IsExam == null ? "" : ev.IsExam.ToString());
                        resultDictionary.Add("IsDue", ev.IsDue == null ? "" : ev.IsDue.ToString());
                        resultDictionary.Add("CreatedAt", ev.CreatedAt == null ? "" : Convert.ToDateTime(ev.CreatedAt).ToString("yyyy-MM-ddThh:mm:ss"));
                        resultDictionary.Add("UpdatedAt", ev.UpdatedAt == null ? "" : Convert.ToDateTime(ev.UpdatedAt).ToString("yyyy-MM-ddThh:mm:ss"));
                        resultDictionary.Add("OriginID", ev.OriginID == null ? "" : ev.OriginID.ToString());
                        resultDictionary.Add("OriginType", ev.OriginType == null ? "" : ev.OriginType.ToString());
                        resultDictionary.Add("SchoolID", ev.SchoolID == null ? "" : ev.SchoolID.ToString());
                        resultDictionary.Add("IsPublished", ev.IsPublished == null ? "" : ev.IsPublished.ToString());

                        ListResultDictionary.Add(resultDictionary);
                    }

                    resultJSON.AddRange(ListResultDictionary);

                    try
                    {
                        int? EmployeeDepartmentID = (from emp in fedenaEntities.employees

                                                     where emp.user_id == UserID
                                                     && emp.school_id == SchoolID

                                                     select new
                                                     {
                                                         EmployeeDepartmentID = emp.employee_department_id
                                                     }).ToList().First().EmployeeDepartmentID;

                        var resultEmployeeDepartmentEvents = (from ede in fedenaEntities.employee_department_events

                                                              join e in fedenaEntities.employees on ede.employee_department_id equals e.employee_department_id

                                                              join ev in fedenaEntities.events on ede.event_id equals ev.id

                                                              join ass in fedenaEntities.assignments on ev.origin_id equals ass.id into assignments_events
                                                              from ass_ev in assignments_events.DefaultIfEmpty()

                                                              where e.employee_department_id == EmployeeDepartmentID
                                                              && ev.start_date >= StartDate
                                                              && ev.end_date <= EndDate

                                                              select new
                                                              {
                                                                  EventsID = ev.id,
                                                                  Title = ev.title,
                                                                  Description = ev.description,
                                                                  NotificationID = ev.id,
                                                                  StartDate = ev.start_date,
                                                                  EndDate = ev.end_date,
                                                                  IsCommon = ev.is_common,
                                                                  IsHoliday = ev.is_holiday,
                                                                  IsExam = ev.is_exam,
                                                                  IsDue = ev.is_due,
                                                                  CreatedAt = ev.created_at,
                                                                  UpdatedAt = ev.updated_at,
                                                                  OriginID = ev.origin_id,
                                                                  OriginType = ev.origin_type,
                                                                  SchoolID = ev.school_id,
                                                                  IsPublished = ass_ev.is_published
                                                              }).Distinct().ToList();


                        foreach (var ev in resultEmployeeDepartmentEvents)
                        {
                            Dictionary<string, string> resultDictionary = new Dictionary<string, string>();

                            resultDictionary.Add("EventsID", ev.EventsID == null ? "" : ev.EventsID.ToString());
                            resultDictionary.Add("Title", ev.Title == null ? "" : ev.Title.ToString());
                            resultDictionary.Add("Description", ev.Description == null ? "" : ev.Description.ToString());
                            resultDictionary.Add("NotificationID", ev.NotificationID == null ? "" : ev.NotificationID.ToString());

                            try
                            {
                                resultDictionary.Add("DateRange", String
                                    .Join(
                                    ",",
                                    Enumerable.Range(0, 1 + Convert.ToDateTime(ev.EndDate).Subtract(Convert.ToDateTime(ev.StartDate)).Days)
                                    .Select(offset => Convert.ToDateTime(ev.StartDate).AddDays(offset))
                                    .ToArray()));

                                resultDictionary.Add("StartDate", Convert.ToDateTime(ev.StartDate).ToString("yyyy-MM-ddThh:mm:ss"));
                                resultDictionary.Add("EndDate", Convert.ToDateTime(ev.EndDate).ToString("yyyy-MM-ddThh:mm:ss"));
                            }
                            catch
                            {
                                try
                                {
                                    resultDictionary.Add("DateRange", String
                                        .Join(
                                        ",",
                                        Enumerable.Range(0, 1 + Convert.ToDateTime(ev.StartDate).Subtract(Convert.ToDateTime(ev.EndDate)).Days)
                                        .Select(offset => Convert.ToDateTime(ev.EndDate).AddDays(offset))
                                        .ToArray()));

                                    resultDictionary.Add("StartDate", Convert.ToDateTime(ev.EndDate).ToString("yyyy-MM-ddThh:mm:ss"));
                                    resultDictionary.Add("EndDate", Convert.ToDateTime(ev.StartDate).ToString("yyyy-MM-ddThh:mm:ss"));
                                }
                                catch
                                {
                                    resultDictionary.Add("DateRange", "");
                                    resultDictionary.Add("StartDate", "N/A");
                                    resultDictionary.Add("EndDate", "N/A");
                                }

                            }

                            resultDictionary.Add("IsCommon", ev.IsCommon == null ? "" : ev.IsCommon.ToString());
                            resultDictionary.Add("IsHoliday", ev.IsHoliday == null ? "" : ev.IsHoliday.ToString());
                            resultDictionary.Add("IsExam", ev.IsExam == null ? "" : ev.IsExam.ToString());
                            resultDictionary.Add("IsDue", ev.IsDue == null ? "" : ev.IsDue.ToString());
                            resultDictionary.Add("CreatedAt", ev.CreatedAt == null ? "" : Convert.ToDateTime(ev.CreatedAt).ToString("yyyy-MM-ddThh:mm:ss"));
                            resultDictionary.Add("UpdatedAt", ev.UpdatedAt == null ? "" : Convert.ToDateTime(ev.UpdatedAt).ToString("yyyy-MM-ddThh:mm:ss"));
                            resultDictionary.Add("OriginID", ev.OriginID == null ? "" : ev.OriginID.ToString());
                            resultDictionary.Add("OriginType", ev.OriginType == null ? "" : ev.OriginType.ToString());
                            resultDictionary.Add("SchoolID", ev.SchoolID == null ? "" : ev.SchoolID.ToString());
                            resultDictionary.Add("IsPublished", ev.IsPublished == null ? "" : ev.IsPublished.ToString());

                            ListResultDictionary.Add(resultDictionary);
                        }

                        resultJSON.AddRange(ListResultDictionary);

                    }
                    catch { }

                    try
                    {
                        var resultUserEvents = (from ue in fedenaEntities.user_events

                                                join e in fedenaEntities.events on ue.event_id equals e.id

                                                join ass in fedenaEntities.assignments on e.origin_id equals ass.id into assignments_events
                                                from ass_ev in assignments_events.DefaultIfEmpty()

                                                where ue.user_id == UserID
                                                && e.school_id == SchoolID
                                                && e.start_date >= StartDate
                                                && e.end_date <= EndDate

                                                select new
                                                {
                                                    EventsID = e.id,
                                                    Title = e.title,
                                                    Description = e.description,
                                                    NotificationID = e.id,
                                                    StartDate = e.start_date,
                                                    EndDate = e.end_date,
                                                    IsCommon = e.is_common,
                                                    IsHoliday = e.is_holiday,
                                                    IsExam = e.is_exam,
                                                    IsDue = e.is_due,
                                                    CreatedAt = e.created_at,
                                                    UpdatedAt = e.updated_at,
                                                    OriginID = e.origin_id,
                                                    OriginType = e.origin_type,
                                                    SchoolID = e.school_id,
                                                    IsPublished = ass_ev.is_published
                                                }).Distinct().ToList();

                        foreach (var ev in resultUserEvents)
                        {
                            Dictionary<string, string> resultDictionary = new Dictionary<string, string>();

                            resultDictionary.Add("EventsID", ev.EventsID == null ? "" : ev.EventsID.ToString());
                            resultDictionary.Add("Title", ev.Title == null ? "" : ev.Title.ToString());
                            resultDictionary.Add("Description", ev.Description == null ? "" : ev.Description.ToString());
                            resultDictionary.Add("NotificationID", ev.NotificationID == null ? "" : ev.NotificationID.ToString());

                            try
                            {
                                resultDictionary.Add("DateRange", String
                                    .Join(
                                    ",",
                                    Enumerable.Range(0, 1 + Convert.ToDateTime(ev.EndDate).Subtract(Convert.ToDateTime(ev.StartDate)).Days)
                                    .Select(offset => Convert.ToDateTime(ev.StartDate).AddDays(offset))
                                    .ToArray()));

                                resultDictionary.Add("StartDate", Convert.ToDateTime(ev.StartDate).ToString("yyyy-MM-ddThh:mm:ss"));
                                resultDictionary.Add("EndDate", Convert.ToDateTime(ev.EndDate).ToString("yyyy-MM-ddThh:mm:ss"));
                            }
                            catch
                            {
                                try
                                {
                                    resultDictionary.Add("DateRange", String
                                        .Join(
                                        ",",
                                        Enumerable.Range(0, 1 + Convert.ToDateTime(ev.StartDate).Subtract(Convert.ToDateTime(ev.EndDate)).Days)
                                        .Select(offset => Convert.ToDateTime(ev.EndDate).AddDays(offset))
                                        .ToArray()));

                                    resultDictionary.Add("StartDate", Convert.ToDateTime(ev.EndDate).ToString("yyyy-MM-ddThh:mm:ss"));
                                    resultDictionary.Add("EndDate", Convert.ToDateTime(ev.StartDate).ToString("yyyy-MM-ddThh:mm:ss"));
                                }
                                catch
                                {
                                    resultDictionary.Add("DateRange", "");
                                    resultDictionary.Add("StartDate", "N/A");
                                    resultDictionary.Add("EndDate", "N/A");
                                }

                            }

                            resultDictionary.Add("IsCommon", ev.IsCommon == null ? "" : ev.IsCommon.ToString());
                            resultDictionary.Add("IsHoliday", ev.IsHoliday == null ? "" : ev.IsHoliday.ToString());
                            resultDictionary.Add("IsExam", ev.IsExam == null ? "" : ev.IsExam.ToString());
                            resultDictionary.Add("IsDue", ev.IsDue == null ? "" : ev.IsDue.ToString());
                            resultDictionary.Add("CreatedAt", ev.CreatedAt == null ? "" : Convert.ToDateTime(ev.CreatedAt).ToString("yyyy-MM-ddThh:mm:ss"));
                            resultDictionary.Add("UpdatedAt", ev.UpdatedAt == null ? "" : Convert.ToDateTime(ev.UpdatedAt).ToString("yyyy-MM-ddThh:mm:ss"));
                            resultDictionary.Add("OriginID", ev.OriginID == null ? "" : ev.OriginID.ToString());
                            resultDictionary.Add("OriginType", ev.OriginType == null ? "" : ev.OriginType.ToString());
                            resultDictionary.Add("SchoolID", ev.SchoolID == null ? "" : ev.SchoolID.ToString());
                            resultDictionary.Add("IsPublished", ev.IsPublished == null ? "" : ev.IsPublished.ToString());

                            ListResultDictionary.Add(resultDictionary);
                        }

                        resultJSON.AddRange(ListResultDictionary);

                    }
                    catch { }


                    try
                    {
                        var resultBatchEvents = (from be in fedenaEntities.batch_events

                                                 join e in fedenaEntities.events on be.event_id equals e.id

                                                 join ass in fedenaEntities.assignments on e.origin_id equals ass.id into assignments_events
                                                 from ass_ev in assignments_events.DefaultIfEmpty()

                                                 where be.school_id == SchoolID
                                                 //&& be.batch_id == BatchID
                                                 && (e.is_exam == true || e.origin_type == "Assignment")
                                                && e.start_date >= StartDate
                                                && e.end_date <= EndDate

                                                 select new
                                                 {
                                                     EventsID = e.id,
                                                     Title = e.title,
                                                     Description = e.description,
                                                     NotificationID = e.id,
                                                     StartDate = e.start_date,
                                                     EndDate = e.end_date,
                                                     IsCommon = e.is_common,
                                                     IsHoliday = e.is_holiday,
                                                     IsExam = e.is_exam,
                                                     IsDue = e.is_due,
                                                     CreatedAt = e.created_at,
                                                     UpdatedAt = e.updated_at,
                                                     OriginID = e.origin_id,
                                                     OriginType = e.origin_type,
                                                     SchoolID = e.school_id,
                                                     IsPublished = ass_ev.is_published
                                                 }).Distinct().ToList();

                        foreach (var ev in resultBatchEvents)
                        {
                            Dictionary<string, string> resultDictionary = new Dictionary<string, string>();

                            resultDictionary.Add("EventsID", ev.EventsID == null ? "" : ev.EventsID.ToString());
                            resultDictionary.Add("Title", ev.Title == null ? "" : ev.Title.ToString());
                            resultDictionary.Add("Description", ev.Description == null ? "" : ev.Description.ToString());
                            resultDictionary.Add("NotificationID", ev.NotificationID == null ? "" : ev.NotificationID.ToString());

                            try
                            {
                                resultDictionary.Add("DateRange", String
                                    .Join(
                                    ",",
                                    Enumerable.Range(0, 1 + Convert.ToDateTime(ev.EndDate).Subtract(Convert.ToDateTime(ev.StartDate)).Days)
                                    .Select(offset => Convert.ToDateTime(ev.StartDate).AddDays(offset))
                                    .ToArray()));

                                resultDictionary.Add("StartDate", Convert.ToDateTime(ev.StartDate).ToString("yyyy-MM-ddThh:mm:ss"));
                                resultDictionary.Add("EndDate", Convert.ToDateTime(ev.EndDate).ToString("yyyy-MM-ddThh:mm:ss"));
                            }
                            catch
                            {
                                try
                                {
                                    resultDictionary.Add("DateRange", String
                                        .Join(
                                        ",",
                                        Enumerable.Range(0, 1 + Convert.ToDateTime(ev.StartDate).Subtract(Convert.ToDateTime(ev.EndDate)).Days)
                                        .Select(offset => Convert.ToDateTime(ev.EndDate).AddDays(offset))
                                        .ToArray()));

                                    resultDictionary.Add("StartDate", Convert.ToDateTime(ev.EndDate).ToString("yyyy-MM-ddThh:mm:ss"));
                                    resultDictionary.Add("EndDate", Convert.ToDateTime(ev.StartDate).ToString("yyyy-MM-ddThh:mm:ss"));
                                }
                                catch
                                {
                                    resultDictionary.Add("DateRange", "");
                                    resultDictionary.Add("StartDate", "N/A");
                                    resultDictionary.Add("EndDate", "N/A");
                                }

                            }

                            resultDictionary.Add("IsCommon", ev.IsCommon == null ? "" : ev.IsCommon.ToString());
                            resultDictionary.Add("IsHoliday", ev.IsHoliday == null ? "" : ev.IsHoliday.ToString());
                            resultDictionary.Add("IsExam", ev.IsExam == null ? "" : ev.IsExam.ToString());
                            resultDictionary.Add("IsDue", ev.IsDue == null ? "" : ev.IsDue.ToString());
                            resultDictionary.Add("CreatedAt", ev.CreatedAt == null ? "" : Convert.ToDateTime(ev.CreatedAt).ToString("yyyy-MM-ddThh:mm:ss"));
                            resultDictionary.Add("UpdatedAt", ev.UpdatedAt == null ? "" : Convert.ToDateTime(ev.UpdatedAt).ToString("yyyy-MM-ddThh:mm:ss"));
                            resultDictionary.Add("OriginID", ev.OriginID == null ? "" : ev.OriginID.ToString());
                            resultDictionary.Add("OriginType", ev.OriginType == null ? "" : ev.OriginType.ToString());
                            resultDictionary.Add("SchoolID", ev.SchoolID == null ? "" : ev.SchoolID.ToString());
                            resultDictionary.Add("IsPublished", ev.IsPublished == null ? "" : ev.IsPublished.ToString());

                            ListResultDictionary.Add(resultDictionary);
                        }

                        resultJSON.AddRange(ListResultDictionary);
                    }
                    catch { }

                    resultJSON = resultJSON.Distinct().ToList<object>();
                }
            }
            else { // not published

                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    var resultEvents = (from e in fedenaEntities.events

                                        where //e.is_common == true && 
                                        e.school_id == SchoolID
                                        && e.start_date >= StartDate
                                        && e.end_date <= EndDate

                                        select new
                                        {
                                            EventsID = e.id,
                                            Title = e.title,
                                            Description = e.description,
                                            NotificationID = e.id,
                                            StartDate = e.start_date,
                                            EndDate = e.end_date,
                                            IsCommon = e.is_common,
                                            IsHoliday = e.is_holiday,
                                            IsExam = e.is_exam,
                                            IsDue = e.is_due,
                                            CreatedAt = e.created_at,
                                            UpdatedAt = e.updated_at,
                                            OriginID = e.origin_id,
                                            OriginType = e.origin_type,
                                            SchoolID = e.school_id
                                        }).Distinct().ToList();


                    foreach (var ev in resultEvents)
                    {
                        Dictionary<string, string> resultDictionary = new Dictionary<string, string>();

                        resultDictionary.Add("EventsID", ev.EventsID == null ? "" : ev.EventsID.ToString());
                        resultDictionary.Add("Title", ev.Title == null ? "" : ev.Title.ToString());
                        resultDictionary.Add("Description", ev.Description == null ? "" : ev.Description.ToString());
                        resultDictionary.Add("NotificationID", ev.NotificationID == null ? "" : ev.NotificationID.ToString());

                        try
                        {
                            resultDictionary.Add("DateRange", String
                                .Join(
                                ",",
                                Enumerable.Range(0, 1 + Convert.ToDateTime(ev.EndDate).Subtract(Convert.ToDateTime(ev.StartDate)).Days)
                                .Select(offset => Convert.ToDateTime(ev.StartDate).AddDays(offset))
                                .ToArray()));

                            resultDictionary.Add("StartDate", Convert.ToDateTime(ev.StartDate).ToString("yyyy-MM-ddThh:mm:ss"));
                            resultDictionary.Add("EndDate", Convert.ToDateTime(ev.EndDate).ToString("yyyy-MM-ddThh:mm:ss"));
                        }
                        catch
                        {
                            try
                            {
                                resultDictionary.Add("DateRange", String
                                    .Join(
                                    ",",
                                    Enumerable.Range(0, 1 + Convert.ToDateTime(ev.StartDate).Subtract(Convert.ToDateTime(ev.EndDate)).Days)
                                    .Select(offset => Convert.ToDateTime(ev.EndDate).AddDays(offset))
                                    .ToArray()));

                                resultDictionary.Add("StartDate", Convert.ToDateTime(ev.EndDate).ToString("yyyy-MM-ddThh:mm:ss"));
                                resultDictionary.Add("EndDate", Convert.ToDateTime(ev.StartDate).ToString("yyyy-MM-ddThh:mm:ss"));
                            }
                            catch
                            {
                                resultDictionary.Add("DateRange", "");
                                resultDictionary.Add("StartDate", "N/A");
                                resultDictionary.Add("EndDate", "N/A");
                            }

                        }

                        resultDictionary.Add("IsCommon", ev.IsCommon == null ? "" : ev.IsCommon.ToString());
                        resultDictionary.Add("IsHoliday", ev.IsHoliday == null ? "" : ev.IsHoliday.ToString());
                        resultDictionary.Add("IsExam", ev.IsExam == null ? "" : ev.IsExam.ToString());
                        resultDictionary.Add("IsDue", ev.IsDue == null ? "" : ev.IsDue.ToString());
                        resultDictionary.Add("CreatedAt", ev.CreatedAt == null ? "" : Convert.ToDateTime(ev.CreatedAt).ToString("yyyy-MM-ddThh:mm:ss"));
                        resultDictionary.Add("UpdatedAt", ev.UpdatedAt == null ? "" : Convert.ToDateTime(ev.UpdatedAt).ToString("yyyy-MM-ddThh:mm:ss"));
                        resultDictionary.Add("OriginID", ev.OriginID == null ? "" : ev.OriginID.ToString());
                        resultDictionary.Add("OriginType", ev.OriginType == null ? "" : ev.OriginType.ToString());
                        resultDictionary.Add("SchoolID", ev.SchoolID == null ? "" : ev.SchoolID.ToString());
                        resultDictionary.Add("IsPublished", "True");

                        ListResultDictionary.Add(resultDictionary);
                    }

                    resultJSON.AddRange(ListResultDictionary);

                    try
                    {
                        int? EmployeeDepartmentID = (from emp in fedenaEntities.employees

                                                     where emp.user_id == UserID
                                                     && emp.school_id == SchoolID

                                                     select new
                                                     {
                                                         EmployeeDepartmentID = emp.employee_department_id
                                                     }).ToList().First().EmployeeDepartmentID;

                        var resultEmployeeDepartmentEvents = (from ede in fedenaEntities.employee_department_events

                                                              join e in fedenaEntities.employees on ede.employee_department_id equals e.employee_department_id

                                                              join ev in fedenaEntities.events on ede.event_id equals ev.id

                                                              where e.employee_department_id == EmployeeDepartmentID
                                                              && ev.start_date >= StartDate
                                                              && ev.end_date <= EndDate

                                                              select new
                                                              {
                                                                  EventsID = ev.id,
                                                                  Title = ev.title,
                                                                  Description = ev.description,
                                                                  NotificationID = ev.id,
                                                                  StartDate = ev.start_date,
                                                                  EndDate = ev.end_date,
                                                                  IsCommon = ev.is_common,
                                                                  IsHoliday = ev.is_holiday,
                                                                  IsExam = ev.is_exam,
                                                                  IsDue = ev.is_due,
                                                                  CreatedAt = ev.created_at,
                                                                  UpdatedAt = ev.updated_at,
                                                                  OriginID = ev.origin_id,
                                                                  OriginType = ev.origin_type,
                                                                  SchoolID = ev.school_id
                                                              }).Distinct().ToList();


                        foreach (var ev in resultEmployeeDepartmentEvents)
                        {
                            Dictionary<string, string> resultDictionary = new Dictionary<string, string>();

                            resultDictionary.Add("EventsID", ev.EventsID == null ? "" : ev.EventsID.ToString());
                            resultDictionary.Add("Title", ev.Title == null ? "" : ev.Title.ToString());
                            resultDictionary.Add("Description", ev.Description == null ? "" : ev.Description.ToString());
                            resultDictionary.Add("NotificationID", ev.NotificationID == null ? "" : ev.NotificationID.ToString());

                            try
                            {
                                resultDictionary.Add("DateRange", String
                                    .Join(
                                    ",",
                                    Enumerable.Range(0, 1 + Convert.ToDateTime(ev.EndDate).Subtract(Convert.ToDateTime(ev.StartDate)).Days)
                                    .Select(offset => Convert.ToDateTime(ev.StartDate).AddDays(offset))
                                    .ToArray()));

                                resultDictionary.Add("StartDate", Convert.ToDateTime(ev.StartDate).ToString("yyyy-MM-ddThh:mm:ss"));
                                resultDictionary.Add("EndDate", Convert.ToDateTime(ev.EndDate).ToString("yyyy-MM-ddThh:mm:ss"));
                            }
                            catch
                            {
                                try
                                {
                                    resultDictionary.Add("DateRange", String
                                        .Join(
                                        ",",
                                        Enumerable.Range(0, 1 + Convert.ToDateTime(ev.StartDate).Subtract(Convert.ToDateTime(ev.EndDate)).Days)
                                        .Select(offset => Convert.ToDateTime(ev.EndDate).AddDays(offset))
                                        .ToArray()));

                                    resultDictionary.Add("StartDate", Convert.ToDateTime(ev.EndDate).ToString("yyyy-MM-ddThh:mm:ss"));
                                    resultDictionary.Add("EndDate", Convert.ToDateTime(ev.StartDate).ToString("yyyy-MM-ddThh:mm:ss"));
                                }
                                catch
                                {
                                    resultDictionary.Add("DateRange", "");
                                    resultDictionary.Add("StartDate", "N/A");
                                    resultDictionary.Add("EndDate", "N/A");
                                }

                            }

                            resultDictionary.Add("IsCommon", ev.IsCommon == null ? "" : ev.IsCommon.ToString());
                            resultDictionary.Add("IsHoliday", ev.IsHoliday == null ? "" : ev.IsHoliday.ToString());
                            resultDictionary.Add("IsExam", ev.IsExam == null ? "" : ev.IsExam.ToString());
                            resultDictionary.Add("IsDue", ev.IsDue == null ? "" : ev.IsDue.ToString());
                            resultDictionary.Add("CreatedAt", ev.CreatedAt == null ? "" : Convert.ToDateTime(ev.CreatedAt).ToString("yyyy-MM-ddThh:mm:ss"));
                            resultDictionary.Add("UpdatedAt", ev.UpdatedAt == null ? "" : Convert.ToDateTime(ev.UpdatedAt).ToString("yyyy-MM-ddThh:mm:ss"));
                            resultDictionary.Add("OriginID", ev.OriginID == null ? "" : ev.OriginID.ToString());
                            resultDictionary.Add("OriginType", ev.OriginType == null ? "" : ev.OriginType.ToString());
                            resultDictionary.Add("SchoolID", ev.SchoolID == null ? "" : ev.SchoolID.ToString());
                            resultDictionary.Add("IsPublished", "True");

                            ListResultDictionary.Add(resultDictionary);
                        }

                        resultJSON.AddRange(ListResultDictionary);

                    }
                    catch { }

                    try
                    {
                        var resultUserEvents = (from ue in fedenaEntities.user_events

                                                join e in fedenaEntities.events on ue.event_id equals e.id

                                                where ue.user_id == UserID
                                                && e.school_id == SchoolID
                                                && e.start_date >= StartDate
                                                && e.end_date <= EndDate

                                                select new
                                                {
                                                    EventsID = e.id,
                                                    Title = e.title,
                                                    Description = e.description,
                                                    NotificationID = e.id,
                                                    StartDate = e.start_date,
                                                    EndDate = e.end_date,
                                                    IsCommon = e.is_common,
                                                    IsHoliday = e.is_holiday,
                                                    IsExam = e.is_exam,
                                                    IsDue = e.is_due,
                                                    CreatedAt = e.created_at,
                                                    UpdatedAt = e.updated_at,
                                                    OriginID = e.origin_id,
                                                    OriginType = e.origin_type,
                                                    SchoolID = e.school_id
                                                }).Distinct().ToList();

                        foreach (var ev in resultUserEvents)
                        {
                            Dictionary<string, string> resultDictionary = new Dictionary<string, string>();

                            resultDictionary.Add("EventsID", ev.EventsID == null ? "" : ev.EventsID.ToString());
                            resultDictionary.Add("Title", ev.Title == null ? "" : ev.Title.ToString());
                            resultDictionary.Add("Description", ev.Description == null ? "" : ev.Description.ToString());
                            resultDictionary.Add("NotificationID", ev.NotificationID == null ? "" : ev.NotificationID.ToString());

                            try
                            {
                                resultDictionary.Add("DateRange", String
                                    .Join(
                                    ",",
                                    Enumerable.Range(0, 1 + Convert.ToDateTime(ev.EndDate).Subtract(Convert.ToDateTime(ev.StartDate)).Days)
                                    .Select(offset => Convert.ToDateTime(ev.StartDate).AddDays(offset))
                                    .ToArray()));

                                resultDictionary.Add("StartDate", Convert.ToDateTime(ev.StartDate).ToString("yyyy-MM-ddThh:mm:ss"));
                                resultDictionary.Add("EndDate", Convert.ToDateTime(ev.EndDate).ToString("yyyy-MM-ddThh:mm:ss"));
                            }
                            catch
                            {
                                try
                                {
                                    resultDictionary.Add("DateRange", String
                                        .Join(
                                        ",",
                                        Enumerable.Range(0, 1 + Convert.ToDateTime(ev.StartDate).Subtract(Convert.ToDateTime(ev.EndDate)).Days)
                                        .Select(offset => Convert.ToDateTime(ev.EndDate).AddDays(offset))
                                        .ToArray()));

                                    resultDictionary.Add("StartDate", Convert.ToDateTime(ev.EndDate).ToString("yyyy-MM-ddThh:mm:ss"));
                                    resultDictionary.Add("EndDate", Convert.ToDateTime(ev.StartDate).ToString("yyyy-MM-ddThh:mm:ss"));
                                }
                                catch
                                {
                                    resultDictionary.Add("DateRange", "");
                                    resultDictionary.Add("StartDate", "N/A");
                                    resultDictionary.Add("EndDate", "N/A");
                                }

                            }

                            resultDictionary.Add("IsCommon", ev.IsCommon == null ? "" : ev.IsCommon.ToString());
                            resultDictionary.Add("IsHoliday", ev.IsHoliday == null ? "" : ev.IsHoliday.ToString());
                            resultDictionary.Add("IsExam", ev.IsExam == null ? "" : ev.IsExam.ToString());
                            resultDictionary.Add("IsDue", ev.IsDue == null ? "" : ev.IsDue.ToString());
                            resultDictionary.Add("CreatedAt", ev.CreatedAt == null ? "" : Convert.ToDateTime(ev.CreatedAt).ToString("yyyy-MM-ddThh:mm:ss"));
                            resultDictionary.Add("UpdatedAt", ev.UpdatedAt == null ? "" : Convert.ToDateTime(ev.UpdatedAt).ToString("yyyy-MM-ddThh:mm:ss"));
                            resultDictionary.Add("OriginID", ev.OriginID == null ? "" : ev.OriginID.ToString());
                            resultDictionary.Add("OriginType", ev.OriginType == null ? "" : ev.OriginType.ToString());
                            resultDictionary.Add("SchoolID", ev.SchoolID == null ? "" : ev.SchoolID.ToString());
                            resultDictionary.Add("IsPublished", "True");

                            ListResultDictionary.Add(resultDictionary);
                        }

                        resultJSON.AddRange(ListResultDictionary);

                    }
                    catch { }


                    try
                    {
                        var resultBatchEvents = (from be in fedenaEntities.batch_events

                                                 join e in fedenaEntities.events on be.event_id equals e.id

                                                 where be.school_id == SchoolID
                                                 //&& be.batch_id == BatchID
                                                 && (e.is_exam == true || e.origin_type == "Assignment")
                                                && e.start_date >= StartDate
                                                && e.end_date <= EndDate


                                                 select new
                                                 {
                                                     EventsID = e.id,
                                                     Title = e.title,
                                                     Description = e.description,
                                                     NotificationID = e.id,
                                                     StartDate = e.start_date,
                                                     EndDate = e.end_date,
                                                     IsCommon = e.is_common,
                                                     IsHoliday = e.is_holiday,
                                                     IsExam = e.is_exam,
                                                     IsDue = e.is_due,
                                                     CreatedAt = e.created_at,
                                                     UpdatedAt = e.updated_at,
                                                     OriginID = e.origin_id,
                                                     OriginType = e.origin_type,
                                                     SchoolID = e.school_id
                                                 }).Distinct().ToList();

                        foreach (var ev in resultBatchEvents)
                        {
                            Dictionary<string, string> resultDictionary = new Dictionary<string, string>();

                            resultDictionary.Add("EventsID", ev.EventsID == null ? "" : ev.EventsID.ToString());
                            resultDictionary.Add("Title", ev.Title == null ? "" : ev.Title.ToString());
                            resultDictionary.Add("Description", ev.Description == null ? "" : ev.Description.ToString());
                            resultDictionary.Add("NotificationID", ev.NotificationID == null ? "" : ev.NotificationID.ToString());

                            try
                            {
                                resultDictionary.Add("DateRange", String
                                    .Join(
                                    ",",
                                    Enumerable.Range(0, 1 + Convert.ToDateTime(ev.EndDate).Subtract(Convert.ToDateTime(ev.StartDate)).Days)
                                    .Select(offset => Convert.ToDateTime(ev.StartDate).AddDays(offset))
                                    .ToArray()));

                                resultDictionary.Add("StartDate", Convert.ToDateTime(ev.StartDate).ToString("yyyy-MM-ddThh:mm:ss"));
                                resultDictionary.Add("EndDate", Convert.ToDateTime(ev.EndDate).ToString("yyyy-MM-ddThh:mm:ss"));
                            }
                            catch
                            {
                                try
                                {
                                    resultDictionary.Add("DateRange", String
                                        .Join(
                                        ",",
                                        Enumerable.Range(0, 1 + Convert.ToDateTime(ev.StartDate).Subtract(Convert.ToDateTime(ev.EndDate)).Days)
                                        .Select(offset => Convert.ToDateTime(ev.EndDate).AddDays(offset))
                                        .ToArray()));

                                    resultDictionary.Add("StartDate", Convert.ToDateTime(ev.EndDate).ToString("yyyy-MM-ddThh:mm:ss"));
                                    resultDictionary.Add("EndDate", Convert.ToDateTime(ev.StartDate).ToString("yyyy-MM-ddThh:mm:ss"));
                                }
                                catch
                                {
                                    resultDictionary.Add("DateRange", "");
                                    resultDictionary.Add("StartDate", "N/A");
                                    resultDictionary.Add("EndDate", "N/A");
                                }

                            }

                            resultDictionary.Add("IsCommon", ev.IsCommon == null ? "" : ev.IsCommon.ToString());
                            resultDictionary.Add("IsHoliday", ev.IsHoliday == null ? "" : ev.IsHoliday.ToString());
                            resultDictionary.Add("IsExam", ev.IsExam == null ? "" : ev.IsExam.ToString());
                            resultDictionary.Add("IsDue", ev.IsDue == null ? "" : ev.IsDue.ToString());
                            resultDictionary.Add("CreatedAt", ev.CreatedAt == null ? "" : Convert.ToDateTime(ev.CreatedAt).ToString("yyyy-MM-ddThh:mm:ss"));
                            resultDictionary.Add("UpdatedAt", ev.UpdatedAt == null ? "" : Convert.ToDateTime(ev.UpdatedAt).ToString("yyyy-MM-ddThh:mm:ss"));
                            resultDictionary.Add("OriginID", ev.OriginID == null ? "" : ev.OriginID.ToString());
                            resultDictionary.Add("OriginType", ev.OriginType == null ? "" : ev.OriginType.ToString());
                            resultDictionary.Add("SchoolID", ev.SchoolID == null ? "" : ev.SchoolID.ToString());
                            resultDictionary.Add("IsPublished", "True");

                            ListResultDictionary.Add(resultDictionary);
                        }

                        resultJSON.AddRange(ListResultDictionary);
                    }
                    catch { }

                    resultJSON = resultJSON.Distinct().ToList<object>();
                }
            }

            return resultJSON;
        }


        [Route("GetEventsByDates")]
        public List<object> GetEventsByDates(DateTime StartDate, DateTime EndDate, int SchoolID)
        {
            List<object> resultJSON = new List<object>();
            List<Dictionary<string, string>> ListResultDictionary = new List<Dictionary<string, string>>();
            bool PublishAgenda = new bool();

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                PublishAgenda = Convert.ToBoolean((from msc in fedenaEntities.madrasatie_system_configs where msc.school_id == SchoolID select msc.publish_agenda).ToList()[0]);
            }

            if (PublishAgenda) //school is published
            {
                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    //var fedenaEntities.assignments = (from e in fedenaEntities.events

                    //                       join ass in fedenaEntities.assignments on e.origin_id equals ass.id into assignments_events
                    //                       from ass_ev in assignments_events.DefaultIfEmpty()

                    //                       where ass_ev.is_published == true
                    //                       select ass_ev);

                    var result = (from e in fedenaEntities.events

                                 join ass in fedenaEntities.assignments on e.origin_id equals ass.id into assignments_events
                                 from ass_ev in assignments_events.DefaultIfEmpty()

                                 where e.start_date >= StartDate
                                 && e.end_date <= EndDate
                                 && e.school_id == SchoolID

                                 select new
                                 {
                                     EventsID = e.id,
                                     Title = e.title,
                                     Description = e.description,
                                     NotificationID = e.id,
                                     StartDate = e.start_date,
                                     EndDate = e.end_date,
                                     IsCommon = e.is_common,
                                     IsHoliday = e.is_holiday,
                                     IsExam = e.is_exam,
                                     IsDue = e.is_due,
                                     CreatedAt = e.created_at,
                                     UpdatedAt = e.updated_at,
                                     OriginID = e.origin_id,
                                     OriginType = e.origin_type,
                                     SchoolID = e.school_id,
                                     IsPublished = ass_ev.is_published
                                 }).ToList();

                    foreach (var ev in result)
                    {
                        Dictionary<string, string> resultDictionary = new Dictionary<string, string>();

                        resultDictionary.Add("EventsID", ev.EventsID == null ? "" : ev.EventsID.ToString());
                        resultDictionary.Add("Title", ev.Title == null ? "" : ev.Title.ToString());
                        resultDictionary.Add("Description", ev.Description == null ? "" : ev.Description.ToString());
                        resultDictionary.Add("NotificationID", ev.NotificationID == null ? "" : ev.NotificationID.ToString());

                        try
                        {
                            resultDictionary.Add("DateRange", String
                                .Join(
                                ",",
                                Enumerable.Range(0, 1 + Convert.ToDateTime(ev.EndDate).Subtract(Convert.ToDateTime(ev.StartDate)).Days)
                                .Select(offset => Convert.ToDateTime(ev.StartDate).AddDays(offset))
                                .ToArray()));

                            resultDictionary.Add("StartDate", Convert.ToDateTime(ev.StartDate).ToString("yyyy-MM-ddThh:mm:ss"));
                            resultDictionary.Add("EndDate", Convert.ToDateTime(ev.EndDate).ToString("yyyy-MM-ddThh:mm:ss"));
                        }
                        catch
                        {
                            try
                            {
                                resultDictionary.Add("DateRange", String
                                    .Join(
                                    ",",
                                    Enumerable.Range(0, 1 + Convert.ToDateTime(ev.StartDate).Subtract(Convert.ToDateTime(ev.EndDate)).Days)
                                    .Select(offset => Convert.ToDateTime(ev.EndDate).AddDays(offset))
                                    .ToArray()));

                                resultDictionary.Add("StartDate", Convert.ToDateTime(ev.EndDate).ToString("yyyy-MM-ddThh:mm:ss"));
                                resultDictionary.Add("EndDate", Convert.ToDateTime(ev.StartDate).ToString("yyyy-MM-ddThh:mm:ss"));
                            }
                            catch
                            {
                                resultDictionary.Add("DateRange", "");
                                resultDictionary.Add("StartDate", "N/A");
                                resultDictionary.Add("EndDate", "N/A");
                            }

                        }

                        resultDictionary.Add("IsCommon", ev.IsCommon == null ? "" : ev.IsCommon.ToString());
                        resultDictionary.Add("IsHoliday", ev.IsHoliday == null ? "" : ev.IsHoliday.ToString());
                        resultDictionary.Add("IsExam", ev.IsExam == null ? "" : ev.IsExam.ToString());
                        resultDictionary.Add("IsDue", ev.IsDue == null ? "" : ev.IsDue.ToString());
                        resultDictionary.Add("CreatedAt", ev.CreatedAt == null ? "" : Convert.ToDateTime(ev.CreatedAt).ToString("yyyy-MM-ddThh:mm:ss"));
                        resultDictionary.Add("UpdatedAt", ev.UpdatedAt == null ? "" : Convert.ToDateTime(ev.UpdatedAt).ToString("yyyy-MM-ddThh:mm:ss"));
                        resultDictionary.Add("OriginID", ev.OriginID == null ? "" : ev.OriginID.ToString());
                        resultDictionary.Add("OriginType", ev.OriginType == null ? "" : ev.OriginType.ToString());
                        resultDictionary.Add("SchoolID", ev.SchoolID == null ? "" : ev.SchoolID.ToString());
                        resultDictionary.Add("IsPublished", ev.IsPublished == null ? "" : ev.IsPublished.ToString());

                        ListResultDictionary.Add(resultDictionary);
                    }

                    resultJSON = ListResultDictionary.ToList<object>();
                }

            }
            else // school not published
            {
                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    var result = from e in fedenaEntities.events

                                 where e.start_date >= StartDate
                                 && e.end_date <= EndDate
                                 && e.school_id == SchoolID

                                 select new
                                 {
                                     EventsID = e.id,
                                     Title = e.title,
                                     Description = e.description,
                                     NotificationID = e.id,
                                     StartDate = e.start_date,
                                     EndDate = e.end_date,
                                     IsCommon = e.is_common,
                                     IsHoliday = e.is_holiday,
                                     IsExam = e.is_exam,
                                     IsDue = e.is_due,
                                     CreatedAt = e.created_at,
                                     UpdatedAt = e.updated_at,
                                     OriginID = e.origin_id,
                                     OriginType = e.origin_type,
                                     SchoolID = e.school_id
                                 };

                    foreach (var ev in result)
                    {
                        Dictionary<string, string> resultDictionary = new Dictionary<string, string>();

                        resultDictionary.Add("EventsID", ev.EventsID == null ? "" : ev.EventsID.ToString());
                        resultDictionary.Add("Title", ev.Title == null ? "" : ev.Title.ToString());
                        resultDictionary.Add("Description", ev.Description == null ? "" : ev.Description.ToString());
                        resultDictionary.Add("NotificationID", ev.NotificationID == null ? "" : ev.NotificationID.ToString());

                        try
                        {
                            resultDictionary.Add("DateRange", String
                                .Join(
                                ",",
                                Enumerable.Range(0, 1 + Convert.ToDateTime(ev.EndDate).Subtract(Convert.ToDateTime(ev.StartDate)).Days)
                                .Select(offset => Convert.ToDateTime(ev.StartDate).AddDays(offset))
                                .ToArray()));

                            resultDictionary.Add("StartDate", Convert.ToDateTime(ev.StartDate).ToString("yyyy-MM-ddThh:mm:ss"));
                            resultDictionary.Add("EndDate", Convert.ToDateTime(ev.EndDate).ToString("yyyy-MM-ddThh:mm:ss"));
                        }
                        catch
                        {
                            try
                            {
                                resultDictionary.Add("DateRange", String
                                    .Join(
                                    ",",
                                    Enumerable.Range(0, 1 + Convert.ToDateTime(ev.StartDate).Subtract(Convert.ToDateTime(ev.EndDate)).Days)
                                    .Select(offset => Convert.ToDateTime(ev.EndDate).AddDays(offset))
                                    .ToArray()));

                                resultDictionary.Add("StartDate", Convert.ToDateTime(ev.EndDate).ToString("yyyy-MM-ddThh:mm:ss"));
                                resultDictionary.Add("EndDate", Convert.ToDateTime(ev.StartDate).ToString("yyyy-MM-ddThh:mm:ss"));
                            }
                            catch
                            {
                                resultDictionary.Add("DateRange", "");
                                resultDictionary.Add("StartDate", "N/A");
                                resultDictionary.Add("EndDate", "N/A");
                            }

                        }

                        resultDictionary.Add("IsCommon", ev.IsCommon == null ? "" : ev.IsCommon.ToString());
                        resultDictionary.Add("IsHoliday", ev.IsHoliday == null ? "" : ev.IsHoliday.ToString());
                        resultDictionary.Add("IsExam", ev.IsExam == null ? "" : ev.IsExam.ToString());
                        resultDictionary.Add("IsDue", ev.IsDue == null ? "" : ev.IsDue.ToString());
                        resultDictionary.Add("CreatedAt", ev.CreatedAt == null ? "" : Convert.ToDateTime(ev.CreatedAt).ToString("yyyy-MM-ddThh:mm:ss"));
                        resultDictionary.Add("UpdatedAt", ev.UpdatedAt == null ? "" : Convert.ToDateTime(ev.UpdatedAt).ToString("yyyy-MM-ddThh:mm:ss"));
                        resultDictionary.Add("OriginID", ev.OriginID == null ? "" : ev.OriginID.ToString());
                        resultDictionary.Add("OriginType", ev.OriginType == null ? "" : ev.OriginType.ToString());
                        resultDictionary.Add("SchoolID", ev.SchoolID == null ? "" : ev.SchoolID.ToString());
                        resultDictionary.Add("IsPublished", "True");

                        ListResultDictionary.Add(resultDictionary);
                    }

                    resultJSON = ListResultDictionary.ToList<object>();
                }
            }

            return resultJSON;
        }


        [Route("GetEventDetails")]
        public List<object> GetEventDetails(int eventID)
        {
            List<object> resultJSON = new List<object>();
            List<Dictionary<string, string>> ListResultDictionary = new List<Dictionary<string, string>>();

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                var result = from e in fedenaEntities.events

                             where e.id == eventID

                             select new
                             {
                                 EventsID = e.id,
                                 Title = e.title,
                                 Description = e.description,
                                 NotificationID = e.id,
                                 StartDate = e.start_date,
                                 EndDate = e.end_date,
                                 IsCommon = e.is_common,
                                 IsHoliday = e.is_holiday,
                                 IsExam = e.is_exam,
                                 IsDue = e.is_due,
                                 CreatedAt = e.created_at,
                                 UpdatedAt = e.updated_at,
                                 OriginID = e.origin_id,
                                 OriginType = e.origin_type,
                                 SchoolID = e.school_id
                             };

                foreach (var ev in result)
                {
                    Dictionary<string, string> resultDictionary = new Dictionary<string, string>();

                    resultDictionary.Add("EventsID", ev.EventsID == null ? "" : ev.EventsID.ToString());
                    resultDictionary.Add("Title", ev.Title == null ? "" : ev.Title.ToString());
                    resultDictionary.Add("Description", ev.Description == null ? "" : ev.Description.ToString());
                    resultDictionary.Add("NotificationID", ev.NotificationID == null ? "" : ev.NotificationID.ToString());

                    try
                    {
                        resultDictionary.Add("DateRange", String
                            .Join(
                            ",",
                            Enumerable.Range(0, 1 + Convert.ToDateTime(ev.EndDate).Subtract(Convert.ToDateTime(ev.StartDate)).Days)
                            .Select(offset => Convert.ToDateTime(ev.StartDate).AddDays(offset))
                            .ToArray()));

                        resultDictionary.Add("StartDate", Convert.ToDateTime(ev.StartDate).ToString("yyyy-MM-ddThh:mm:ss"));
                        resultDictionary.Add("EndDate", Convert.ToDateTime(ev.EndDate).ToString("yyyy-MM-ddThh:mm:ss"));
                    }
                    catch
                    {
                        try
                        {
                            resultDictionary.Add("DateRange", String
                                .Join(
                                ",",
                                Enumerable.Range(0, 1 + Convert.ToDateTime(ev.StartDate).Subtract(Convert.ToDateTime(ev.EndDate)).Days)
                                .Select(offset => Convert.ToDateTime(ev.EndDate).AddDays(offset))
                                .ToArray()));

                            resultDictionary.Add("StartDate", Convert.ToDateTime(ev.EndDate).ToString("yyyy-MM-ddThh:mm:ss"));
                            resultDictionary.Add("EndDate", Convert.ToDateTime(ev.StartDate).ToString("yyyy-MM-ddThh:mm:ss"));
                        }
                        catch
                        {
                            resultDictionary.Add("DateRange", "");
                            resultDictionary.Add("StartDate", "N/A");
                            resultDictionary.Add("EndDate", "N/A");
                        }

                    }

                    resultDictionary.Add("IsCommon", ev.IsCommon == null ? "" : ev.IsCommon.ToString());
                    resultDictionary.Add("IsHoliday", ev.IsHoliday == null ? "" : ev.IsHoliday.ToString());
                    resultDictionary.Add("IsExam", ev.IsExam == null ? "" : ev.IsExam.ToString());
                    resultDictionary.Add("IsDue", ev.IsDue == null ? "" : ev.IsDue.ToString());
                    resultDictionary.Add("CreatedAt", ev.CreatedAt == null ? "" : Convert.ToDateTime(ev.CreatedAt).ToString("yyyy-MM-ddThh:mm:ss"));
                    resultDictionary.Add("UpdatedAt", ev.UpdatedAt == null ? "" : Convert.ToDateTime(ev.UpdatedAt).ToString("yyyy-MM-ddThh:mm:ss"));
                    resultDictionary.Add("OriginID", ev.OriginID == null ? "" : ev.OriginID.ToString());
                    resultDictionary.Add("OriginType", ev.OriginType == null ? "" : ev.OriginType.ToString());
                    resultDictionary.Add("SchoolID", ev.SchoolID == null ? "" : ev.SchoolID.ToString());

                    ListResultDictionary.Add(resultDictionary);
                }

                resultJSON = ListResultDictionary.ToList<object>();
                //resultJSON = result.ToList<object>();
            }

            return resultJSON;
        }


        [Route("UpdateEvents")]
        public async Task<List<Dictionary<string, string>>> UpdateEvents(string Title, string Description, int? SchoolID, DateTime? StartDate, DateTime? EndDate, bool? IsCommon, bool? IsHoliday, int EventID)
        {
            try
            {
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                List<@event> result = new List<@event>();

                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    result = (from ev in fedenaEntities.events

                              where ev.id == EventID

                              select ev).ToList<@event>();

                    foreach (var ev in result)
                    {
                        ev.title = Title;
                        ev.description = Description;
                        ev.start_date = StartDate;
                        ev.end_date = EndDate;
                        ev.is_common = IsCommon;
                        ev.is_holiday = IsHoliday;

                        fedenaEntities.SaveChanges();
                    }

                    Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                    if (result.Count > 0)
                    {
                        OutputMessage.Add("Message", "Success");
                    }
                    else
                    {
                        OutputMessage.Add("Message", "No records were updated");
                    }

                    OutputMessageList.Add(OutputMessage);
                }

                try
                {
                    List<Dictionary<string, string>> ListOfUsers = await UsersPerSchoolID(Convert.ToInt32(SchoolID));

                    foreach (var user in ListOfUsers)
                    {
                        try
                        {
                            var NotifyUserID = Convert.ToInt32(user.First(x => x.Key == "UserID").Value);
                            var NotifySchoolID = Convert.ToInt32(user.First(x => x.Key == "SchoolID").Value);

                            List<Dictionary<string, string>> DeviceInformation = GetUserToken(NotifyUserID, NotifySchoolID);

                            foreach (var device in DeviceInformation)
                            {
                                string studentToken = device["Token"];
                                string deviceType = device["DeviceType"];

                                string eventMessage = updatePushNotificationMessage(StartDate.ToString());

                                PushNotification(studentToken, eventMessage, deviceType, Globals.Configuration.MPNS_PRODUCTION);
                            }
                        }
                        catch { }
                    }
                }
                catch { }

                


                return OutputMessageList;
            }
            catch
            {
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                OutputMessage.Add("Message", "Error");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
        }


        [Route("UpdateEventForClass")]
        public async Task<List<Dictionary<string, string>>> UpdateEventForClass(int[] BatchIDList, string Title, string Description, int? SchoolID, DateTime? StartDate, DateTime? EndDate, bool? IsCommon, bool? IsHoliday, int EventID)
        {
            try
            {
                int resultEventID;
                List<object> resultJSON = new List<object>();
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();
                List<@event> resultEvents = new List<@event>();


                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    resultEvents = (from ev in fedenaEntities.events

                                    where ev.id == EventID

                                    select ev).ToList<@event>();

                    foreach (var ev in resultEvents)
                    {
                        ev.title = Title;
                        ev.description = Description;
                        ev.start_date = StartDate;
                        ev.end_date = EndDate;
                        ev.is_common = IsCommon;
                        ev.is_holiday = IsHoliday;

                        fedenaEntities.SaveChanges();
                    }

                    foreach (var BatchID in BatchIDList)
                    {
                        var MaxCreatedAt = (from ev in fedenaEntities.events select new { ev.created_at }).Max(x => x.created_at);

                        var result = (from be in fedenaEntities.batch_events

                                      where be.event_id == EventID

                                      select be).ToList<batch_events>();

                        foreach (var be in result)
                        {
                            fedenaEntities.batch_events.Remove(be);
                            fedenaEntities.SaveChanges();
                        }

                        var resultEventsList = (from e in fedenaEntities.events

                                                where e.id == EventID

                                                select e).ToList<@event>();

                        foreach (var ev in result)
                        {
                            batch_events BatchEvents = new batch_events();
                            BatchEvents.id = EventID;
                            BatchEvents.batch_id = BatchID;
                            BatchEvents.created_at = ev.created_at;
                            BatchEvents.school_id = SchoolID;

                            fedenaEntities.batch_events.Add(BatchEvents);

                            fedenaEntities.SaveChanges();
                        }

                        try
                        {
                            List<Dictionary<string, string>> ListOfUsers = await UserPerBatch(BatchID);

                            foreach (var user in ListOfUsers)
                            {
                                try
                                {
                                    var NotifyUserID = Convert.ToInt32(user.First(x => x.Key == "UserID").Value);
                                    var NotifySchoolID = Convert.ToInt32(user.First(x => x.Key == "SchoolID").Value);

                                    List<Dictionary<string, string>> DeviceInformation = GetUserToken(NotifyUserID, NotifySchoolID);

                                    foreach (var device in DeviceInformation)
                                    {
                                        string studentToken = device["Token"];
                                        string deviceType = device["DeviceType"];

                                        string eventMessage = updatePushNotificationMessage(StartDate.ToString());

                                        PushNotification(studentToken, eventMessage, deviceType, Globals.Configuration.MPNS_PRODUCTION);
                                    }
                                }
                                catch { }
                            }
                        }
                        catch { }

                        

                        try
                        {
                            List<Dictionary<string, string>> GuardiansListOfUsers = await GuardiansPerBatch(BatchID);

                            foreach (var user in GuardiansListOfUsers)
                            {
                                try
                                {
                                    var NotifyUserID = Convert.ToInt32(user.First(x => x.Key == "UserID").Value);
                                    var NotifySchoolID = Convert.ToInt32(user.First(x => x.Key == "SchoolID").Value);

                                    List<Dictionary<string, string>> DeviceInformation = GetUserToken(NotifyUserID, NotifySchoolID);

                                    foreach (var device in DeviceInformation)
                                    {
                                        string studentToken = device["Token"];
                                        string deviceType = device["DeviceType"];

                                        string eventMessage = updatePushNotificationMessage(StartDate.ToString());

                                        PushNotification(studentToken, eventMessage, deviceType, Globals.Configuration.MPNS_PRODUCTION);
                                    }
                                }
                                catch { }
                            }
                        }
                        catch { }
                       
                    }
                }

                OutputMessage.Add("Message", "Success");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
            catch
            {
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                OutputMessage.Add("Message", "Error");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
        }

        [Route("UpdateEventForDepartment")]
        public List<Dictionary<string, string>> UpdateEventForDepartment(int[] EmployeeDepartmentIDList, string Title, string Description, int? SchoolID, DateTime? StartDate, DateTime? EndDate, bool? IsCommon, bool? IsHoliday, int EventID)
        {
            try
            {
                int resultEventID;
                List<object> resultJSON = new List<object>();
                List<@event> resultEvents = new List<@event>();
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    resultEvents = (from ev in fedenaEntities.events

                              where ev.id == EventID

                              select ev).ToList<@event>();

                    foreach (var ev in resultEvents)
                    {
                        ev.title = Title;
                        ev.description = Description;
                        ev.start_date = StartDate;
                        ev.end_date = EndDate;
                        ev.is_common = IsCommon;
                        ev.is_holiday = IsHoliday;

                        fedenaEntities.SaveChanges();
                    }
                }

                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    foreach (var EmployeeDepartmentID in EmployeeDepartmentIDList)
                    {
                        var result = (from ede in fedenaEntities.employee_department_events

                                      where ede.event_id == EventID

                                      select ede).ToList<employee_department_events>();

                        foreach (var ede in result)
                        {
                            fedenaEntities.employee_department_events.Remove(ede);
                            fedenaEntities.SaveChanges();
                        }


                        var resultEventsList = (from ev in fedenaEntities.events

                                                where ev.id == EventID

                                                select ev).ToList<@event>();

                        foreach (var ev in resultEventsList)
                        {
                            employee_department_events EmployeeDepartmentEvent = new employee_department_events();
                            EmployeeDepartmentEvent.event_id = EventID;
                            EmployeeDepartmentEvent.employee_department_id = EmployeeDepartmentID;
                            EmployeeDepartmentEvent.created_at = ev.created_at;
                            EmployeeDepartmentEvent.school_id = SchoolID;

                            fedenaEntities.employee_department_events.Add(EmployeeDepartmentEvent);

                            fedenaEntities.SaveChanges();
                        }
                    }
                }

                OutputMessage.Add("Message", "Success");

                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
            catch
            {
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                OutputMessage.Add("Message", "Error");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
        }

        [Route("UpdateEventForDepartmentAndClasses")]
        public async Task<List<Dictionary<string, string>>> UpdateEventForDepartmentAndClasses(Dictionary<string, List<int>> BatchIDEmployeeDepartmentID, string Title, string Description, int? SchoolID, DateTime? StartDate, DateTime? EndDate, bool? IsCommon, bool? IsHoliday, int EventID)
        {
            try
            {
                List<int> BatchIDList = new List<int>();
                List<int> EmployeeDepartmentList = new List<int>();

                //foreach (var item in BatchIDEmployeeDepartmentID)
                //{
                try
                {
                    foreach (var BatchID in BatchIDEmployeeDepartmentID["BatchIDList"])
                    {
                        BatchIDList.Add(BatchID);
                    }
                }
                catch
                {

                }

                try
                {
                    foreach (var EmployeeDepartmentID in BatchIDEmployeeDepartmentID["EmployeeDepartmentIDList"])
                    {
                        EmployeeDepartmentList.Add(EmployeeDepartmentID);
                    }
                }
                catch
                {

                }
                //}


                int resultEventID;
                List<object> resultJSON = new List<object>();
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();
                List<@event> resultEvents = new List<@event>();


                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    resultEvents = (from ev in fedenaEntities.events

                                    where ev.id == EventID

                                    select ev).ToList<@event>();

                    foreach (var ev in resultEvents)
                    {
                        ev.title = Title;
                        ev.description = Description;
                        ev.start_date = StartDate;
                        ev.end_date = EndDate;
                        ev.is_common = IsCommon;
                        ev.is_holiday = IsHoliday;

                        fedenaEntities.SaveChanges();
                    }

                    foreach (var BatchID in BatchIDList)
                    {
                        var MaxCreatedAt = (from ev in fedenaEntities.events select new { ev.created_at }).Max(x => x.created_at);

                        var result = (from be in fedenaEntities.batch_events

                                      where be.event_id == EventID

                                      select be).ToList<batch_events>();

                        foreach (var be in result)
                        {
                            fedenaEntities.batch_events.Remove(be);
                            fedenaEntities.SaveChanges();
                        }

                        var resultEventsList = (from e in fedenaEntities.events

                                                where e.id == EventID

                                                select e).ToList<@event>();

                        foreach (var ev in result)
                        {
                            batch_events BatchEvents = new batch_events();
                            BatchEvents.id = EventID;
                            BatchEvents.batch_id = BatchID;
                            BatchEvents.created_at = ev.created_at;
                            BatchEvents.school_id = SchoolID;

                            fedenaEntities.batch_events.Add(BatchEvents);

                            fedenaEntities.SaveChanges();
                        }


                        try
                        {
                            List<Dictionary<string, string>> ListOfUsers = await UserPerBatch(BatchID);

                            foreach (var user in ListOfUsers)
                            {
                                try
                                {
                                    var NotifyUserID = Convert.ToInt32(user.First(x => x.Key == "UserID").Value);
                                    var NotifySchoolID = Convert.ToInt32(user.First(x => x.Key == "SchoolID").Value);

                                    List<Dictionary<string, string>> DeviceInformation = GetUserToken(NotifyUserID, NotifySchoolID);

                                    foreach (var device in DeviceInformation)
                                    {
                                        string studentToken = device["Token"];
                                        string deviceType = device["DeviceType"];

                                        string eventMessage = updatePushNotificationMessage(StartDate.ToString());

                                        PushNotification(studentToken, "", deviceType, Globals.Configuration.MPNS_PRODUCTION);
                                    }
                                }
                                catch { }
                            }
                        }
                        catch { }

                        


                        try
                        {
                            List<Dictionary<string, string>> GuardiansListOfUsers = await GuardiansPerBatch(BatchID);

                            foreach (var user in GuardiansListOfUsers)
                            {
                                try
                                {
                                    var NotifyUserID = Convert.ToInt32(user.First(x => x.Key == "UserID").Value);
                                    var NotifySchoolID = Convert.ToInt32(user.First(x => x.Key == "SchoolID").Value);

                                    List<Dictionary<string, string>> DeviceInformation = GetUserToken(NotifyUserID, NotifySchoolID);

                                    foreach (var device in DeviceInformation)
                                    {
                                        string studentToken = device["Token"];
                                        string deviceType = device["DeviceType"];

                                        string eventMessage = updatePushNotificationMessage(StartDate.ToString());

                                        PushNotification(studentToken, "", deviceType, Globals.Configuration.MPNS_PRODUCTION);
                                    }
                                }
                                catch { }
                            }
                        }
                        catch { }
                        



                    }
                }

                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    resultEvents = (from ev in fedenaEntities.events

                                    where ev.id == EventID

                                    select ev).ToList<@event>();

                    foreach (var ev in resultEvents)
                    {
                        ev.title = Title;
                        ev.description = Description;
                        ev.start_date = StartDate;
                        ev.end_date = EndDate;
                        ev.is_common = IsCommon;
                        ev.is_holiday = IsHoliday;

                        fedenaEntities.SaveChanges();
                    }
                }

                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    foreach (var EmployeeDepartmentID in EmployeeDepartmentList)
                    {
                        var result = (from ede in fedenaEntities.employee_department_events

                                      where ede.event_id == EventID

                                      select ede).ToList<employee_department_events>();

                        foreach (var ede in result)
                        {
                            fedenaEntities.employee_department_events.Remove(ede);
                            fedenaEntities.SaveChanges();
                        }


                        var resultEventsList = (from ev in fedenaEntities.events

                                                where ev.id == EventID

                                                select ev).ToList<@event>();

                        foreach (var ev in resultEventsList)
                        {
                            employee_department_events EmployeeDepartmentEvent = new employee_department_events();
                            EmployeeDepartmentEvent.event_id = EventID;
                            EmployeeDepartmentEvent.employee_department_id = EmployeeDepartmentID;
                            EmployeeDepartmentEvent.created_at = ev.created_at;
                            EmployeeDepartmentEvent.school_id = SchoolID;

                            fedenaEntities.employee_department_events.Add(EmployeeDepartmentEvent);

                            fedenaEntities.SaveChanges();
                        }
                    }
                }

                OutputMessage.Add("Message", "Success");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
            catch
            {
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                OutputMessage.Add("Message", "Error");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
        }



        [Route("DeleteFromEvents")]
        public List<Dictionary<string, string>> DeleteFromEvents(int EventID)
        {
            try
            {
                List<object> resultJSON = new List<object>();

                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    var result = from ev in fedenaEntities.events
                                 where ev.id == EventID
                                 select ev;

                    foreach (var ev in result)
                    {
                        fedenaEntities.events.Remove(ev);
                    }

                    fedenaEntities.SaveChanges();

                    resultJSON = result.ToList<object>();
                }

                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                if (resultJSON.Count > 0)
                {
                    OutputMessage.Add("Message", "Success");
                }else
                {
                    OutputMessage.Add("Message", "No records were updated");
                }
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
            catch
            {
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                OutputMessage.Add("Message", "Error");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
        }


        [Route("NewEvent")]
        public List<Dictionary<string, string>> NewEvent(string Title, string Description, int? SchoolID, DateTime? StartDate, DateTime? EndDate, bool? IsCommon, bool? IsHoliday)
        {
            try
            {
                int resultEventID;
                List<object> resultJSON = new List<object>();
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    @event newEvent = new @event();

                    newEvent.created_at = DateTime.Now;
                    newEvent.description = Description;
                    newEvent.end_date = EndDate;
                    newEvent.is_common = IsCommon;
                    newEvent.is_due = false;
                    newEvent.is_exam = false;
                    newEvent.is_holiday = IsHoliday;
                    newEvent.origin_id = null;
                    newEvent.origin_type = null;
                    newEvent.school_id = SchoolID;
                    newEvent.start_date = StartDate;
                    newEvent.title = Title;
                    newEvent.updated_at = null;

                    fedenaEntities.events.Add(newEvent);

                    fedenaEntities.SaveChanges();

                    var MaxCreatedAtEvent = (from ev in fedenaEntities.events select new { ev.created_at }).Max(x => x.created_at);

                    resultEventID = (from ev in fedenaEntities.events

                                     where ev.created_at == MaxCreatedAtEvent
                                     && ev.school_id == SchoolID
                                     select new
                                     {
                                         EventID = ev.id,
                                     }).ToList().First().EventID;
                }

                OutputMessage.Add("Message", "Success");
                OutputMessage.Add("EventID", resultEventID.ToString());

                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
            catch
            {
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                OutputMessage.Add("Message", "Error");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
        }


        [Route("GetEmployeeDepartments")]
        public List<object> GetEmployeeDepartments(int SchoolID)
        {
            List<object> resultJSON = new List<object>();

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                var result = from ed in fedenaEntities.employee_departments

                             where ed.school_id == SchoolID

                             select new
                             {
                                 EmployeeDepartmentID = ed.id,
                                 EmployeeDepartmentCode = ed.code,
                                 EmployeeDepartmentName = ed.name,
                                 EmployeeDepartmentStatus = ed.status,
                                 EmployeeDepartmentUpdatedAt = ed.updated_at,
                                 EmployeeDepartmentCreatedAt = ed.created_at,
                                 EmployeeDepartmentSchoolID = ed.school_id
                             };

                resultJSON = result.ToList<object>();
            }

            return resultJSON;
        }



        [Route("NewEmployeeDepartmentEvents")]
        public async Task<List<Dictionary<string, string>>> NewEmployeeDepartmentEvents(int EmployeeDepartmentID, int SchoolID)
        {
            try
            {
                List<object> resultJSON = new List<object>();
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    var MaxCreatedAt = (from ev in fedenaEntities.events select new { ev.created_at }).Max(x => x.created_at);

                    var result = (from ede in fedenaEntities.employee_department_events

                                  where ede.created_at == MaxCreatedAt

                                  select new
                                  {
                                      EventID = ede.id,
                                      EmployeeDepartmentID = EmployeeDepartmentID,
                                      CreatedAt = DateTime.Now,
                                      SchoolID = SchoolID,
                                  }).ToList();

                    foreach (var ev in result)
                    {
                        employee_department_events EmployeeDepartmentEvent = new employee_department_events();
                        EmployeeDepartmentEvent.id = ev.EventID;
                        EmployeeDepartmentEvent.employee_department_id = ev.EmployeeDepartmentID;
                        EmployeeDepartmentEvent.created_at = ev.CreatedAt;
                        EmployeeDepartmentEvent.school_id = ev.SchoolID;

                        fedenaEntities.employee_department_events.Add(EmployeeDepartmentEvent);

                        fedenaEntities.SaveChanges();


                        try
                        {
                            List<Dictionary<string, string>> ListOfUsers = await EmployeePerDepartment(EmployeeDepartmentID);

                            foreach (var user in ListOfUsers)
                            {
                                try
                                {
                                    var NotifyUserID = Convert.ToInt32(user.First(x => x.Key == "UserID").Value);
                                    var NotifySchoolID = Convert.ToInt32(user.First(x => x.Key == "SchoolID").Value);

                                    List<Dictionary<string, string>> DeviceInformation = GetUserToken(NotifyUserID, NotifySchoolID);

                                    foreach (var device in DeviceInformation)
                                    {
                                        string studentToken = device["Token"];
                                        string deviceType = device["DeviceType"];

                                        string eventMessage = newPushNotificationMessage(ev.CreatedAt.ToString());

                                        PushNotification(studentToken, eventMessage, deviceType, Globals.Configuration.MPNS_PRODUCTION);
                                    }
                                }
                                catch { }
                            }
                        }
                        catch { }
                    }

                    if (result.Count == 0)
                    {
                        OutputMessage.Add("Message", "No Data Inserted");
                        OutputMessageList.Add(OutputMessage);
                        return OutputMessageList;
                    }
                }

                OutputMessage.Add("Message", "Success");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
            catch
            {
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                OutputMessage.Add("Message", "Error");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
        }



        [Route("NewBatchEvent")]
        public async Task<List<Dictionary<string, string>>> NewBatchEvent(int BatchID, int SchoolID)
        {
            try
            {
                List<object> resultJSON = new List<object>();
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    var MaxCreatedAt = (from ev in fedenaEntities.events select new { ev.created_at }).Max(x => x.created_at);

                    var result = (from be in fedenaEntities.events

                                  where be.created_at == MaxCreatedAt

                                  select new
                                  {
                                      EventID = be.id,
                                      BatchID = BatchID,
                                      CreatedAt = DateTime.Now,
                                      SchoolID = SchoolID,
                                  }).ToList();

                    foreach (var ev in result)
                    {
                        batch_events BatchEvents = new batch_events();
                        BatchEvents.id = ev.EventID;
                        BatchEvents.batch_id = ev.BatchID;
                        BatchEvents.created_at = ev.CreatedAt;
                        BatchEvents.school_id = ev.SchoolID;

                        fedenaEntities.batch_events.Add(BatchEvents);

                        fedenaEntities.SaveChanges();

                        try
                        {
                            List<Dictionary<string, string>> ListOfUsers = await UserPerBatch(BatchID);

                            foreach (var user in ListOfUsers)
                            {
                                try
                                {
                                    var NotifyUserID = Convert.ToInt32(user.First(x => x.Key == "UserID").Value);
                                    var NotifySchoolID = Convert.ToInt32(user.First(x => x.Key == "SchoolID").Value);

                                    List<Dictionary<string, string>> DeviceInformation = GetUserToken(NotifyUserID, NotifySchoolID);

                                    foreach (var device in DeviceInformation)
                                    {
                                        string studentToken = device["Token"];
                                        string deviceType = device["DeviceType"];

                                        string eventMessage = newPushNotificationMessage(ev.CreatedAt.ToString());

                                        PushNotification(studentToken, eventMessage, deviceType, Globals.Configuration.MPNS_PRODUCTION);
                                    }
                                }
                                catch { }
                            }
                        }
                        catch { }

                        try
                        {
                            List<Dictionary<string, string>> ListOfUsersGuardians = await GuardiansPerBatch(BatchID);

                            foreach (var user in ListOfUsersGuardians)
                            {
                                try
                                {
                                    var NotifyUserID = Convert.ToInt32(user.First(x => x.Key == "UserID").Value);
                                    var NotifySchoolID = Convert.ToInt32(user.First(x => x.Key == "SchoolID").Value);

                                    List<Dictionary<string, string>> DeviceInformation = GetUserToken(NotifyUserID, NotifySchoolID);

                                    foreach (var device in DeviceInformation)
                                    {
                                        string studentToken = device["Token"];
                                        string deviceType = device["DeviceType"];

                                        string eventMessage = newPushNotificationMessage(ev.CreatedAt.ToString());

                                        PushNotification(studentToken, eventMessage, deviceType, Globals.Configuration.MPNS_PRODUCTION);
                                    }
                                }
                                catch { }
                            }
                        }
                        catch { }
                    }

                    if (result.Count == 0)
                    {
                        OutputMessage.Add("Message", "No Data Inserted");
                        OutputMessageList.Add(OutputMessage);
                        return OutputMessageList;
                    }
                }        

                OutputMessage.Add("Message", "Success");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
            catch
            {
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                OutputMessage.Add("Message", "Error");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
        }


        [Route("GetMadrasatieRemarksForEmployee")]
        public List<object> GetMadrasatieRemarksForEmployee(int EmployeeID, int SchoolID)
        {
            List<object> resultJSON = new List<object>();

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                var result = from mr in fedenaEntities.madrasatie_remarks

                             join mer in fedenaEntities.madrasatie_employee_remarks on mr.madrasatie_remarks_list_id equals mer.madrasatie_remarks_list_id

                             where mer.employee_id == EmployeeID
                             && mr.school_id == SchoolID

                             select new
                             {
                                 RemarkID = mr.id,
                                 RemarkText = mr.text,
                                 RemarkListID = mr.madrasatie_remarks_list_id,
                                 SchoolID = mr.school_id,
                                 CreatedAt = mr.created_at,
                                 UpdatedAt = mr.updated_at,
                                 Positive = mr.positive
                             };

                resultJSON = result.ToList<object>();
            }

            return resultJSON;
        }




        [Route("NewRemark")]
        public List<Dictionary<string, string>> NewRemark(int[] StudentIDList, string FullName, int BatchID, int UserID, int SchoolID, string RemarkSubject, string RemarkBody, DateTime CreatedAt)
        {
            try
            {
                List<object> resultJSON = new List<object>();
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    var PublishRemarks = (from pr in fedenaEntities.madrasatie_system_configs where pr.school_id == SchoolID select new { pr.publish_remarks }).ToList().First().publish_remarks;
                    PublishRemarks = PublishRemarks == false ? true : PublishRemarks;

                    foreach(var StudentID in StudentIDList)
                    {
                        var result = (from rs in fedenaEntities.remark_settings

                                      where rs.target == "custom_remark"

                                      select new
                                      {
                                          RemarkSettingID = rs.id,
                                          StudentID = StudentID,
                                          BatchID = BatchID,
                                          UserID = UserID,
                                          RemarkSubject = RemarkSubject,
                                          RemarkBody = RemarkBody,
                                          RemarkedBy = FullName,
                                          SchoolID = SchoolID,
                                          CreatedAt = DateTime.Now,
                                          IsPublished = PublishRemarks
                                      }).ToList();


                        foreach (var rs in result)
                        {
                            remark newRemark = new remark();
                            newRemark.target_id = rs.RemarkSettingID;
                            newRemark.student_id = rs.StudentID;
                            newRemark.batch_id = rs.BatchID;
                            newRemark.submitted_by = rs.UserID;
                            newRemark.remark_subject = rs.RemarkSubject;
                            newRemark.remark_body = rs.RemarkBody;
                            newRemark.remarked_by = rs.RemarkedBy;
                            newRemark.created_at = rs.CreatedAt;
                            newRemark.school_id = SchoolID;
                            newRemark.is_published = rs.IsPublished;

                            fedenaEntities.remarks.Add(newRemark);

                            fedenaEntities.SaveChanges();
                        }
                    }                    
                }

                OutputMessage.Add("Message", "Success");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
            catch
            {
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                OutputMessage.Add("Message", "Error");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
        }



        [Route("GetAllRemarksPerSchool")]
        public List<object> GetAllRemarksPerSchool(int SchoolID)
        {
            List<object> resultJSON = new List<object>();

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                var result = from mr in fedenaEntities.madrasatie_remarks

                             where mr.school_id == SchoolID

                             select new
                             {
                                 RemarkID = mr.id,
                                 RemarkText = mr.text,
                                 RemarkListID = mr.madrasatie_remarks_list_id,
                                 SchoolID = mr.school_id,
                                 CreatedAt = mr.created_at,
                                 UpdatedAt = mr.updated_at,
                                 Positive = mr.positive
                             };

                resultJSON = result.ToList<object>();
            }

            return resultJSON;
        }


        [Route("NewEventForClass")]
        public async Task<List<Dictionary<string, string>>> NewEventForClass(int[] BatchIDList, string Title, string Description, int? SchoolID, DateTime? StartDate, DateTime? EndDate, bool? IsCommon, bool? IsHoliday)
        {
            try
            {
                int resultEventID;
                List<object> resultJSON = new List<object>();
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    @event newEvent = new @event();

                    newEvent.created_at = DateTime.Now;
                    newEvent.description = Description;
                    newEvent.end_date = EndDate;
                    newEvent.is_common = IsCommon;
                    newEvent.is_due = false;
                    newEvent.is_exam = false;
                    newEvent.is_holiday = IsHoliday;
                    newEvent.origin_id = null;
                    newEvent.origin_type = null;
                    newEvent.school_id = SchoolID;
                    newEvent.start_date = StartDate;
                    newEvent.title = Title;
                    newEvent.updated_at = null;

                    fedenaEntities.events.Add(newEvent);

                    fedenaEntities.SaveChanges();
                }

                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    foreach (var BatchID in BatchIDList)
                    {
                        var MaxCreatedAt = (from ev in fedenaEntities.events select new { ev.created_at }).Max(x => x.created_at);

                        var result = (from be in fedenaEntities.events

                                      where be.created_at == MaxCreatedAt

                                      select new
                                      {
                                          EventID = be.id,
                                          BatchID = BatchID,
                                          CreatedAt = DateTime.Now,
                                          SchoolID = SchoolID,
                                      }).ToList();

                        foreach (var ev in result)
                        {
                            batch_events BatchEvents = new batch_events();
                            //BatchEvents.id = ev.EventID;
                            BatchEvents.batch_id = ev.BatchID;
                            BatchEvents.created_at = ev.CreatedAt;
                            BatchEvents.school_id = ev.SchoolID;
                            BatchEvents.event_id = ev.EventID;

                            fedenaEntities.batch_events.Add(BatchEvents);

                            fedenaEntities.SaveChanges();

                            try
                            {
                                List<Dictionary<string, string>> ListOfUsers = await UserPerBatch(BatchID);

                                foreach (var user in ListOfUsers)
                                {
                                    try
                                    {
                                        var NotifyUserID = Convert.ToInt32(user.First(x => x.Key == "UserID").Value);
                                        var NotifySchoolID = Convert.ToInt32(user.First(x => x.Key == "SchoolID").Value);

                                        List<Dictionary<string, string>> DeviceInformation = GetUserToken(NotifyUserID, NotifySchoolID);

                                        foreach (var device in DeviceInformation)
                                        {
                                            string studentToken = device["Token"];
                                            string deviceType = device["DeviceType"];

                                            string eventMessage = newPushNotificationMessage(ev.CreatedAt.ToString());

                                            PushNotification(studentToken, eventMessage, deviceType, Globals.Configuration.MPNS_PRODUCTION);
                                        }
                                    }
                                    catch { }
                                }
                            }
                            catch { }


                            try
                            {
                                List<Dictionary<string, string>> GuardiansListOfUsers = await GuardiansPerBatch(BatchID);

                                foreach (var user in GuardiansListOfUsers)
                                {
                                    try
                                    {
                                        var NotifyUserID = Convert.ToInt32(user.First(x => x.Key == "UserID").Value);
                                        var NotifySchoolID = Convert.ToInt32(user.First(x => x.Key == "SchoolID").Value);

                                        List<Dictionary<string, string>> DeviceInformation = GetUserToken(NotifyUserID, NotifySchoolID);

                                        foreach (var device in DeviceInformation)
                                        {
                                            string studentToken = device["Token"];
                                            string deviceType = device["DeviceType"];

                                            string eventMessage = newPushNotificationMessage(ev.CreatedAt.ToString());

                                            PushNotification(studentToken, eventMessage, deviceType, Globals.Configuration.MPNS_PRODUCTION);
                                        }
                                    }
                                    catch { }
                                }
                            }
                            catch { }
                        }

                        if (result.Count == 0)
                        {
                            OutputMessage.Add("Message", "No Data Inserted");
                            OutputMessageList.Add(OutputMessage);
                            return OutputMessageList;
                        }
                    }

                    var MaxCreatedAtEvent = (from ev in fedenaEntities.events select new { ev.created_at }).Max(x => x.created_at);

                    resultEventID = (from ev in fedenaEntities.events

                                     where ev.created_at == MaxCreatedAtEvent
                                     && ev.school_id == SchoolID
                                     select new
                                     {
                                         EventID = ev.id,
                                     }).ToList().First().EventID;

                }

                OutputMessage.Add("Message", "Success");
                OutputMessage.Add("EventID", resultEventID.ToString());
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
            catch
            {
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                OutputMessage.Add("Message", "Error");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
        }

        [Route("NewEventForDepartment")]
        public async Task<List<Dictionary<string, string>>> NewEventForDepartment(int[] EmployeeDepartmentIDList, string Title, string Description, int? SchoolID, DateTime? StartDate, DateTime? EndDate, bool? IsCommon, bool? IsHoliday)
        {
            try
            {
                int resultEventID;
                List<object> resultJSON = new List<object>();
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    @event newEvent = new @event();

                    newEvent.created_at = DateTime.Now;
                    newEvent.description = Description;
                    newEvent.end_date = EndDate;
                    newEvent.is_common = IsCommon;
                    newEvent.is_due = false;
                    newEvent.is_exam = false;
                    newEvent.is_holiday = IsHoliday;
                    newEvent.origin_id = null;
                    newEvent.origin_type = null;
                    newEvent.school_id = SchoolID;
                    newEvent.start_date = StartDate;
                    newEvent.title = Title;
                    newEvent.updated_at = null;

                    fedenaEntities.events.Add(newEvent);

                    fedenaEntities.SaveChanges();
                }

                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    foreach (var EmployeeDepartmentID in EmployeeDepartmentIDList)
                    {
                        var MaxCreatedAt = (from ev in fedenaEntities.events select new { ev.created_at }).Max(x => x.created_at);

                        var result = (from ev in fedenaEntities.events

                                      where ev.created_at == MaxCreatedAt

                                      select new
                                      {
                                          EventID = ev.id,
                                          EmployeeDepartmentID = EmployeeDepartmentID,
                                          CreatedAt = DateTime.Now,
                                          SchoolID = SchoolID,
                                      }).ToList();

                        foreach (var ev in result)
                        {
                            employee_department_events EmployeeDepartmentEvent = new employee_department_events();
                            EmployeeDepartmentEvent.event_id = ev.EventID;
                            EmployeeDepartmentEvent.employee_department_id = ev.EmployeeDepartmentID;
                            EmployeeDepartmentEvent.created_at = ev.CreatedAt;
                            EmployeeDepartmentEvent.school_id = ev.SchoolID;

                            fedenaEntities.employee_department_events.Add(EmployeeDepartmentEvent);

                            fedenaEntities.SaveChanges();

                            try
                            {
                                List<Dictionary<string, string>> ListOfUsers = await EmployeePerDepartment(EmployeeDepartmentID);

                                foreach (var user in ListOfUsers)
                                {
                                    try
                                    {
                                        var NotifyUserID = Convert.ToInt32(user.First(x => x.Key == "UserID").Value);
                                        var NotifySchoolID = Convert.ToInt32(user.First(x => x.Key == "SchoolID").Value);

                                        List<Dictionary<string, string>> DeviceInformation = GetUserToken(NotifyUserID, NotifySchoolID);

                                        foreach (var device in DeviceInformation)
                                        {
                                            string studentToken = device["Token"];
                                            string deviceType = device["DeviceType"];

                                            string eventMessage = newPushNotificationMessage(ev.CreatedAt.ToString());

                                            PushNotification(studentToken, eventMessage, deviceType, Globals.Configuration.MPNS_PRODUCTION);
                                        }
                                    }
                                    catch { }
                                }
                            }
                            catch { }
                        }

                        if (result.Count == 0)
                        {
                            OutputMessage.Add("Message", "No Data Inserted");
                            OutputMessageList.Add(OutputMessage);
                            return OutputMessageList;
                        }

                        
                    }

                    var MaxCreatedAtEvent = (from ev in fedenaEntities.events select new { ev.created_at }).Max(x => x.created_at);

                    resultEventID = (from ev in fedenaEntities.events

                                     where ev.created_at == MaxCreatedAtEvent
                                     && ev.school_id == SchoolID
                                     select new
                                     {
                                         EventID = ev.id,
                                     }).ToList().First().EventID;

                }

                OutputMessage.Add("EventID", resultEventID.ToString());
                OutputMessage.Add("Message", "Success");

                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
            catch
            {
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                OutputMessage.Add("Message", "Error");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
        }

        [Route("NewEventForDepartmentAndClasses")]
        public async Task<List<Dictionary<string, string>>> NewEventForDepartmentAndClasses(Dictionary<string, List<int>> BatchIDEmployeeDepartmentID, string Title, string Description, int? SchoolID, DateTime? StartDate, DateTime? EndDate, bool? IsCommon, bool? IsHoliday)
        {
            try
            {
                List<int> BatchIDList = new List<int>();
                List<int> EmployeeDepartmentList = new List<int>();

                //try
                //{
                //    var BatchIDListArray = BatchIDEmployeeDepartmentID.Where(x => x.Key == "BatchIDList").Select(x => x.Value).ToList()[0].ToString().Replace("[", string.Empty).Replace("]", string.Empty).Split(',').ToList();

                //    foreach (var BatchID in BatchIDListArray)
                //    {
                //        BatchIDList.Add(Convert.ToInt32(GetNumbers(BatchID)));
                //    }
                //}
                //catch { }

                //try
                //{
                //    var EmployeeDepartmentIDListArray = BatchIDEmployeeDepartmentID.Where(x => x.Key == "EmployeeDepartmentIDList").Select(x => x.Value).ToList()[0].ToString().Replace("[", string.Empty).Replace("]", string.Empty).Split(',').ToList();

                //    foreach (var EmployeeDepartmentID in EmployeeDepartmentIDListArray)
                //    {
                //        EmployeeDepartmentList.Add(Convert.ToInt32(GetNumbers(EmployeeDepartmentID)));
                //    }
                //}
                //catch { }

                try
                {
                    foreach (var BatchID in BatchIDEmployeeDepartmentID["BatchIDList"])
                    {
                        BatchIDList.Add(BatchID);
                    }
                }
                catch { }

                try
                {
                    foreach (var EmployeeDepartmentID in BatchIDEmployeeDepartmentID["EmployeeDepartmentIDList"])
                    {
                        EmployeeDepartmentList.Add(EmployeeDepartmentID);
                    }
                }
                catch { }

                int resultEventID;
                List<object> resultJSON = new List<object>();
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    @event newEvent = new @event();

                    newEvent.created_at = DateTime.Now;
                    newEvent.description = Description;
                    newEvent.end_date = EndDate;
                    newEvent.is_common = IsCommon;
                    newEvent.is_due = false;
                    newEvent.is_exam = false;
                    newEvent.is_holiday = IsHoliday;
                    newEvent.origin_id = null;
                    newEvent.origin_type = null;
                    newEvent.school_id = SchoolID;
                    newEvent.start_date = StartDate;
                    newEvent.title = Title;
                    newEvent.updated_at = null;

                    fedenaEntities.events.Add(newEvent);

                    fedenaEntities.SaveChanges();
                }

                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    foreach (var EmployeeDepartmentID in EmployeeDepartmentList)
                    {
                        var MaxCreatedAt = (from ev in fedenaEntities.events select new { ev.created_at }).Max(x => x.created_at);

                        var result = (from ev in fedenaEntities.events

                                      where ev.created_at == MaxCreatedAt

                                      select new
                                      {
                                          EventID = ev.id,
                                          EmployeeDepartmentID = EmployeeDepartmentID,
                                          CreatedAt = DateTime.Now,
                                          SchoolID = SchoolID,
                                      }).ToList();

                        foreach (var ev in result)
                        {
                            employee_department_events EmployeeDepartmentEvent = new employee_department_events();
                            EmployeeDepartmentEvent.event_id = ev.EventID;
                            EmployeeDepartmentEvent.employee_department_id = ev.EmployeeDepartmentID;
                            EmployeeDepartmentEvent.created_at = ev.CreatedAt;
                            EmployeeDepartmentEvent.school_id = ev.SchoolID;

                            fedenaEntities.employee_department_events.Add(EmployeeDepartmentEvent);

                            fedenaEntities.SaveChanges();

                            try
                            {
                                List<Dictionary<string, string>> ListOfUsers = await EmployeePerDepartment(EmployeeDepartmentID);

                                foreach (var user in ListOfUsers)
                                {
                                    try
                                    {
                                        var NotifyUserID = Convert.ToInt32(user.First(x => x.Key == "UserID").Value);
                                        var NotifySchoolID = Convert.ToInt32(user.First(x => x.Key == "SchoolID").Value);

                                        List<Dictionary<string, string>> DeviceInformation = GetUserToken(NotifyUserID, NotifySchoolID);

                                        foreach (var device in DeviceInformation)
                                        {
                                            string studentToken = device["Token"];
                                            string deviceType = device["DeviceType"];

                                            string eventMessage = newPushNotificationMessage(ev.CreatedAt.ToString());

                                            PushNotification(studentToken, eventMessage, deviceType, Globals.Configuration.MPNS_PRODUCTION);
                                        }
                                    }
                                    catch { }
                                }
                            }
                            catch { }
                        }

                        if (result.Count == 0)
                        {
                            OutputMessage.Add("Message", "No Data Inserted");
                            OutputMessageList.Add(OutputMessage);
                            return OutputMessageList;
                        }

                        

                    }
                }

                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    foreach (var BatchID in BatchIDList)
                    {
                        var MaxCreatedAt = (from ev in fedenaEntities.events select new { ev.created_at }).Max(x => x.created_at);

                        var result = (from ev in fedenaEntities.events

                                      where ev.created_at == MaxCreatedAt

                                      select new
                                      {
                                          EventID = ev.id,
                                          BatchID = BatchID,
                                          CreatedAt = DateTime.Now,
                                          SchoolID = SchoolID,
                                      }).ToList();

                        foreach (var ev in result)
                        {
                            batch_events BatchEvents = new batch_events();
                            BatchEvents.event_id = ev.EventID;
                            BatchEvents.batch_id = ev.BatchID;
                            BatchEvents.created_at = ev.CreatedAt;
                            BatchEvents.school_id = ev.SchoolID;

                            fedenaEntities.batch_events.Add(BatchEvents);

                            fedenaEntities.SaveChanges();

                            try
                            {
                                List<Dictionary<string, string>> ListOfUsers = await UserPerBatch(BatchID);

                                foreach (var user in ListOfUsers)
                                {
                                    try
                                    {
                                        var NotifyUserID = Convert.ToInt32(user.First(x => x.Key == "UserID").Value);
                                        var NotifySchoolID = Convert.ToInt32(user.First(x => x.Key == "SchoolID").Value);

                                        List<Dictionary<string, string>> DeviceInformation = GetUserToken(NotifyUserID, NotifySchoolID);

                                        foreach (var device in DeviceInformation)
                                        {
                                            string studentToken = device["Token"];
                                            string deviceType = device["DeviceType"];

                                            string eventMessage = newPushNotificationMessage(ev.CreatedAt.ToString());

                                            PushNotification(studentToken, eventMessage, deviceType, Globals.Configuration.MPNS_PRODUCTION);
                                        }
                                    }
                                    catch { }
                                }
                            }
                            catch { }

                            try
                            {
                                List<Dictionary<string, string>> GuardiansListOfUsers = await GuardiansPerBatch(BatchID);

                                foreach (var user in GuardiansListOfUsers)
                                {
                                    try
                                    {
                                        var NotifyUserID = Convert.ToInt32(user.First(x => x.Key == "UserID").Value);
                                        var NotifySchoolID = Convert.ToInt32(user.First(x => x.Key == "SchoolID").Value);

                                        List<Dictionary<string, string>> DeviceInformation = GetUserToken(NotifyUserID, NotifySchoolID);

                                        foreach (var device in DeviceInformation)
                                        {
                                            string studentToken = device["Token"];
                                            string deviceType = device["DeviceType"];

                                            string eventMessage = newPushNotificationMessage(ev.CreatedAt.ToString());

                                            PushNotification(studentToken, eventMessage, deviceType, Globals.Configuration.MPNS_PRODUCTION);
                                        }
                                    }
                                    catch { }
                                }
                            }
                            catch { }
                        }

                        if (result.Count == 0)
                        {
                            OutputMessage.Add("Message", "No Data Inserted");
                            OutputMessageList.Add(OutputMessage);
                            return OutputMessageList;
                        }
                    }
                }

                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    var MaxCreatedAtEvent = (from ev in fedenaEntities.events select new { ev.created_at }).Max(x => x.created_at);

                    resultEventID = (from ev in fedenaEntities.events

                                     where ev.created_at == MaxCreatedAtEvent
                                     && ev.school_id == SchoolID
                                     select new
                                     {
                                         EventID = ev.id,
                                     }).ToList().First().EventID;
                }

                OutputMessage.Add("Message", "Success");
                OutputMessage.Add("EventID", resultEventID.ToString());

                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
            catch
            {
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                OutputMessage.Add("Message", "Error");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
        }


        [Route("GetRemarksByStudent")]
        public List<object> GetRemarksByStudent(int SchoolID, int StudentID)
        {
            List<object> resultJSON = new List<object>();

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                var result = from r in fedenaEntities.remarks

                             join rs in fedenaEntities.remark_settings on r.target_id equals rs.id

                             join msc in fedenaEntities.madrasatie_system_configs on r.school_id equals msc.school_id

                             join mr in fedenaEntities.madrasatie_remarks on r.madrasatie_remark_id equals mr.id into madrasatieRemarks_remarks
                             from mr_r in madrasatieRemarks_remarks.DefaultIfEmpty()

                             where r.school_id == SchoolID
                             //&& r.batch_id == BatchID
                             && r.is_published == (msc.publish_remarks == true ? true : r.is_published)//(CASE WHEN publish_remarks = 1 THEN   1 ELSE 0 END)
                             && r.student_id == StudentID

                             select new
                             {
                                 RemarkSubject = r.remark_subject,
                                 RemarkBody = r.remark_body,
                                 RemarkedBy = r.remarked_by,
                                 CreatedAt = r.created_at,
                                 Positive = mr_r.positive,
                                 RemarkID = r.id
                             };

                resultJSON = result.ToList<object>();
            }

            return resultJSON;
        }


        [Route("GetRemarksByBatch")]
        public List<object> GetRemarksByBatch(int SchoolID, int BatchID)
        {
            List<object> resultJSON = new List<object>();

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                var result = from r in fedenaEntities.remarks

                             join rs in fedenaEntities.remark_settings on r.target_id equals rs.id

                             join s in fedenaEntities.students on r.student_id equals s.id

                             join msc in fedenaEntities.madrasatie_system_configs on r.school_id equals msc.school_id

                             join mr in fedenaEntities.madrasatie_remarks on r.madrasatie_remark_id equals mr.id into madrasatieRemarks_remarks
                             from mr_r in madrasatieRemarks_remarks.DefaultIfEmpty()

                             where r.school_id == SchoolID
                             && r.batch_id == BatchID
                             && r.is_published == (msc.publish_remarks == true ? true : r.is_published)//(CASE WHEN publish_remarks = 1 THEN   1 ELSE 0 END)

                             select new
                             {
                                 FullName = s.first_name + " " + s.middle_name + " " + s.last_name,
                                 RemarkSubject = r.remark_subject,
                                 RemarkBody = r.remark_body,
                                 RemarkedBy = r.remarked_by,
                                 CreatedAt = r.created_at,
                                 Positive = mr_r.positive,
                                 RemarkID = r.id
                             };

                resultJSON = result.ToList<object>();
            }

            return resultJSON;
        }


        [Route("UpdateRemarks")]
        public List<Dictionary<string, string>> UpdateRemarks(int RemarkID, string RemarkBody)
        {
            try
            {
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                List<remark> result = new List<remark>();

                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    result = (from r in fedenaEntities.remarks

                              where r.id == RemarkID

                              select r).ToList<remark>();

                    foreach (var r in result)
                    {
                        r.remark_body = RemarkBody;

                        fedenaEntities.SaveChanges();
                    }

                    Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                    if (result.Count > 0)
                    {
                        OutputMessage.Add("Message", "Success");
                    }
                    else
                    {
                        OutputMessage.Add("Message", "No records were updated");
                    }

                    OutputMessageList.Add(OutputMessage);
                }

                return OutputMessageList;
            }
            catch
            {
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                OutputMessage.Add("Message", "Error");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
        }


        [Route("DeleteRemark")]
        public List<Dictionary<string, string>> DeleteRemark(int RemarkID)
        {
            try
            {
                List<object> resultJSON = new List<object>();

                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    var result = from r in fedenaEntities.remarks
                                 where r.id == RemarkID
                                 select r;

                    foreach (var remark in result)
                    {
                        fedenaEntities.remarks.Remove(remark);
                    }

                    fedenaEntities.SaveChanges();

                    resultJSON = result.ToList<object>();
                }


                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                OutputMessage.Add("Message", "Success");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
            catch
            {
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                OutputMessage.Add("Message", "Error");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
        }


        [Route("GetRemarkDetails")]
        public List<object> GetRemarkDetails(int RemarkID)
        {
            List<object> resultJSON = new List<object>();

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                var result = from r in fedenaEntities.remarks

                             where r.id == RemarkID

                             select new
                             {
                                 RemarkID = r.id,
                                 TargetID = r.target_id,
                                 StudentID = r.student_id,
                                 BatchID = r.batch_id,
                                 SubmittedBy = r.submitted_by,
                                 RemarkSubject = r.remark_subject,
                                 RemarkBody = r.remark_body,
                                 RemarkedBy = r.remarked_by,
                                 CreatedAt = r.created_at,
                                 UpdatedAt = r.updated_at,
                                 SchoolID = r.school_id,
                                 CurrentDate = r.current_date,
                                 IsPublished = r.is_published,
                                 MadrasatieRemarkID = r.madrasatie_remark_id
                             };

                resultJSON = result.ToList<object>();
            }

            return resultJSON;
        }


        [Route("GetStudentsForBatch")]
        public List<object> GetStudentsForBatch(int BatchID)
        {
            List<object> resultJSON = new List<object>();

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                var result = from s in fedenaEntities.students

                             join bs in fedenaEntities.batch_students on s.batch_id equals bs.batch_id into batchStudents_students
                             from bs_s in batchStudents_students.DefaultIfEmpty()

                             where bs_s.batch_id == BatchID

                             select new
                             {
                                 StudentID = s.id,
                                 StudentName = s.first_name + " " + s.last_name
                             };

                resultJSON = result.ToList<object>();
            }

            return resultJSON;
        }


        [Route("GetBanner")]
        public List<object> GetBanner(int SchoolID)
        {
            List<object> resultJSON = new List<object>();

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                var result = from sd in fedenaEntities.school_details

                             where sd.school_id == SchoolID


                             /// srv / Fedena / Fedena_app / uploads / (school_id_partitioned) / school_details / logos / (school_details_id_partitioned) / original / (file_name.type)
                             select new
                             {
                                 //BannerUrl = "srv/Fedena/Fedena_app/uploads/" + sd.school_id + "/school_details/logos/" + sd. + "/original/" + sd.logo_file_name
                             };

                resultJSON = result.ToList<object>();
            }
            
            return resultJSON;
        }



        [Route("StudentsPerBatch")]
        public List<object> StudentsPerBatch(int BatchID)
        {
            List<object> resultJSON = new List<object>();

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                var result = from st in fedenaEntities.students

                             where st.batch_id == BatchID

                             select new
                             {
                                 StudentID = st.id,
                                 FullName = st.first_name + " " + st.last_name,
                                 SchoolID = st.school_id
                             };

                resultJSON = result.ToList<object>();
            }

            return resultJSON;
        }

        [Route("UserPerBatch")]
        public async Task<List<Dictionary<string, string>>> UserPerBatch(int BatchID)
        {
            List<Dictionary<string, string>> resultJSON = new List<Dictionary<string, string>>();

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                var result = from st in fedenaEntities.students

                             where st.batch_id == BatchID

                             select new
                             {
                                 StudentID = st.user_id,
                                 FullName = st.first_name + " " + st.last_name,
                                 SchoolID = st.school_id
                             };

                foreach(var res in result)
                {
                    Dictionary<string, string> resultDictionary = new Dictionary<string, string>();

                    resultDictionary.Add("UserID", res.StudentID.ToString());
                    resultDictionary.Add("FullName", res.FullName.ToString());
                    resultDictionary.Add("SchoolID", res.SchoolID.ToString());

                    resultJSON.Add(resultDictionary);
                }

            }

            return resultJSON;
        }

        [Route("GuardiansPerBatch")]
        public async Task<List<Dictionary<string, string>>> GuardiansPerBatch(int BatchID)
        {
            List<Dictionary<string, string>> resultJSON = new List<Dictionary<string, string>>();

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                var result = from st in fedenaEntities.students

                             join g in fedenaEntities.guardians on st.sibling_id equals g.ward_id

                             where st.batch_id == BatchID

                             select new
                             {
                                 StudentID = st.user_id,
                                 ParentFullName = g.first_name + " " + g.last_name,
                                 SchoolID = st.school_id
                             };

                foreach (var res in result)
                {
                    Dictionary<string, string> resultDictionary = new Dictionary<string, string>();

                    resultDictionary.Add("UserID", res.StudentID.ToString());
                    resultDictionary.Add("ParentFullName", res.ParentFullName.ToString());
                    resultDictionary.Add("SchoolID", res.SchoolID.ToString());

                    resultJSON.Add(resultDictionary);
                }
            }

            return resultJSON;
        }

        [Route("EmployeePerDepartment")]
        public async Task<List<Dictionary<string, string>>> EmployeePerDepartment(int DeparementID)
        {
            List<Dictionary<string, string>> resultJSON = new List<Dictionary<string, string>>();

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                var result = from e in fedenaEntities.employees

                             where e.employee_department_id == DeparementID

                             select new
                             {
                                 EmployeeID = e.user_id,
                                 EmployeeFullName = e.first_name + " " + e.last_name,
                                 SchoolID = e.school_id
                             };

                foreach (var res in result)
                {
                    Dictionary<string, string> resultDictionary = new Dictionary<string, string>();

                    resultDictionary.Add("UserID", res.EmployeeID.ToString());
                    resultDictionary.Add("EmployeeFullName", res.EmployeeFullName.ToString());
                    resultDictionary.Add("SchoolID", res.SchoolID.ToString());

                    resultJSON.Add(resultDictionary);
                }

            }

            return resultJSON;
        }

        [Route("UsersPerSchoolID")]
        public async Task<List<Dictionary<string, string>>> UsersPerSchoolID(int SchoolID)
        {
            List<Dictionary<string, string>> resultJSON = new List<Dictionary<string, string>>();

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                var result = from u in fedenaEntities.users

                             where u.school_id == SchoolID

                             select new
                             {
                                 UserID = u.id,
                                 UserFullName = u.first_name + " " + u.last_name,
                                 SchoolID = u.school_id
                             };

                foreach (var res in result)
                {
                    Dictionary<string, string> resultDictionary = new Dictionary<string, string>();

                    resultDictionary.Add("UserID", res.UserID.ToString());
                    resultDictionary.Add("UserFullName", res.UserFullName.ToString());
                    resultDictionary.Add("SchoolID", res.SchoolID.ToString());

                    resultJSON.Add(resultDictionary);
                }

            }

            return resultJSON;
        }

        [Route("GetSchoolLogo")]
        public async Task<List<Dictionary<string, string>>> GetSchoolLogo(int SchoolID)
        {
            List<Dictionary<string, string>> resultJSON = new List<Dictionary<string, string>>();
            string base64ImageText = string.Empty;
            Dictionary<string, string> resultDictionary = new Dictionary<string, string>();

            using (fedenaEntities fedenaEntities = new fedenaEntities())
            {
                var result = (from sd in fedenaEntities.school_domains

                              where sd.linkable_id == SchoolID

                              select new
                              {
                                  SchoolDomain = "http://" + sd.domain
                              }).ToList();

                var m_strFilePath = result.ToList().First().SchoolDomain + "/api/madrasatie/get_school_logo";
                string xmlStr;
                using (var wc = new WebClient())
                {
                    xmlStr = wc.DownloadString(m_strFilePath);
                }
                var xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(xmlStr);

                XmlNodeList elemList = xmlDoc.GetElementsByTagName("school");

                foreach (XmlNode chldNode in xmlDoc.ChildNodes)
                {
                    foreach (XmlNode grandChldNode in chldNode)
                    {
                        foreach (XmlNode grandGrandChldNode in grandChldNode)
                        {
                            if (grandGrandChldNode.Name == "image")
                            {
                                base64ImageText = grandGrandChldNode.InnerText;
                            }
                        }
                    }
                }

                int indexOfInfo = base64ImageText.IndexOf("base64,");

                base64ImageText = base64ImageText.Substring(indexOfInfo + 7);
                base64ImageText = base64ImageText.Replace("\n", String.Empty);

                resultDictionary.Add("Message", "Success");
                resultDictionary.Add("Logo", base64ImageText);

                resultJSON.Add(resultDictionary);

                resultJSON = resultJSON.ToList<Dictionary<string, string>>();
            }

            return resultJSON;
        }

        [Route("RegisterActiveUser")]
        public List<Dictionary<string, string>> RegisterActiveUser(int UserID, int SchoolID, string Username, string Password, string DeviceType, string Token = null, string OSVersion = null, string DeviceID = null)
        {
            try
            {
                List<object> resultJSON = new List<object>();
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    //log out all active users
                    try
                    {
                        List<mpns_madrasatie_notifications> result = (from mpns in fedenaEntities.mpns_madrasatie_notifications
                                                                      where mpns.token == Token
                                                                      select mpns).ToList();

                        foreach (var mpnsNotification in result)
                        {
                            mpnsNotification.logged_in = false;
                        }
                    }
                    catch { }

                    //check if user already exist and log him in
                    try
                    {
                        List<mpns_madrasatie_notifications> result = (from mpns in fedenaEntities.mpns_madrasatie_notifications
                                                                      where mpns.user_id == UserID
                                                                      && mpns.school_id == SchoolID
                                                                      && mpns.username == Username
                                                                      && mpns.device_type == DeviceType
                                                                      && mpns.token == Token
                                                                      select mpns).ToList();

                        foreach (var mpnsNotification in result)
                        {
                            mpnsNotification.logged_in = true;

                            fedenaEntities.SaveChanges();
                        }

                        if(result.Count > 0)
                        {
                            OutputMessage.Add("Message", "Success");
                            OutputMessageList.Add(OutputMessage);

                            return OutputMessageList;
                        }
                    }
                    catch { }
                    
                    //if user doesn't exist, add him to table
                    mpns_madrasatie_notifications MpnsMadrasatieNotification = new mpns_madrasatie_notifications();
                    MpnsMadrasatieNotification.user_id = UserID;
                    MpnsMadrasatieNotification.school_id = SchoolID;
                    MpnsMadrasatieNotification.token = Token;
                    MpnsMadrasatieNotification.username = Username;
                    MpnsMadrasatieNotification.password = Password;
                    MpnsMadrasatieNotification.operating_system = OSVersion;
                    MpnsMadrasatieNotification.device_type = DeviceType;
                    MpnsMadrasatieNotification.device_id = DeviceID;
                    MpnsMadrasatieNotification.logged_in = true;

                    fedenaEntities.mpns_madrasatie_notifications.Add(MpnsMadrasatieNotification);

                    fedenaEntities.SaveChanges();
                }

                Dictionary<string, object> deviceDescription = new Dictionary<string, object>();

                deviceDescription.Add("Name", Username);
                deviceDescription.Add("RemoteIdentifier", Username);
                deviceDescription.Add("OS", OSVersion);
                deviceDescription.Add("NotificationToken", Token);
                deviceDescription.Add("ApplicationIdentifier", Globals.Configuration.APPLICATION_IDENTIFIER);
                deviceDescription.Add("Mode", "2");

                OutputMessage.Add("Message", "Success");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
            catch
            {
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                OutputMessage.Add("Message", "Error");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
        }

        [Route("LogOutUser")]
        public List<Dictionary<string, string>> LogOutUser(int UserID, int SchoolID, string Username, string Password, string Token = null)
        {
            try
            {
                List<object> resultJSON = new List<object>();
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    List<mpns_madrasatie_notifications> result = (from mpns in fedenaEntities.mpns_madrasatie_notifications
                                                                  where mpns.user_id == UserID
                                                                 && mpns.school_id == SchoolID
                                                                 && mpns.username == Username
                                                                 && mpns.password == Password
                                                                 && mpns.token == Token
                                                                 select mpns).ToList();

                    foreach (var mpnsNotification in result)
                    {
                        mpnsNotification.logged_in = false;
                    }

                    fedenaEntities.SaveChanges();

                    resultJSON = result.ToList<object>();
                }

                OutputMessage.Add("Message", "Success");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
            catch
            {
                List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
                Dictionary<string, string> OutputMessage = new Dictionary<string, string>();

                OutputMessage.Add("Message", "Error");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
        }

        [Route("PushNotification")]
        public async Task<List<Dictionary<string, string>>> PushNotification(string Token, string Message, string DeviceType, bool Production = false)
        {
            List<Dictionary<string, string>> OutputMessageList = new List<Dictionary<string, string>>();
            Dictionary<string, string> OutputMessage = new Dictionary<string, string>();


            try
            {
                switch (DeviceType.ToLower().Trim())
                {
                    case "ios":
                        MPNS.NotificationDescriptor notificationDescriptor = new MPNS.NotificationDescriptor();
                        notificationDescriptor.DeviceToken = Token;
                        notificationDescriptor.Message = Message;

                        MPNS.Application application = new MPNS.Application();
                        application.Identifier = "madrasatie.app.wb";
                        application.Mode = Production == true ? MPNS.ServiceContext.ServiceContextProduction : MPNS.ServiceContext.ServiceContextSandbox;
                        application.OperatingSystem = MPNS.ServiceType.ServiceTypeiOS;

                        MPNS.MobilePushNotificationServicesInterfaceClient clientIOS = new MPNS.MobilePushNotificationServicesInterfaceClient();
                        clientIOS.registerDeviceNotification(notificationDescriptor, application);

                        OutputMessage.Add("Message", "Success");
                        OutputMessageList.Add(OutputMessage);
                        break;

                    case "android":
                        using (HttpClient client = new HttpClient())
                        {
                            client.BaseAddress = new Uri(Globals.Configuration.ANDROID_PUSH_SERVICE_API);
                            client.DefaultRequestHeaders.Accept.Clear();
                            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                            StringBuilder stringBuilder = new StringBuilder();
                            stringBuilder.Append("/api/AndroidPush/SendAndroidMessage");
                            stringBuilder.Append("?");
                            stringBuilder.AppendFormat("message={0}", Message);
                            stringBuilder.Append("&");
                            stringBuilder.AppendFormat("token={0}", Token);
                            HttpResponseMessage response = await client.GetAsync(stringBuilder.ToString()).ConfigureAwait(false);
                            response.EnsureSuccessStatusCode();
                            if (response.IsSuccessStatusCode)
                            {
                                string result = "success";
                            }
                        }

                        OutputMessage.Add("Message", "Success");
                        OutputMessageList.Add(OutputMessage);
                        break;

                    default:
                        break;
                }

                return OutputMessageList;
            }
            catch
            {
                OutputMessage.Add("message", "error");
                OutputMessageList.Add(OutputMessage);

                return OutputMessageList;
            }
        }

        public string SixDigitsString(string OldString)
        {
            string NewString = string.Empty;

            switch (OldString.Length)
            {
                case 1:
                    NewString = "000/000/00" + OldString[0];
                    break;

                case 2:
                    NewString = "000/000/0" + OldString[0] + OldString[1];
                    break;

                case 3:
                    NewString = "000/000/" + OldString[0] + OldString[1] + OldString[2];
                    break;

                case 4:
                    NewString = "000/00" + OldString[0] + "/" + OldString[1] + OldString[2] + OldString[3];
                    break;

                case 5:
                    NewString = "000/0" + OldString[0] + OldString[1] + "/" + OldString[2] + OldString[3] + OldString[4];
                    break;

                case 6:
                    NewString = "000/" + OldString[0] + OldString[1] + OldString[2] + "/" + OldString[3] + OldString[4] + OldString[5];
                    break;

                case 7:
                    NewString = "00" + OldString[0] + "/" + OldString[1] + OldString[2] + OldString[3] + "/" + OldString[4] + OldString[5] + OldString[6];
                    break;

                case 8:
                    NewString = "0" + OldString[0] + OldString[1] + "/" + OldString[2] + OldString[3] + OldString[4] + "/" + OldString[5] + OldString[6] + OldString[7];
                    break;

                case 9:
                    NewString = OldString[0] + OldString[1] + OldString[2] + "/" + OldString[3] + OldString[4] + OldString[5] + "/" + OldString[6] + OldString[7] + OldString[8];
                    break;

                default:
                    NewString = "";
                    break;

            }
            return NewString;
        }

        static string Hash(string input)
        {
            var hash = (new SHA1Managed()).ComputeHash(Encoding.UTF8.GetBytes(input));
            return string.Join("", hash.Select(b => b.ToString("x2")).ToArray());
        }

        private byte[] ReadData(Stream stream)
        {
            byte[] buffer = new byte[16 * 1024];

            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }

                return ms.ToArray();
            }
        }

        private string GetSchoolToken(string Username, string Password, string ClientID, string ClientSecret, string URL)
        {
            string result = string.Empty;

            string redirect_uri = "http://apis.fedena.com/fedena/generate_token";
            URL += "oauth/token/";
            URL = "http://" + URL;
            //string Url = "http://bis1.madrasatie.net/oauth/token/";

            //GET
            var postData = "?client_id=" + ClientID;
            postData += "&client_secret=" + ClientSecret;
            postData += "&username=" + Username;
            postData += "&password=" + Password;
            postData += "&grant_type=password";
            postData += "&redirect_uri=" + redirect_uri;

            var request = (HttpWebRequest)WebRequest.Create(URL + postData);

            var data = Encoding.ASCII.GetBytes(postData);

            request.Method = "POST";
            request.ContentType = "'application/x-www-form-urlencoded";

            var response = (HttpWebResponse)request.GetResponse();

            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            JObject jsonResponse = JObject.Parse(responseString);

            result = jsonResponse["access_token"].ToString();

            return result;
        }

        private List<Dictionary<string,string>> GetUserToken(int? UserID, int? SchoolID)
        {
            List<Dictionary<string, string>> ListResult = new List<Dictionary<string, string>>();

            try
            {
                using (fedenaEntities fedenaEntities = new fedenaEntities())
                {
                    List<mpns_madrasatie_notifications> MPNSUser = (from mpns in fedenaEntities.mpns_madrasatie_notifications
                                                                    where mpns.user_id == UserID
                                                                   && mpns.school_id == SchoolID
                                                                   //&& mpns.username == Username
                                                                   //&& mpns.password == Password
                                                                   && mpns.logged_in == true
                                                                   select mpns).ToList();

                    foreach(var user in MPNSUser)
                    {
                        Dictionary<string, string> result = new Dictionary<string, string>();
                        result.Add("Token", user.token);
                        result.Add("DeviceType", user.device_type);
                        ListResult.Add(result);
                    }
                }
            }
            catch
            {
                Dictionary<string, string> result = new Dictionary<string, string>();
                result.Add("Message", "Error");
                ListResult.Add(result);
            }

            return ListResult;
        }

        private string generatePushNotificationMessage(int? UserIDParent, int? UserIDStudent, int? UserIDEmployee, int? SchoolID, string UserType, string ActionName)
        {
            string result = string.Empty;

            switch (UserType.ToLower().Trim())
            {
                case "parent":
                    using (fedenaEntities fedenaEntities = new fedenaEntities())
                    {
                        string userParent = (from g in fedenaEntities.guardians
                                       where g.user_id == UserIDParent
                                       select new
                                       {
                                           ParentName = (g.first_name + " " + g.last_name)
                                       }
                                       ).First()
                                       .ParentName.ToString();

                        string userStudent = (from s in fedenaEntities.students
                                              where s.id == UserIDStudent
                                              select new
                                              {
                                                  StudentName = (s.first_name + " " + s.last_name)
                                              }
                                              ).First()
                                              .StudentName.ToString();
                        
                        result = "Dear " + userParent + ", " + userStudent + " was absent today.";
                    }
                    break;

                case "student":
                    using (fedenaEntities fedenaEntities = new fedenaEntities())
                    {
                        string userStudent = (from s in fedenaEntities.students
                                              where s.id == UserIDStudent
                                              select new
                                              {
                                                  StudentName = (s.first_name + " " + s.last_name)
                                              }
                                              ).First()
                                              .StudentName.ToString();

                        result = "Dear " + userStudent + ", you have been assigned an " + ActionName;
                    }
                    break;
            }

            return result;
        }

        private string generatePushNotificationMessageLateness(int? UserIDParent, int? UserIDStudent, int? UserIDEmployee, int? SchoolID, string UserType, string ActionName)
        {
            string result = string.Empty;

            switch (UserType.ToLower().Trim())
            {
                case "parent":
                    using (fedenaEntities fedenaEntities = new fedenaEntities())
                    {
                        string userParent = (from g in fedenaEntities.guardians
                                             where g.user_id == UserIDParent
                                             select new
                                             {
                                                 ParentName = (g.first_name + " " + g.last_name)
                                             }
                                       ).First()
                                       .ParentName.ToString();

                        string userStudent = (from s in fedenaEntities.students
                                              where s.id == UserIDStudent
                                              select new
                                              {
                                                  StudentName = (s.first_name + " " + s.last_name)
                                              }
                                              ).First()
                                              .StudentName.ToString();

                        result = "Dear " + userParent + ", " + userStudent + " was late for class.";
                    }
                    break;

                case "student":
                    using (fedenaEntities fedenaEntities = new fedenaEntities())
                    {
                        string userStudent = (from s in fedenaEntities.students
                                              where s.id == UserIDStudent
                                              select new
                                              {
                                                  StudentName = (s.first_name + " " + s.last_name)
                                              }
                                              ).First()
                                              .StudentName.ToString();

                        result = "Dear " + userStudent + ", you are late for class.";
                    }
                    break;
            }

            return result;
        }

        private string generateAssignmentPushNotificationMessage()
        {
            return "Assignment notification";
        }


        private string newPushNotificationMessage(string Date)
        {
            return "A new event has been created on " + Date;
        }

        private string updatePushNotificationMessage(string Date)
        {
            return "An event has been updated on " + Date;
        }


        private static string GetNumbers(string input)
        {
            return new string(input.Where(c => char.IsDigit(c)).ToArray());
        }
    }
}